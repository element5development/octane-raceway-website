<?php
/**
 * Template Name: blog
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
           <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
          </div>
          <?php //include 'sb.php'; 
          ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
		
<div class="stay-inform-form"> 
     <div class="row">
        <div class="stay-inform-form-in clearfix">
			  
			<?php the_content();?>
        </div>
     </div>
</div>

     </div>  <!--about-us-banner ends here-->
	
    
          
        
	<?php 
	}
}
	
	?>
    <div id="blogpage"> 
<div class=" row clearfix">
	<div class="content">
		<div class="blog-page-main">
			<div class="big-demo go-wide" data-js-module="filtering-demo">
				<div class="tabs blog-tabs">
				
					<ul class="list-items categories filter-button-group button-group js-radio-button-group"> 
						<li><button class="button is-checked" data-filter="*">all</button></li>
						<?php $category_ids = get_all_category_ids(); ?> 
						<?php $args = array( 'orderby' => 'slug', 'parent' => 0, 'exclude' => array(1,11) ); 
						$categories = get_categories( $args );
						foreach ( $categories as $category ) { 
							if(!empty($category->slug)) {
								echo '<li><button class="button" data-filter=".cat_'. $category->slug .'">' . $category->name . '</button></li>'; 
							}
						} ?> 
					</ul>
				
				</div>
			
				<style type="text/css">
					.fts-jal-fb-group-display, .fts-twitter-div {
						clear: none;
					}
				</style>
				<div class=" blog-listing">
					<ul class="grid" id="more_list_post">
						<?php
						$args=array( 'category__not_in' => '1' );
						query_posts( $args );
						$no_of_post=$wp_query->found_posts;
						while ( have_posts() ) : the_post(); ?>	 
							<?php
							$category = get_the_category( $post->ID );
							$cats = '';
							foreach ($category as $cats_list){
								$cats .= 'cat_'.$cats_list->slug.' '; 
							}
							?>
							<li class="element-item <?php echo $cats; ?>">
								<?php 
								$blog_img_url = '';
								if ( has_post_thumbnail() ) {
									$blog_img=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog_list_img' );
									$blog_img_url=$blog_img['0']; 
								} ?>
								<div class=" list-img-wrap" style="background: url(<?php echo $blog_img_url; ?>) center no-repeat; background-size: cover;">
									<div class=" list-img">
										<div class=" list-heading"> 
											<h3><?php the_title();?></h3>
											<span class=" date-text"><?php echo get_the_date(); ?></span>
										</div>
									</div>
								</div>
								<div class="list-bottom-heading clearfix">
									<div class="list-social">
										<a class="facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
										<a class="twitter customer share" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title();?>&hashtags=octaneraceway" target="_blank"><i class="fa fa-twitter"></i></a>
										<a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
									</div>
									<div class="list-read-btn clearfix">
										<a href="<?php the_permalink(); ?>" class="red-button red-button-small">READ</a>
									</div> 
								</div>
							</li>
						<?php 
						endwhile;wp_reset_query(); ?>
					</ul>    
				<?php echo do_shortcode('[fts_facebook id=octaneracewaykarting posts_displayed=page_only type=page]'); ?>

					<img src="<?php echo get_template_directory_uri();?>/images/loading.gif" style="padding-left:267px;display:none;" id="loding_window">
					<?php

					if($no_of_post>get_option('posts_per_page')) { ?>
						<a class="button-default button-full" onclick="get_more_posts('all',<?php echo $no_of_post;?>)" id="load">load more</a>
					<?php
					} ?>
				</div>
			</div>
       </div>
     </div>
 	<div id="sidebar">
		<h2>CUSTOMER REVIEWS</h2>
		<div class="sidebar-box tripadvisor">
			<div id="TA_selfserveprop392" class="TA_selfserveprop">
				<ul id="VpEj6jU3F" class="TA_links 1RqK371zDQeG">
					<li id="sGfazHHiZx" class="sJ5NI3b6fSdE">
						<a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
					</li>
				</ul>
			</div>
			<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=392&amp;locationId=2243652&amp;lang=en_US&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
	        </div>
		
		
		</div>	
	</div>
</div>
<script type="text/javascript">
function get_more_posts(id,number){
			//alert(id);
			//alert(number);
		  jQuery('#loding_window').fadeIn();
		  var url =  '<?php echo  admin_url('admin-ajax.php');?>?action=my_action&cat_id='+id+'&number='+number;
			jQuery.post(url, {  },
				function( data1 ) {
				//alert(data1);exit;
				jQuery('#more_list_post').html(data1);
				jQuery('#more_list_post').css('height', 'auto');
				jQuery('#loding_window').fadeOut();
				jQuery('#load').fadeOut();
				}
				);
		   
	   }
      
</script>

<script>
;(function($){
  
  /**
   * jQuery function to prevent default anchor event and take the href * and the title to make a share pupup
   *
   * @param  {[object]} e           [Mouse event]
   * @param  {[integer]} intWidth   [Popup width defalut 500]
   * @param  {[integer]} intHeight  [Popup height defalut 400]
   * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
   */
  $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
    
    // Prevent default anchor event
    e.preventDefault();
    
    // Set values for window
    intWidth = intWidth || '500';
    intHeight = intHeight || '400';
    strResize = (blnResize ? 'yes' : 'no');

    // Set title and open popup with focus on it
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
  }
  
  /* ================================================== */
  
  $(document).ready(function ($) {
    $('.customer.share').on("click", function(e) {
      $(this).customerPopup(e);
    });
  });
    
}(jQuery));

</script>
<?php
get_footer();
?>