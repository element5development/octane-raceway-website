<?php
/**
 * octane functions and definitions
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */

// Register custom InterWebing Testimonials Function
include("functions/register_custom_post_type_testimonials.php");

define('THEME_PREFIX', 'octane_');
define('THEME_URL_PREFIX', 'octane-');
define('THEME_TEXTDOMAIN', 'octane_text_domain');
define('THEME_NAME', __(' octane ',THEME_TEXTDOMAIN));
define('THEME_SLUG', 'octane');
define( 'THEME_PATH', get_stylesheet_directory().'/');
define( 'THEME_URL', get_stylesheet_directory_uri().'/');
define( 'THEME_ADMIN_PATH',  THEME_PATH.'admin'  );
/**
 * octane setup.
 * Sets up theme defaults and registers the various WordPress features that
 * octane supports.
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 * @since octane 1.0
 */
function octane_setup() {
	/*
	 * Makes octane available for translation.
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on octane, use a find and
	 * replace to change THEME_TEXTDOMAIN to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( THEME_TEXTDOMAIN, get_template_directory() . '/languages' );

	// Adds RSS feed links to <head> for posts and comments.

	add_theme_support( 'automatic-feed-links' );

	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	add_image_size( 'home_big', 380, 195, array( 'center', 'center' ) );
	add_image_size( 'home_1big', 448.813, 195, array( 'center', 'center' ) );
	add_image_size( 'custom', 60, 55, array( 'center', 'center' ) );
	add_image_size( 'slider', 270, 224, array( 'center', 'center' ) );
	add_image_size( 'eat_drink_big', 515, 321, array( 'center', 'center' ) );
	add_image_size( 'eat_drink_small', 80, 78, array( 'center', 'center' ) );
	add_image_size( 'group_event_image', 483, 264, array( 'center', 'center' ) );
	add_image_size( 'blog_list_img', 288, 280, array( 'center', 'center' ) );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'top_menu', __( 'Top Menu', THEME_TEXTDOMAIN ) );
	register_nav_menu( 'blog_menu', __( 'Blog Menu', THEME_TEXTDOMAIN ) );
	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
}
add_action( 'after_setup_theme', 'octane_setup' );

// Register Custom Post Type
function octane_testimonials() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'toctane_testimonial' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'toctane_testimonial' ),
		'menu_name'             => __( 'Testimonials', 'toctane_testimonial' ),
		'name_admin_bar'        => __( 'Post Type', 'toctane_testimonial' ),
		'archives'              => __( 'Item Archives', 'toctane_testimonial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'toctane_testimonial' ),
		'all_items'             => __( 'All Items', 'toctane_testimonial' ),
		'add_new_item'          => __( 'Add New Item', 'toctane_testimonial' ),
		'add_new'               => __( 'Add Testimonial', 'toctane_testimonial' ),
		'new_item'              => __( 'New Item', 'toctane_testimonial' ),
		'edit_item'             => __( 'Edit Item', 'toctane_testimonial' ),
		'update_item'           => __( 'Update Item', 'toctane_testimonial' ),
		'view_item'             => __( 'View Item', 'toctane_testimonial' ),
		'search_items'          => __( 'Search Item', 'toctane_testimonial' ),
		'not_found'             => __( 'Not found', 'toctane_testimonial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'toctane_testimonial' ),
		'featured_image'        => __( 'Featured Image', 'toctane_testimonial' ),
		'set_featured_image'    => __( 'Set featured image', 'toctane_testimonial' ),
		'remove_featured_image' => __( 'Remove featured image', 'toctane_testimonial' ),
		'use_featured_image'    => __( 'Use as featured image', 'toctane_testimonial' ),
		'insert_into_item'      => __( 'Insert into item', 'toctane_testimonial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'toctane_testimonial' ),
		'items_list'            => __( 'Items list', 'toctane_testimonial' ),
		'items_list_navigation' => __( 'Items list navigation', 'toctane_testimonial' ),
		'filter_items_list'     => __( 'Filter items list', 'toctane_testimonial' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'toctane_testimonial' ),
		'description'           => __( 'Testimonial', 'toctane_testimonial' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-thumbs-up',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'octane_testimonials', $args );

}
add_action( 'init', 'octane_testimonials', 0 );

function octane_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Eat&Drink Side Bar Text Heading Area', 'octane' ),
		'id' => 'eat_drink_foodmenu_textheading_widget',
		'description' => __( 'The Eat&Drink Heading Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Eat&Drink Food Menu Sidebar Area', 'octane' ),
		'id' => 'eat_drink_foodmenu_widget',
		'description' => __( 'The Eat&Drink Food Menu Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Contact Regular Hours Sidebar', 'octane' ),
		'id' => 'contact_regular_hours_sidebar',
		'description' => __( 'The Contact Regular Hours Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Contact Holiday Hours Sidebar', 'octane' ),
		'id' => 'contact_holiday_hours_sidebar',
		'description' => __( 'The Contact Holiday Hours Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Kart Racing Sidebar', 'octane' ),
		'id' => 'kart_racing_sidebar',
		'description' => __( 'Kart Racing Page Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Play Page Group Events Sidebar', 'octane' ),
		'id' => 'play_group_events_sidebar',
		'description' => __( 'Play Page Group Events Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Play Page Outdoor Games Sidebar', 'octane' ),
		'id' => 'play_outdoor_games_sidebar',
		'description' => __( 'Play Page Outdoor Games Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Instragram Sidebar', 'octane' ),
		'id' => 'instragram_sidebar',
		'description' => __( 'Instragram Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	register_sidebar( array(
		'name' => __( 'Group Events Sidebar', 'octane' ),
		'id' => 'group_event_sidebar',
		'description' => __( 'Group Events Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );

	register_sidebar( array(
		'name' => __( 'Velocity VR Sidebar', 'octane' ),
		'id' => 'velocity_vr_sidebar',
		'description' => __( 'Velocity VR Page Sidebar Area', 'octane' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
	
	
}
add_action( 'widgets_init', 'octane_widgets_init' );
/**
 * Enqueue scripts and styles for the front end.
 * @since octane 1.0
 */

function octane_enqueue_style() {
		//Add a new Flexislider css and js for octane site
		
	    //wp_enqueue_style( 'octane-bgndgallery', THEME_URL.'dist/css/jquery.mb.bgndgallery.min.css' );
		wp_enqueue_style( 'googleapi-font-style','https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic');
		wp_enqueue_style( 'awesome-font-style','https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
		wp_enqueue_style( 'style-main', THEME_URL.'style.css');
		wp_enqueue_style( 'octane_media-style', THEME_URL.'css/media.css');
		wp_enqueue_style( 'octane-animate', THEME_URL.'css/animate.css' );
		//wp_enqueue_style( 'octane-lightslider-css', THEME_URL.'css/lightslider.css' );
		wp_enqueue_style( 'octane-magnific-popup-css', THEME_URL.'css/magnific-popup.css' );
		wp_enqueue_style( 'octane-carousel-css', THEME_URL.'css/owl.carousel.css' );
		//wp_enqueue_style( 'octane-fancybox-css', THEME_URL.'css/jquery.fancybox.css' );
		wp_enqueue_style( 'octane-testimonials-css', THEME_URL.'css/testimonials.css' );
		//wp_enqueue_style( 'awesome-font-style','https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
		
		
		//wp_enqueue_style( 'googleapis-font-style','https://fonts.googleapis.com/css?family=Lato:400,900italic,700,300italic,300,100italic,100,900,400italic,700italic' );
		
		//wp_enqueue_style( 'style-main', THEME_URL.'css/style.css');
		
		
				
		 // Loads our main stylesheet.
		//wp_enqueue_style( 'octane-style', get_stylesheet_uri());
		
		
		//Load main css
		//wp_enqueue_style( 'octane_media-style', THEME_URL.'css/media.css');
		
}
function octane_enqueue_script() {
	
			//wp_enqueue_script( 'jquery-jquery-script', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js' );
			
			wp_register_script( 'octane-map-script-site', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB16sGmIekuGIvYOfNoW9T44377IU2d2Es', '10', '1.1', true );
			wp_register_script( 'octane-extends-map-script', THEME_URL.'js/google_map.js', '20', '1.1', true );
			
			//wp_enqueue_script( 'jquery-lightslider-script', THEME_URL.'js/lightslider.js' );
			wp_enqueue_script( 'jquery-rotate-script', THEME_URL.'js/rotate.js' );
			wp_enqueue_script( 'jquery-parallax-script', THEME_URL.'js/parallax.js' );
			wp_enqueue_script( 'jquery-custom-override-script', THEME_URL.'js/custom-override.js' );
			wp_enqueue_script( 'jquery-magnific-popup-script', THEME_URL.'js/jquery.magnific-popup.js' );
			wp_enqueue_script( 'jquery-custom-funct-script', THEME_URL.'js/custom-funct.js' );
			wp_enqueue_script( 'jquery-validation-subscriber-script', THEME_URL.'js/validation-subscriber.js' );
			wp_enqueue_script( 'jquery-validation-groupevent-form-script', THEME_URL.'js/validation-groupevent-form.js' );
			//wp_enqueue_script( 'jquery-validation-index-script', THEME_URL.'js/validation-index.js' );
			wp_enqueue_script( 'jquery-validation-bookparty-script', THEME_URL.'js/validation-bookparty.js' );
			
			//wp_enqueue_script( 'jquery-bgndGallery-script', THEME_URL.'dist/jquery.mb.bgndGallery.min.js' );
			//wp_enqueue_script( 'jquery-owl-carousel-script', THEME_URL.'js/owl.carousel.js' );
			wp_enqueue_script( 'jquery-owl-carousel-min-script', THEME_URL.'js/owl.carousel.min.js' );
	             /*********** for fancy box **********************/	
		    //wp_enqueue_script( 'jquery-fancybox-script', THEME_URL.'js/jquery.fancybox.js' );
			wp_enqueue_script( 'jquery-mousewheel-script', THEME_URL.'js/jquery.mousewheel.pack.js' );
			 /*********** for fade animation **********************/
			wp_enqueue_script( 'jquery-orientation-fix-script', THEME_URL.'js/orientation-fix.js' );
			wp_enqueue_script( 'jquery-waypoints-script', THEME_URL.'js/waypoints.min.js' );
			wp_enqueue_script( 'jquery-background-video', THEME_URL.'js/backgroundVideo.js' );
			wp_enqueue_script( 'jquery-gform-validation', THEME_URL.'js/validation-gform.js' );
			wp_enqueue_script( 'jquery-iefix', THEME_URL.'js/ieFix.js');
}
add_action( 'wp_enqueue_scripts', 'octane_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'octane_enqueue_script' );
add_action( 'admin_enqueue_scripts', 'bs_enqueue_admin_scripts' );

function bs_enqueue_admin_scripts() {
		
		wp_enqueue_media();
		wp_enqueue_script( 'gvp-media', THEME_URL.'js/bs.js' );
		wp_localize_script( 'gvp-media', 'logo',
				array(
					'title'  => __( 'Upload or choose a Image', 'gvp' ), 
					'button' => __( 'Insert Image', 'gvp' )              
				)
			);
	
		
	}

/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since octane 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */

function octane_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() )
		return $title;
	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";
	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		$title = "$title $sep " . sprintf( __( 'Page %s', THEME_TEXTDOMAIN ), max( $paged, $page ) );
	return $title;
}
add_filter( 'wp_title', 'octane_wp_title', 10, 2 );



/**
 * Return the post URL.
 *
 * the first link found in the post content.
 * Falls back to the post permalink if no URL is found in the post.
 * @since octane 1.0
 * @return string The Link format URL.
 */

function octane_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );
	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

if ( ! function_exists( 'octane_excerpt_more' ) && ! is_admin() ) 
{
	/**
	 * Replaces "[...]" (appended to automatically generated excerpts) with ...
	 * and a Continue reading link.
	 * @since octane 1.4
	 * @param string $more Default Read More excerpt link.
	 * @return string Filtered Read More excerpt link.
	 */
	
	function octane_excerpt_more( $more ) {
		/*$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
			esc_url( get_permalink( get_the_ID() ) ),
				// translators: %s: Name of current post 
				sprintf( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', THEME_TEXTDOMAIN ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
			);
		return ' &hellip; ' . $link;*/
		return ' ';
	}
	add_filter( 'excerpt_more', 'octane_excerpt_more' );
}



/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since octane 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function octane_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';
	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	if(is_front_page() || is_home())
	{
		$classes[] = 'main-color';
	}
	return $classes;
}
add_filter( 'body_class', 'octane_body_class' );


function octane_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'octane_customize_register' );
$modules = array (
	THEME_PATH.'modules/',
	THEME_PATH.'modules/register-post-type/',
	THEME_PATH.'modules/short-code/',
	THEME_PATH.'modules/widgets/',
);

foreach ($modules as $module) {
	$php_files = glob( $module."*.php");
	if(!empty($php_files))
	{	foreach ($php_files as $php_file)
		{
			include_once( $php_file );
		}
	}
}

function octane_paginate($args = null) {
	$output ='';
	$defaults = array(
		'page' => null, 'pages' => null, 
		'range' => 3, 'gap' => 3, 'anchor' => 1,
		'before' => '<ul class="octane_paginate">', 'after' => '</ul>',
		'title' => __('', THEME_TEXTDOMAIN),
		'nextpage' => 'Next', 
		'previouspage' => 'Prev', 
		'lastpage' => __('', THEME_TEXTDOMAIN), 'firstpage' => __('', THEME_TEXTDOMAIN), 
		'echo' => 1
	);
	$r = wp_parse_args($args, $defaults);

	if (!$page && !$pages) {
		global $wp_query;
		$page = get_query_var('paged');
		$page = !empty($page) ? intval($page) : 1;
		$posts_per_page = intval(get_query_var('posts_per_page'));
		$pages = intval(ceil($wp_query->found_posts / $posts_per_page));
	}
	
	if ($pages > 1) {	
		$current_page = !empty($_REQUEST['paged']) ?  intval($_REQUEST['paged']) : '';
		if($current_page == 0)$current_page =1;
		$remain_page = intval($pages);
    	$output = $defaults['before'];
		$ellipsis = "";

		if ($page > 0 ) {
			$output .= "<li><a href='" . get_pagenum_link($page - 1) . "' class='emm-prev'>$previouspage</a></li>";
		}

		$min_links = $range * 2 + 1;
		$block_min = min($page - $range, $pages - $min_links);
		$block_high = max($page + $range, $min_links);
		$left_gap = (($block_min - $anchor - $gap) > 0) ? true : false;
		$right_gap = (($block_high + $anchor + $gap) < $pages) ? true : false;
		
		if ($left_gap && !$right_gap) {
			$output .= sprintf('%s%s%s', 
				octane_paginate_loop(1, $anchor), 
				$ellipsis, 
				octane_paginate_loop($block_min, $pages, $page)
			);
		}
		else if ($left_gap && $right_gap) {
			$output .= sprintf('%s%s%s%s%s', 
				octane_paginate_loop(1, $anchor), 
				$ellipsis, 
				octane_paginate_loop($block_min, $block_high, $page), 
				$ellipsis, 
				octane_paginate_loop(($pages - $anchor + 1), $pages)
			);
		}
		else if ($right_gap && !$left_gap) {
			$output .= sprintf('%s%s%s', 
				octane_paginate_loop(1, $block_high, $page),
				$ellipsis,
				octane_paginate_loop(($pages - $anchor + 1), $pages)
			);
		}
		else {

			$output .= octane_paginate_loop(1, $pages, $page);

		}

		if ($page < $pages && !empty($nextpage)) {
			$output .= "<li><a href='" . get_pagenum_link($page + 1) . "' class='emm-next'>$nextpage</a></li>";
		}
		$output .= $defaults['after'];
}
	if ($echo) {
		echo $output;
	}
	return $output;

}

function octane_paginate_loop($start, $max, $page = 0) {
	$output = "";
	for ($i = $start; $i <= $max; $i++) {
		$output .= ($page === intval($i)) 
			? "<li><a class='emm-page current emm-current emm-page-" . $i . "' href='javascript:;'>$i</a></li>" 
			: "<li><a href='" . get_pagenum_link($i) . "' class='emm-page emm-page-" . $i . "'>$i</a></li>";
	}
	return $output;
}
add_action( 'admin_menu', 'custom_create_meta_box' );
add_action( 'save_post', 'custom_save_meta_box' );
function custom_save_meta_box()
{
	global $post;
	    if($_REQUEST['cust_post_type']=='post')
		{
			if ( in_category( array( 'inside octane', 'nascar news', 'events' ) )) 
	       {
			$facebook_link=$_POST['facebook_link'];
			$linkedin_url=$_POST['linkedin_url'];
			$twitter_link=$_POST['twitter_link'];
			
		
			update_post_meta($post->ID,'facebook_link',$facebook_link);
			update_post_meta($post->ID,'linkedin_url',$linkedin_url);
			update_post_meta($post->ID,'twitter_link', $twitter_link);
		   }
		}
		if($_REQUEST['cust_post_type']=='team')
		{
			$designation=$_POST['designation'];
			$linkedin_url=$_POST['linkedin_url'];
			$email_id=$_POST['email_id'];
			
		
			update_post_meta($post->ID,'designation',$designation);
			update_post_meta($post->ID,'linkedin_url',$linkedin_url);
			update_post_meta($post->ID,'email_id', $email_id);
			
		}
		if($_REQUEST['post_type']=='page_meta')
		{
			$pagetitle1=$_POST['pagetitle1'];
			$pagetitle2=$_POST['pagetitle2'];
			update_post_meta($post->ID,'pagetitle1',$pagetitle1);
			update_post_meta($post->ID,'pagetitle2',$pagetitle2);
			$pagename=get_the_title( $post->ID );
			if($pagename=="Book Race")
			{
			$termcondition_text1=$_POST['termcondition_text1'];
			$termcondition_text2=$_POST['termcondition_text2'];
			$reservation_text=$_POST['reservation_text'];
			update_post_meta($post->ID,'termcondition_text1',$termcondition_text1);
			update_post_meta($post->ID,'termcondition_text2',$termcondition_text2);
			update_post_meta($post->ID,'reservation_text',$reservation_text);
			}
			if($pagename=="EAT DRINK")
			{
			$top_heading=$_POST['top_heading'];
			$middle_heading=$_POST['middle_heading'];
			$timings=$_POST['timings'];
			$background_image=$_POST['background_image'];
			$catering_postname=$_POST['catering_postname'];
			update_post_meta($post->ID,'top_heading',$top_heading);
			update_post_meta($post->ID,'middle_heading',$middle_heading);
			update_post_meta($post->ID,'timings',$timings);
			update_post_meta($post->ID,'background_image',$background_image);
			update_post_meta($post->ID,'catering_postname',$catering_postname);
			}
			if($pagename=="Contact")
			{
			$image_url=$_POST['image_url'];
			$googlemap_place_address=$_POST['googlemap_place_address'];
			
			update_post_meta($post->ID,'image_url',$image_url);
			update_post_meta($post->ID,'googlemap_place_address',$googlemap_place_address);
			
			}
			if($pagename=="Kart Racing")
			{
			$top_heading=$_POST['top_heading'];
			$middle_heading=$_POST['middle_heading'];
			$link_title=$_POST['link_title'];
			$link_url=$_POST['link_url'];
			$background_image=$_POST['background_image'];
			$instragram_heading=$_POST['instragram_heading'];
			$follow_heading=$_POST['follow_heading'];
			update_post_meta($post->ID,'top_heading',$top_heading);
			update_post_meta($post->ID,'middle_heading',$middle_heading);
			update_post_meta($post->ID,'link_title',$link_title);
			update_post_meta($post->ID,'link_url',$link_url);
			update_post_meta($post->ID,'background_image',$background_image);
			update_post_meta($post->ID,'instragram_heading',$instragram_heading);
			update_post_meta($post->ID,'follow_heading',$follow_heading);
			
			}
			if($pagename=="Play")
			{
			$top_heading=$_POST['top_heading'];
			$middle_heading=$_POST['middle_heading'];
			$link_title=$_POST['link_title'];
			$link_url=$_POST['link_url'];
			$background_image=$_POST['background_image'];
			$outdoor_games_heading=$_POST['outdoor_games_heading'];
			$outdoor_games_background=$_POST['outdoor_games_background'];
			$exclusive_event_heading=$_POST['exclusive_event_heading'];
			$exclusive_event_background=$_POST['exclusive_event_background'];
			
			update_post_meta($post->ID,'top_heading',$top_heading);
			update_post_meta($post->ID,'middle_heading',$middle_heading);
			update_post_meta($post->ID,'link_title',$link_title);
			update_post_meta($post->ID,'link_url',$link_url);
			update_post_meta($post->ID,'background_image',$background_image);
			update_post_meta($post->ID,'outdoor_games_heading',$outdoor_games_heading);
			update_post_meta($post->ID,'outdoor_games_background',$outdoor_games_background);
			update_post_meta($post->ID,'exclusive_event_heading',$exclusive_event_heading);
			update_post_meta($post->ID,'exclusive_event_background',$exclusive_event_background);
			
			
			}
			if($pagename=="Private Parties")
			{
			$awesome_event_spaces_link=$_POST['awesome_event_spaces_link'];
			update_post_meta($post->ID,'awesome_event_spaces_link',$awesome_event_spaces_link);	
			}
			
			
		}
		$bsimage_url=$_POST['bsimage_url'];
			$bsbtn_url=$_POST['bsbtn_url'];
			update_post_meta($post->ID,'bsimage_url',$bsimage_url);	
			update_post_meta($post->ID,'bsbtn_url',$bsbtn_url);
}
function custom_create_meta_box()
{
	if( function_exists( 'add_meta_box' ) ) {
	add_meta_box( 'new-meta-boxes','Team Information', 'display_team_meta_box', 'team', 'normal', 'high' );
	add_meta_box( 'new-meta-boxes','Page Information', 'display_page_meta_box', 'page', 'normal', 'high' );
	add_meta_box( 'new-meta-boxes2','Bottom Background', 'display_bb_meta_box', 'page', 'normal', 'high' );
	add_meta_box( 'new-meta-boxes','Blog Social Information', 'display_blog_meta_box', 'post', 'normal', 'high' );
	
	}
}
function display_blog_meta_box()
{
	if ( in_category( array( 'inside octane', 'nascar news', 'events' ) )) 
	{
	global $post; ?>
	<div style="padding-top:10px; padding-left:20px;">
         <table border="0" id="specialist_table" cellpadding="0" cellspacing="0">
        <?php
				
			$facebook_link=get_post_meta($post->ID,'facebook_link', true);
		    $linkedin_url=get_post_meta($post->ID,'linkedin_url', true);
			$twitter_link=get_post_meta($post->ID,'twitter_link', true);
		
			?>
				<tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;">Blog Facebook Link :</td><td>
                <input type="hidden" name="cust_post_type" value="post"  />
                <input type="text" name="facebook_link"  value="<?php echo $facebook_link;?>" id="facebook_link" style="width:400px;"  />  </td></tr></table>
                </td></tr>
                
              <tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;" >Blog Linked In Link :</td><td><input type="text" name="linkedin_url"  value="<?php echo $linkedin_url;?>" id="linkedin_url" style="width:400px;"  /></td></tr>
                </table> </td></tr>
                
                <tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;">Blog Twitter Link :</td><td><input type="text" name="twitter_link"  value="<?php echo $twitter_link;?>" id="twitter_link" style="width:400px;"  /></td></tr>
                 </table>
                </td></tr>
             </table>
             
             </div> <?php
    } 
}
function display_team_meta_box()
{
	global $post; ?>
	<div style="padding-top:10px; padding-left:20px;">
         <table border="0" id="specialist_table" cellpadding="0" cellspacing="0">
        <?php
				
			$designation=get_post_meta($post->ID,'designation', true);
		    $linkedin_url=get_post_meta($post->ID,'linkedin_url', true);
			$email_id=get_post_meta($post->ID,'email_id', true);
		
			?>
				<tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;">Team Member Designation :</td><td>
                <input type="hidden" name="cust_post_type" value="team"  />
                <input type="text" name="designation"  value="<?php echo $designation;?>" id="designation" style="width:400px;"  />  </td></tr></table>
                </td></tr>
                
              <tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;" >Team Member Linked In URL :</td><td><input type="text" name="linkedin_url"  value="<?php echo $linkedin_url;?>" id="linkedin_url" style="width:400px;"  /></td></tr>
                </table> </td></tr>
                
                <tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;">Team Member Email Id :</td><td><input type="text" name="email_id"  value="<?php echo $email_id;?>" id="email_id" style="width:400px;"  /></td></tr>
                 </table>
                </td></tr>
             </table>
             
             </div> <?php
}
function display_page_meta_box()
{
	global $post; ?>
	<div style="padding-top:10px; padding-left:20px;">
         <table border="0" id="specialist_table" cellpadding="0" cellspacing="0">
        <?php
				
			$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
		    $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
			$pagename=get_the_title( $post->ID );
		
			?>
				<tr><td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;">Page Title 1 :</td><td>
                <input type="hidden" name="post_type" value="page_meta"  />
                <input type="text" name="pagetitle1"  value="<?php echo $pagetitle1;?>" id="pagetitle1" style="width:400px;"  />  </td></tr></table>
                </td></tr>
                
              <tr>
			   <td style="vertical-align:text-top; padding-top:5px;">	
                <table border="0" ><tr><td style="width:160px;" >Page Title 2 :</td><td><input type="text" name="pagetitle2"  value="<?php echo $pagetitle2;?>" id="pagetitle2" style="width:400px;"  /></td></tr>
                </table>
			   </td>
			  </tr>
			  <?php if($pagename=="Private Parties")
				{
				    $awesome_event_spaces_link=get_post_meta($post->ID,'awesome_event_spaces_link', true);?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">Awesome Event Spaces Link :</td><td>
					
					<input type="text" name="awesome_event_spaces_link"  value="<?php echo $awesome_event_spaces_link;?>" id="awesome_event_spaces_link" style="width:400px;"  />  </td></tr></table>
					</td></tr>
				<?php }
				if($pagename=="Book Race")
				{
				    $termcondition_text1=get_post_meta($post->ID,'termcondition_text1', true);
					$termcondition_text2=get_post_meta($post->ID,'termcondition_text2', true);
					$reservation_text=get_post_meta($post->ID,'reservation_text', true);
				?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">Term & Condition Text 1 :</td><td>
					
					<input type="text" name="termcondition_text1"  value="<?php echo $termcondition_text1;?>" id="termcondition_text1" style="width:400px;"  />  </td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Term & Condition Text 2 :</td><td><input type="text" name="termcondition_text2"  value="<?php echo $termcondition_text2;?>" id="termcondition_text2" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Reservation Text  :</td><td><input type="text" name="reservation_text"  value="<?php echo $reservation_text;?>" id="reservation_text" style="width:400px;"  /></td></tr>
					</table> </td></tr>
				<?php	
				} 
				if($pagename=="EAT DRINK")
				{
				    $top_heading=get_post_meta($post->ID,'top_heading', true);
					$middle_heading=get_post_meta($post->ID,'middle_heading', true);
					$timings=get_post_meta($post->ID,'timings', true);
					$background_image=get_post_meta($post->ID,'background_image', true);$catering_postname=get_post_meta($post->ID,'catering_postname', true);
				?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">advertisement top heading :</td><td>
					
					<input type="text" name="top_heading"  value="<?php echo $top_heading;?>" id="top_heading" style="width:400px;"  />  </td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement middle heading :</td><td><input type="text" name="middle_heading"  value="<?php echo $middle_heading;?>" id="middle_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement timings :</td><td><input type="text" name="timings"  value="<?php echo $timings;?>" id="timings" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement background image url :</td><td><input type="text" name="background_image"  value="<?php echo $background_image;?>" id="background_image" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >catering post name :</td><td><input type="text" name="catering_postname"  value="<?php echo $catering_postname;?>" id="catering_postname" style="width:400px;"  /></td></tr>
					</table> </td></tr>
				<?php	
				} 
				if($pagename=="Contact")
				{
				    $image_url=get_post_meta($post->ID,'image_url', true);
					$googlemap_place_address=get_post_meta($post->ID,'googlemap_place_address', true);
					
				?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">Image Url :</td><td>
					
					<input type="text" name="image_url"  value="<?php echo $image_url;?>" id="image_url" style="width:400px;"  />  </td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Google Map Place Address :</td><td><input type="text" name="googlemap_place_address"  value="<?php echo $googlemap_place_address;?>" id="googlemap_place_address" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
				<?php	
				} 
				if($pagename=="Kart Racing")
				{
				    $top_heading=get_post_meta($post->ID,'top_heading', true);
					$middle_heading=get_post_meta($post->ID,'middle_heading', true);
					$background_image=get_post_meta($post->ID,'background_image', true);$link_title=get_post_meta($post->ID,'link_title', true);
					$link_url=get_post_meta($post->ID,'link_url', true);
					$instragram_heading=get_post_meta($post->ID,'instragram_heading', true);
					$follow_heading=get_post_meta($post->ID,'follow_heading', true);
				?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">advertisement top heading :</td><td>
					
					<input type="text" name="top_heading"  value="<?php echo $top_heading;?>" id="top_heading" style="width:400px;"  />  </td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement middle heading :</td><td><input type="text" name="middle_heading"  value="<?php echo $middle_heading;?>" id="middle_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement background image url :</td><td><input type="text" name="background_image"  value="<?php echo $background_image;?>" id="background_image" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement link title :</td><td><input type="text" name="link_title"  value="<?php echo $link_title;?>" id="link_title" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement link url :</td><td><input type="text" name="link_url"  value="<?php echo $link_url;?>" id="link_url" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >instragram heading :</td><td><input type="text" name="instragram_heading"  value="<?php echo $instragram_heading;?>" id="instragram_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >follow us heading :</td><td><input type="text" name="follow_heading"  value="<?php echo $follow_heading;?>" id="follow_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
				<?php	
				} 
				if($pagename=="Play")
				{
				    $top_heading=get_post_meta($post->ID,'top_heading', true);
					$middle_heading=get_post_meta($post->ID,'middle_heading', true);
					$background_image=get_post_meta($post->ID,'background_image', true);$link_title=get_post_meta($post->ID,'link_title', true);
					$link_url=get_post_meta($post->ID,'link_url', true);
					$outdoor_games_heading=get_post_meta($post->ID,'outdoor_games_heading', true);
					$outdoor_games_background=get_post_meta($post->ID,'outdoor_games_background', true);
					$exclusive_event_heading=get_post_meta($post->ID,'exclusive_event_heading', true);
					$exclusive_event_background=get_post_meta($post->ID,'exclusive_event_background', true);
					
				?>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">advertisement top heading :</td><td>
					
					<input type="text" name="top_heading"  value="<?php echo $top_heading;?>" id="top_heading" style="width:400px;"  />  </td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement middle heading :</td><td><input type="text" name="middle_heading"  value="<?php echo $middle_heading;?>" id="middle_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement background image url :</td><td><input type="text" name="background_image"  value="<?php echo $background_image;?>" id="background_image" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement link title :</td><td><input type="text" name="link_title"  value="<?php echo $link_title;?>" id="link_title" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >advertisement link url :</td><td><input type="text" name="link_url"  value="<?php echo $link_url;?>" id="link_url" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
					
					
					
					
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Outdoor Games Heading :</td><td><input type="text" name="outdoor_games_heading"  value="<?php echo $outdoor_games_heading;?>" id="outdoor_games_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Outdoor Games Background Image Url :</td><td><input type="text" name="outdoor_games_background"  value="<?php echo $outdoor_games_background;?>" id="outdoor_games_background" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Exclusive Event Heading :</td><td><input type="text" name="exclusive_event_heading"  value="<?php echo $exclusive_event_heading;?>" id="exclusive_event_heading" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Exclusive Event Background Image Url :</td><td><input type="text" name="exclusive_event_background"  value="<?php echo $exclusive_event_background;?>" id="exclusive_event_background" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
				<?php	
				} ?>
                
                 </table>
                </td></tr>
             </table>
             
             </div> <?php
}
function display_bb_meta_box()
{
	
	global $post; ?>
	<div style="padding-top:10px; padding-left:20px;">
         <table border="0" id="specialist_table" cellpadding="0" cellspacing="0">
        <?php	
			$bsimage_url=get_post_meta($post->ID,'bsimage_url', true);
		    $bsbtn_url=get_post_meta($post->ID,'bsbtn_url', true);
			$pagename=get_the_title( $post->ID );
		
			?>
			  
					<tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;">Book Session Image Url :</td><td>
					
					<input type="text" name="bsimage_url"  value="<?php if(isset($bsimage_url) && $bsimage_url == ""){echo get_bloginfo('url')."/wp-content/uploads/2015/12/kart-racing-row2-bg1.jpg";} else { echo $bsimage_url; } ?>" id="bsimage_url" style="width:400px;"  title="Book Session Image"/>  </td><td><a href="#" type="button" class="gvp_settings_upload_button button-primary" title="Select or Upload Image"/>Select Image</a></td></tr></table>
					</td></tr>
					
				  <tr><td style="vertical-align:text-top; padding-top:5px;">	
					<table border="0" ><tr><td style="width:160px;" >Book Session Button Url :</td><td><input type="text" name="bsbtn_url"  value="<?php echo $bsbtn_url;?>" id="bsbtn_url" style="width:400px;"  /></td></tr>
					</table> </td></tr>
					
			
            
             </table>
            
             </div> <?php
}


add_action( 'wp_ajax_calendar_callback', 'calendar_callback' );
add_action( 'wp_ajax_nopriv_calendar_callback', 'calendar_callback' );

function calendar_callback(){
	$month = $_REQUEST['month'];
	$year = $_REQUEST['year'];
	echo qem_show_calendar('',$month ,$year);
	die();
}

add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action( 'wp_ajax_nopriv_my_action', 'my_action_callback' );

function my_action_callback(){
	global $post;
	$cat_id=$_REQUEST['cat_id'];
	$number=$_REQUEST['number'];
	if($cat_id=="all")
	{
		$defaults = array( 'menu' => 'Blog Menu' );
			
			@$nav_items=wp_get_nav_menu_items($defaults);
			
			foreach($nav_items as $nav_cat)
			{
				$catidarray[]=$nav_cat->object_id;
			}
			
			$args = array( 'category__in' => $catidarray, 'posts_per_page'  => $number);
	}
	else{
		$args = array( 'category__in' => $cat_id, 'posts_per_page'  => $number );	
	}
	  
    query_posts( $args );
	
	//r_print_r($catPost);
	while ( have_posts() ) : the_post();

							$category = get_the_category( $post->ID );
							$cats = '';
							foreach ($category as $cats_list){
								$cats .= 'cat_'.$cats_list->slug.' '; 
							}
		?>
    
		<li class="element-item <?php echo $cats; ?>">
			<?php 
			$blog_img_url = '';
			if ( has_post_thumbnail() ) {
				$blog_img=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog_list_img' );
				$blog_img_url=$blog_img['0']; 
			} ?>
			<div class=" list-img-wrap" style="background: url(<?php echo $blog_img_url; ?>) center no-repeat; background-size: cover;">
				<div class=" list-img">
					<div class=" list-heading"> 
						<h3><?php the_title();?></h3>
						<span class=" date-text"><?php echo get_the_date(); ?></span>
					</div>
				</div>
			</div>
			<div class="list-bottom-heading clearfix">
				<div class="list-social">
					<a class="facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
					<a class="twitter customer share" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title();?>&hashtags=octaneraceway" target="_blank"><i class="fa fa-twitter"></i></a>
					<a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
				</div>
				<div class="list-read-btn clearfix">
					<a href="<?php the_permalink(); ?>" class="red-button red-button-small">READ</a>
				</div> 
			</div>
		</li>
    
    <?php endwhile;wp_reset_query();
	exit();
}
add_action( 'wp_ajax_contact_email_submit', 'email_submit' );
add_action( 'wp_ajax_nopriv_contact_email_submit', 'email_submit' );
function email_submit()
{
add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        $email_from = $_REQUEST['email']; 
		$email_body = "Dear Admin,".'<br/>';
		$email_body .= "These are my following details." .'<br/>';
		
		foreach($_REQUEST as $key =>$value)
		{
			$lable_val  = ucfirst(str_replace('_',' ',$key));
			switch($key)
			{
				
				case 'first_name':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				case 'last_name':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;					
									
								
				case 'email':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				
				case 'phone':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;						
									
				case 'message':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
														
			}
			
		}
		
		
		$email_subject= "Enquery form";
			
		$admin_email = get_option(THEME_PREFIX.'contact_email');
		$admin_email = explode(',',$admin_email);
		if(!empty($admin_email))
			$result=wp_mail($admin_email, $email_subject, $email_body );
		
		echo $result;
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		exit();
	
}
/************ book party email function ajax ***************/
add_action( 'wp_ajax_book_party_email', 'book_party_submit' );
add_action( 'wp_ajax_nopriv_book_party_email', 'book_party_submit' );
function book_party_submit()
{
add_filter( 'wp_mail_content_type', 'set_html_content_type' );
//print_r($_REQUEST);die;
        $email_from = $_REQUEST['email']; 
		$email_body = "Dear Admin,".'<br/>';
		$email_body .= "These are my following details.Please book a party" .'<br/>';
		
		foreach($_REQUEST as $key =>$value)
		{
			$lable_val  = ucfirst(str_replace('_',' ',$key));
			switch($key)
			{
				
				case 'first_name':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				case 'last_name':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;					
				case 'company':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
			    case 'zip_code':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
								
				case 'email':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				
				case 'phone':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'party_type':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'guests_count':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				
				case 'heard_about_us':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'heard_about_us_other_option':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;							
				case 'message':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
														
			}
			
		}
		
		
		$email_subject= "Book A Party";
			
		$admin_email = get_option(THEME_PREFIX.'cor_contact_email');
		$admin_email = explode(',',$admin_email);
		if(!empty($admin_email))
			$result=wp_mail($admin_email, $email_subject, $email_body );
		echo $result;
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		exit();
	
}
/************ end book party email function ajax ***************/
/************ book group event contact email function ajax start***************/
add_action( 'wp_ajax_event_contact_detail_submit', 'group_event_contact_submit' );
add_action( 'wp_ajax_nopriv_event_contact_detail_submit', 'group_event_contact_submit' );
function group_event_contact_submit()
{
add_filter( 'wp_mail_content_type', 'set_html_content_type' );
//print_r($_REQUEST['response']);die;
        $email_from = $_REQUEST['email']; 
		$email_body = "Dear Admin,".'<br/>';
		$email_body .= "These are my following details." .'<br/>';
		
		foreach($_REQUEST as $key =>$value)
		{
			$lable_val  = ucfirst(str_replace('_',' ',$key));
			switch($key)
			{
				
				case 'name':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				
								
				case 'email':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
									
				
				case 'phone':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'question':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'response':    
				                    foreach($value as $value1)
				                    {
									 $res=nl2br($value1);
									 if(!empty($response))
										 {
										 $response=$response.','.$res;
										 }
										 else{
											 $response=$res;
										 }
									}
									$email_body .= $lable_val.": ".$response.'<br/>';
									break;
				case 'response_email':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				case 'response_phone':
									$email_body .= $lable_val.": ".nl2br($value).'<br/>';
									break;
				
									
														
			}
			
		}
		
		
		$email_subject= "Contact Details";
		//echo $email_body;die;
		$admin_email = get_option(THEME_PREFIX.'gro_contact_email');
		$admin_email = explode(',',$admin_email);
		if(!empty($admin_email))
			$result=wp_mail($admin_email, $email_subject, $email_body );
		echo $result;
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		exit();
	
}
/************ group event contact email ajax end ***************/
function set_html_content_type() {
	return 'text/html';
}


function create_subscriber_table()
{
	global $wpdb,$table_prefix;
	//$test="Drop Table ".$table_prefix."subscribe_users";
	//$wpdb->query($test);
	$query="CREATE TABLE IF NOT EXISTS ".$table_prefix."subscribe_users (
							sub_id int(11) NOT NULL AUTO_INCREMENT,
							name VARCHAR(256) NOT NULL,
							email VARCHAR(256) NOT NULL,
							phone VARCHAR(100) NOT NULL,
							gender enum('male', 'female')  NOT NULL,
							sub_detail TEXT NOT NULL,
							PRIMARY KEY  (sub_id)
							) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
	$wpdb->query($query);
}
create_subscriber_table();
function create_event_custom_table()
{
	global $wpdb,$table_prefix;
	//$test="Drop Table ".$table_prefix."custom_event_table";
	//$wpdb->query($test);
	$query="CREATE TABLE IF NOT EXISTS ".$table_prefix."custom_event_table (
							event_id int(11) NOT NULL AUTO_INCREMENT,
							post_id int(11) NOT NULL,
							start_date VARCHAR(55) NOT NULL,
							end_date VARCHAR(55) NOT NULL,
							start_time VARCHAR(55) NOT NULL,
							end_time VARCHAR(55) NOT NULL,
							is_featured int(11) NOT NULL,
							PRIMARY KEY  (event_id)
							) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
	$wpdb->query($query);
}
create_event_custom_table();
if(is_admin())
{
	require_once( THEME_ADMIN_PATH . '/load.php' );
}
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
add_action( 'save_post', 'custom_save_event' );
function custom_save_event()
{
	//echo "<pre>";print_r($_REQUEST);die;
		global $post;global $wpdb;
	    if($_REQUEST['post_type']=='event' && count($_REQUEST)>1)
		{
			$post_id=$_REQUEST['post_ID'];
			$start_date=$_REQUEST['event_date'];
			$start_date=strtotime($start_date);
			$end_date=$_REQUEST['event_end_date'];
			$end_date=strtotime($end_date);
			$start_time=$_REQUEST['event_start'];
			$end_time=$_REQUEST['event_finish'];
			$featured=count($_REQUEST['post_category']);
				if($featured>1)
				{
					$is_featured="1";
				}
				else
				{
					$is_featured="0";
				}
			if($_REQUEST['action']=="editpost")
			{
			$exists_post = '';	
			$exists_post = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".$wpdb->prefix."custom_event_table WHERE 	post_id = %s ", array($post_id)));
			if(!empty($exists_post))
			{
				
				$result=$wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix."custom_event_table SET  start_date = %s,   end_date = %s,  start_time = %s,  end_time = %s,  is_featured = %s WHERE post_id = %d", array($start_date, $end_date, $start_time, $end_time, $is_featured, $exists_post->post_id)));
				
			 }
			 else
			 {
				$result=$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."custom_event_table SET post_id = %s,  start_date = %s,   end_date = %s,  start_time = %s,  end_time = %s,  is_featured = %s",  array($post_id, $start_date, $end_date, $start_time, $end_time, $is_featured))); 
			 }
			}
			else
			{
			$result=$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."custom_event_table SET post_id = %s,  start_date = %s,   end_date = %s,  start_time = %s,  end_time = %s,  is_featured = %s",  array($post_id, $start_date, $end_date, $start_time, $end_time, $is_featured)));	
			}
			
		}
}

function popup_waiver( $atts, $content = null ) {
	return '<a class="popup-with-form" href="#popup-terms">'.$content.'</a>';
}
add_shortcode( 'waiver', 'popup_waiver' );
function popup_menuboards( $atts, $content = null ) {
	return '<a class="popup-with-form hideMobileButton" href="#popup-terms2"><button class=" submit-btn1 download-btn">'.$content.'</button></a><p class="showMobileButton"><a href="/wp-content/uploads/2018/04/Brickyard-Grill-Menu-4.18.pdf" target="_blank" style="display: block;"><button class=" submit-btn1 download-btn">'.$content.'</button></a></p>';
}
add_shortcode( 'menuboards', 'popup_menuboards' );
function current_year( $atts, $content = null ) {
	return date("Y");
}
add_shortcode( 'year', 'current_year' );

function get_the_ID_id() {
    $post = get_post();
    return ! empty( $post ) ? $post->ID : false;
}

function get_popup_link ($atts, $content = "") {
	$a = shortcode_atts( array(
        'id' => 'something',
        'class' => ''
    ), $atts );
	$res = '<a href="#'.$a['id'].'" class="popup-with-form '.$a['class'].'">'.$content.'</a>';
	return $res;
}
add_shortcode( 'popup_link', 'get_popup_link' );

function private_button_func ($atts, $content = "") {
	$a = shortcode_atts( array(
        'data-value' => '',
        'class' => '',
        'href' => 'javascript:void(0)'
    ), $atts );
	$res = '<div class="idea-box">
				<a class="scroll_by_class '.$a['class'].'" href="'.$a['href'].'" data-value="'.$a['data-value'].'">'.$content.'</a>
			</div>';
	return $res;
}
add_shortcode( 'private_button', 'private_button_func' );

function get_popup_content ($atts, $content = "") {
	$a = shortcode_atts( array(
        'id' => 'something',
        'class' => '',
        'file' => ''
    ), $atts );
	if(!empty($a['file'])) {
		$res = '<div id="'.$a['id'].'" class="popupbox-main mfp-hide white-popup-block ge-btn">
					<div class="popup-content">
						<div class="popupbox-inner popupbox-inner-scroll">
							<embed width="100%" height="450" src="'.$a['file'].'" type="application/pdf">
						</div>
					</div>
				</div>';
	} else {
		$res = '<div id="'.$a['id'].'" class="popupbox-main mfp-hide white-popup-block">
					<div class="popup-content">
						<div class="popupbox-inner popupbox-inner-scroll">
							'.$content.'
						</div>
					</div>
				</div>';
	}
    return $res;
}
add_shortcode( 'popup_content', 'get_popup_content' );

function get_galleries_func ( $atts, $content = "" ) {
	$a = shortcode_atts( array(
        'id' => 'something'
    ), $atts );
	
	$img1 = ''; $img2 = ''; $img3 = '';
	if($a['id'] == 'brickyard_patio') {
		$img1 = get_field('bp_image_1', 757);
		$img2 = get_field('bp_image_2', 757);
		$img3 = get_field('bp_image_3', 757);
	}
	if($a['id'] == 'indy_daytona') {
		$img1 = get_field('id_image_1', 757);
		$img2 = get_field('id_image_2', 757);
		$img3 = get_field('id_image_3', 757);
	}
	if($a['id'] == 'billiards_room') {
		$img1 = get_field('br_image_1', 757);
		$img2 = get_field('br_image_2', 757);
		$img3 = get_field('br_image_3', 759);
	}
	if($a['id'] == 'brickyard_grill') {
		$img1 = get_field('bg_image_1', 757);
		$img2 = get_field('bg_image_2', 757);
		$img3 = get_field('bg_image_3', 757);
	}
	$res = ' <div class="images-wapper ">
				<ul class=" racing-list" id="racing-list-row1-col1">';
				if( !empty($img1) ) {
					$res .= ' 	<li class="current_thumbnail">
									<a href="javascript:void(0);" class="change_gallery" id="list-img1" data-value-small="'.$img1['sizes']['home_big'].'" data-value-big="'.$img1['url'].'">
										<img src="'.$img1['sizes']['thumbnail'].'" alt="img">
										<span>
											<img src="http://e5octane.wpengine.com/wp-content/themes/octane/images/plus-icon.png" alt="img">
										</span>
									</a>
								</li>';
				}
				if( !empty($img2) ) {
					$res .= '	<li>
									<a href="javascript:void(0);" class="change_gallery" id="list-img2" data-value-small="'.$img2['sizes']['home_big'].'" data-value-big="'.$img2['url'].'">
										<img src="'.$img2['sizes']['thumbnail'].'" alt="img">
										<span>
											<img src="http://e5octane.wpengine.com/wp-content/themes/octane/images/plus-icon.png" alt="img">
										</span>
									</a>
								</li>';
				}
				if( !empty($img3) ) {
					$res .= '	<li>
									<a href="javascript:void(0);" class="change_gallery" id="list-img3" data-value-small="'.$img3['sizes']['home_big'].'" data-value-big="'.$img3['url'].'">
										<img src="'.$img3['sizes']['thumbnail'].'" alt="img">
										<span>
											<img src="http://e5octane.wpengine.com/wp-content/themes/octane/images/plus-icon.png" alt="img">
										</span>
									</a>
								</li>';
				}
		$res .= '</ul>
				<ul class="racing-list2">
					<li>
						<a class="fancybox" data-fancybox-group="gallery" href="'.$img1['url'].'">
							<img class="target_image list-img1" src="'.$img1['sizes']['home_big'].'" alt="img">
							<img class="target_image list-img2" src="'.$img2['sizes']['home_big'].'" alt="img" style="display: none;">
							<img class="target_image list-img3" src="'.$img3['sizes']['home_big'].'" alt="img" style="display: none;">
						</a>
					</li>
				</ul>
			</div>';
	return $res;
}
add_shortcode( 'event_spaces_gallery', 'get_galleries_func' );


include 'menu-item-custom-fields-example.php';


add_filter( 'nav_menu_css_class', 'add_custom_class', 10, 2 );

function add_custom_class( $classes = array(), $menu_item = false ) {

	$custom_fields = get_post_meta($menu_item->ID);
	// die();
	if( isset($custom_fields['menu-item-field-01']) ){
	        $classes[] = 'ithasmegamenu';
	    }
    return $classes;
}

function add_menu_atts( $atts, $item, $args ) {
    //get current menu item meta
    $custom_fields = get_post_meta($item->ID);
    if( isset($custom_fields['menu-item-field-01']) ){
        $menuparentid = $item->ID;
    }
    // echo $menuparentid;
    // echo "-";
    // echo $item->menu_item_parent;
    // if ($item->menu_item_parent != 0) {
    // 	echo "yes";
    // }
    //check that custom field is stored in meta db
	// var_dump($item);
	// die();
    // if( isset($custom_fields['menu-item-field-01']) ){
	if ($item->menu_item_parent != 0) {
		// die();
		$the_query = new WP_Query(array('p' => $item->object_id,'post_type' => 'any'));
		
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
        		$atts['field-title'] = get_the_title();
        		$atts['field-link'] = get_the_permalink();
        		$atts['field-excerpt'] = substr(get_the_excerpt(), 0, 52).'...';
        		$atts['field-image'] = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
			endwhile;
			wp_reset_postdata();
		endif;
	}
    // }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_atts', 10, 3 );

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

function metaslider_flex_carousel_move($options, $slider_id, $settings) {
    $options['move'] = 1;
    return $options;
}
add_filter('metaslider_flex_slider_parameters', 'metaslider_flex_carousel_move', 10, 3);
add_filter('gform_confirmation_anchor', '__return_false');