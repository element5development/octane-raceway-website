<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en" lang="en">
<head>
  
  </title>
<!--[if IE]>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
<?php
	//get favicon icon
	 $theme_favicon = get_option(THEME_PREFIX.'favicon');
	 $theme_favicon = !empty($theme_favicon)? $theme_favicon : get_template_directory_uri().'/favicon.ico';
	?>
	<link rel="shortcut icon" href="<?php echo   $theme_favicon; ?>" />
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/parallax.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/isotope-docs.min.js"></script>
<?php wp_head();?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KML55W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KML55W');</script>
<!-- End Google Tag Manager -->

<!-- Remote Conversion Tracking Code -->  
<script type="text/javascript">
rlrctTRKDOM="rtsys.rtrk.com";
(function() {
var rct_load = document.createElement("script"); 
rct_load.type = "text/javascript"; 
rct_load.src = document.location.protocol+"//"+rlrctTRKDOM+"/rct_lct/js/rlrct1.js"; 
(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(rct_load); }
)(); </script> 
  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8477363-2', 'auto');
  ga('send', 'pageview');

</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1040903585944676');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1040903585944676&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
  <style> 
	header #special-banner {
    top: 0px!important;
}
</style>
  
  <?php
if(is_front_page()){
?>
  <style> 
.top-bar {
    background: #0f0f0f;
    border-bottom: 3px solid #2b2b2b;
    position: fixed;
    width: 100%;
    z-index: 99999;
    top: 100px !important;
}
</style>
<?php
}
  ?>

</head>

<body <?php body_class(); ?>>
<div id="page">
<header>
<?php 
$tel=get_option( THEME_PREFIX.'phone');
$map_dir_link=get_option( THEME_PREFIX.'map_direction_link');
$open_time=get_option( THEME_PREFIX.'open_timing');
$week_day=date('l');
$open_time=$open_time[$week_day];
$logo_img= get_option( THEME_PREFIX.'home_page_logo');
$logo_img = !empty($logo_img)? $logo_img : get_template_directory_uri().'/images/logo.png';

?>
<div id="header-mobile">
	<div class="row clearfix">
		<div class="logo">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_img;?>" alt="logo" width="150"></a>
		</div>
		<div class="main-nav-mobile">
			<ul>
				<?php
				if(!empty($tel)) { ?>

					<li><a href="tel:(602)302-7223"><i class="fa fa-phone"></i> <span>Call Us</span></a></li>
				<?php
				} ?>
				<li><a href="<?php echo site_url(); ?>/contact"><i class="fa fa-map-marker"></i> <span>Directions</span></a></li>
				<li>
					<a href="javascript:void(0)"><i class="fa fa-bars"></i> <span>Menu</span></a>
					<ul class="mobile-menu" style="display: none;">
						<li><span class="close-button"><i class="fa fa-times"></i></a></li>
					 
						<li><a href="<?php echo site_url(); ?>/book-a-race/">Book a Race</a></li>
						<?php
						$default = array( 'menu' => 'Middle Menu', 'container'  => '' );
						wp_nav_menu($default); ?>
						<li class="footer-right-sec">
							<?php
							$facebook_link= get_option( THEME_PREFIX.'facebook');
							$twitter_link= get_option( THEME_PREFIX.'twitter');
							$instagram_link= get_option( THEME_PREFIX.'instagram_link');
							$googleplus_link= get_option( THEME_PREFIX.'google_plus');
							$youtube_link= get_option( THEME_PREFIX.'youtube');
							if(!empty($facebook_link))
							{
								?>
								<span><a target="_blank" href="<?php echo $facebook_link;?>"><i class="fa fa-facebook"></i></a></span>
								<?php
							}
							if(!empty($twitter_link))
							{
								?>
								<span><a target="_blank" href="<?php echo $twitter_link;?>"><i class="fa fa-twitter"></i></a></span>
								<?php
							}
							if(!empty($instagram_link))
							{
								?>
								<span><a target="_blank" href="<?php echo $instagram_link;?>"><i class="fa fa-instagram"></i></a></span>
								<?php
							}
							if(!empty($youtube_link))
							{
								?>
								<span><a target="_blank" href="<?php echo $youtube_link;?>"><i class="fa fa-youtube-play"></i></a></span>
								<?php
							}
							if(!empty($googleplus_link))
							{
								?>
								<span><a target="_blank" href="<?php echo $googleplus_link;?>"><i class="fa fa-google-plus"></i></a></span>
								<?php
							} ?>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>

<?php if(is_home() || is_front_page()) { 
$special_banner= get_option( THEME_PREFIX.'inner_page_banner');
$special_banner_content= get_option( THEME_PREFIX.'site_banner_content');
?> 
<?php if(!empty($special_banner) || !empty($special_banner_content)) { ?>
<?php if ( is_user_logged_in() ) 
	{
		?>
		<style>#main-menu-sec.menufix {top: 163px !important;}
 
  </style>
		<?php
	}
	else
	{ ?>
<style>.home-banner.has-sbanner{margin-top: 93px;} #special-banner{z-index: 9999;} 
#main-menu-sec.menufix {
 top: 137px !important;
}
@media (max-width: 693px){
.home-banner.has-sbanner{margin-top: 0;}
}
</style> 
<?php } include 'sb.php'; } }?>

<div class="top-bar">
<div class=" row">
<div class=" clearfix">
<div class=" top-bar-left">
	<div class=" top-bar-left-menu">
		<ul>
				<?php
				if(!empty($tel))
				{
					?>
					<li><a href="tel:(602)302-7223"><i class="fa fa-phone"></i> <?php echo $tel?></a></li>
					<?php
				}
				?>
					<li><a href="<?php echo site_url();?>/contact"><i class="fa fa-map-markerfa fa-map-marker"> </i> Map & Directions </a></li>
				<?php	
				if(!empty($open_time))
				{
					?>
					<li><a href="http://octaneraceway.com/contact#hours" class=" active"> Open Today: <?php echo $open_time;?> </a></li>
					<?php
                               
				} ?>
		</ul>
	</div>
</div>
<div class=" top-bar-right">
<div class=" top-bar-right-menu">
<?php
$default = array( 'menu' => 'Top Menu' );
wp_nav_menu($default);
?>

</div>
<?php get_search_form();?>
<div class="book-box-head book_vr"><a href="https://www.velocityvr.com/book-now/" target="_blank">BOOK VR</a></div>
<?php
$box_title_1= get_option( THEME_PREFIX.'box_title_1');
$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
			if($box_title_1!="" && $box_title_1_link!="") { ?>
				<div class="book-box-head"><a href="<?php echo $box_title_1_link;?>"><?php echo $box_title_1;?></a></div>
			<?php
			} ?>


<!-----------
<div class=" top-bar-right-search">
<input name="" type="text" class="search" placeholder="search">
<button class=" search-btn"><i class="fa fa-search"></i></button>
</div>-->
</div>
</div>
</div>
</div><!----topbar end here--->


</header>
<!----header end here---->

<?php
if(is_home())
{
	if ( is_user_logged_in() ) 
	{
		?>
		<style>.menufix{ top:63px!important;}</style>
		<?php
	}
	else
	{
		?>
		<style>.menufix{ top:37px!important;}</style>
		<?php
	}
}
else
{
	if ( is_user_logged_in() ) 
	{
		?>
		<style>.innerpage-banner #main-menu-sec, .innerpage-banner #main-menu-sec.menufixc{ top:67px!important;}</style>
		<?php
	}
	else
	{
		?>
		<style>.innerpage-banner #main-menu-sec, .innerpage-banner #main-menu-sec.menufixc{ top:37px!important;}</style>
		<?php
	}
	
}

?>