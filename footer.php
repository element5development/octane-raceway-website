<div class="footer">
              <div class="row clearfix">
                <div class="footer-left-sec">
                  <?php
				  echo do_shortcode(get_option( THEME_PREFIX.'copy_right')."  ");
				  $default_foot = array( 'menu' => 'Footer Menu', 'after' => ' <span>|</span>' );
                  wp_nav_menu($default_foot);
				  ?>

                </div>
                <!--footer-left-sec ends here-->
                <div class="footer-left-sec" style="width:120px;" id="footer-left-secbtn">
                  	<a href="<?php echo get_site_url();?>/blog" class="footerred" style="width:119px;">BLOG</a>
                  	<a href="<?php echo get_site_url();?>/about-us" class="footerred" style="width:119px;margin-top:5px;">ABOUT</a>
                  </div>
                <div class="footer-right-sec">
                  <?php
				   $facebook_link= get_option( THEME_PREFIX.'facebook');
				   $twitter_link= get_option( THEME_PREFIX.'twitter');
				   $instagram_link= get_option( THEME_PREFIX.'instagram_link');
				   $googleplus_link= get_option( THEME_PREFIX.'google_plus');
				   $youtube_link= get_option( THEME_PREFIX.'youtube');
					if(!empty($facebook_link))
					{
						?>
						<span><a target="_blank" href="<?php echo $facebook_link;?>"><i class="fa fa-facebook"></i></a></span>
						<?php
					}
					if(!empty($twitter_link))
					{
						?>
						<span><a target="_blank" href="<?php echo $twitter_link;?>"><i class="fa fa-twitter"></i></a></span>
						<?php
					}
					if(!empty($instagram_link))
					{
						?>
						<span><a target="_blank" href="<?php echo $instagram_link;?>"><i class="fa fa-instagram"></i></a></span>
						<?php
					}
					if(!empty($youtube_link))
					{
						?>
						<span><a target="_blank" href="<?php echo $youtube_link;?>"><i class="fa fa-youtube-play"></i></a></span>
						<?php
					}
					if(!empty($googleplus_link))
					{
						?>
						<span><a target="_blank" href="<?php echo $googleplus_link;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/google-icon-white.png" alt="img"></a></span>
						<?php
					} ?>
                </div>
				
				<!-- BEGIN: Constant Contact Email List Form Button -->
				<div class="footer-right-sec">
					<!--  <a href="https://visitor.r20.constantcontact.com/d.jsp?llr=cgselrgab&amp;p=oi&amp;m=1106536188098&amp;sit=lqmp8pegb&amp;f=0f7a9b6c-280d-49b3-8bc3-2231cdaee949" class="fancybox-iframe" style="background-color: rgb(255, 255, 255); border: 1px solid rgb(91, 91, 91); color: rgb(128, 0, 0); display: inline-block; padding: 8px 10px; text-shadow: none; border-radius: 10px;">Sign-Up: Octane Newsletter</a> -->
						<a style="margin-top:12px;" href="https://visitor.r20.constantcontact.com/d.jsp?llr=cgselrgab&amp;p=oi&amp;m=1106536188098&amp;sit=lqmp8pegb&amp;f=0f7a9b6c-280d-49b3-8bc3-2231cdaee949" class="fancybox-iframe footerred" >Sign-Up: Octane Newsletter</a>
					<!-- BEGIN: Email Marketing you can trust -->
						<div id="ctct_button_footer" align="center" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;margin-top: 10px;">
						</div>
				</div>

				
              </div>
            </div><!----footer-end here---->
</div><!----page end here---->
<?php if(isset($_REQUEST['section']))
{
	$scrollclass=$_REQUEST['section'];?>
	<script type="text/javascript">
	var readyscroll= '<?php echo $scrollclass;?>';
    jQuery('html , body').animate({scrollTop:jQuery('.'+readyscroll).offset().top - 105},400);
	</script>
<?php } ?>
<script>(function() { var s = document.createElement("script");s.async = true;s.onload = s.onreadystatechange = function(){getYelpWidget("octane-raceway-scottsdale-2","300","RED","y","y","3");};s.src='http://chrisawren.com/widgets/yelp/yelpv2.js' ;var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})();</script>
<?php wp_footer();?>
<a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>
<?php
	$popup_contents = get_field('popup_contents', get_the_ID_id()); 
	if(isset($popup_contents)) {
		echo do_shortcode($popup_contents);
	}
?>
<style type="text/css">
.owl-nav {
	width: 100%;
	top: 40%;
	position: absolute !important;
}
.follow-bg .owl-nav {
	display: none !important;
}
.btn {
    color: #fff !important;
    text-decoration: none !important;
	background: #d62129;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    padding: 12px 15px;
    border-bottom: 5px rgba(143, 24, 36, 0.95) solid;
}
#qem-calendar .day,
#qem-calendar .oldday {
    background: #fff;
}
.kart-race-contant .content ul li {
	list-style: inherit;
	list-style-position: inside;
}
.owl-prev {
	position: relative;
	margin-left: -20px !important;
}
.owl-next {
	margin-right: -20px !important;
}
<?php if (is_page('contact')): ?>
.gform_wrapper .top_label .gfield_label {
	display: none;
}
<?php endif ?>
</style>
	
</body>
<script src="http://i.simpli.fi/dpx.js?cid=25&action=100&segment=3454272&m=1"></script>
</html>