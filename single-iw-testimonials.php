<?php
/**
 * Template Name: contact
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
?>

<?php
	// Get archive header images
	$images = get_field('test_arc_header_background_image', 'option');
	// Get random image from 'images' array
	$rand = array_rand($images, 1);
	// Get URL for random image
	$randUrl = $images[$rand]['url'];
?>

<script>
base_path='<?php echo get_template_directory_uri().'/images/';?>';
</script>
	 <div id="banner" class="innerpage-banner">
		  <div class="about-us-banner parallax-banner" <?php if(!empty($randUrl)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $randUrl; ?>) no-repeat scroll center center / cover ;"<?php } ?>>
           <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							
								<div class="testi-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
									echo "<div><h1>";
									echo "<span class='trans-bg'>". get_field('test_arc_header_title', 'option') ."</span>";
									echo "</h1></div>";
									echo "<span class='test-description'>" . get_field('test_arc_header_description', 'option') . "</span>";
								?>
							
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
          </div>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
     </div>  <!--about-us-banner ends here-->

    <!-- Begin IW Testominials Section -->

	<div id="blogpage"> 
		<div class=" row clearfix">
			<div class="content">
				<div class="blog-page-main">

				    <?php
				    if (have_posts()) : while (have_posts()) : the_post();
 
						echo '<div class="testi-post-navigation">';
							echo '<div>' . previous_post_link('&laquo; %link', 'Previous') . '</div>';
							echo '<div>' . next_post_link('%link &raquo;', 'Next') . '</div>';
						echo '</div>';
						// Create 'display_date' from ISO date setup in custom field
						$display_date = date("F jS, Y", strtotime( get_field('review_date') ));
						// Wrap each single testimonial post
						echo '<div class="testi-single-wrap clearfix">';
							// Image wrap
							echo '<div class="testi-single-img">';
								if ( has_post_thumbnail() ) {
								    echo the_post_thumbnail('medium');
								}
								else {
									echo '<img src="' . get_field('testimonials_default_image', 'option') . '">';
								}
							echo '</div>';
							// Content Wrap
							echo '<div class="testi-single-content">';
								// Get testimonial title and wrap in link to testimonial post
								echo '<h3>' . get_the_title() . '</h3>';
								// Schema.org wrap
								echo '<div itemscope itemtype="http://schema.org/Review">';
									// Get 'reviewed_by' field and display with date
								  	echo '<span itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name">' . get_field('review_by') . '</span></span> <span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization"> reviewed <span itemprop="name">Octane Raceway</span></span> on <span><meta itemprop="datePublished" content="' . get_field('review_date') . '">' . $display_date . '</span>';
								  	// Schema.org rating wrap
								  	echo '<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
									  	echo '<span class="rating-stars">';

									  	// Divide rating into whole stars and half stars
									  	$whole_stars = floor( get_field('review_rating') );
									  	$half_stars = ceil(fmod(get_field('review_rating'), 1)); 

									  	// Display icons for star rating
										for ($i=1; $i<=$whole_stars; $i++) {
											echo '<i class="fa fa-star"></i>';
										}
										if ( $half_stars > 0 ) {
											echo '<i class="fa fa-star-half"></i>';
										}	 

									  	echo '<span> ';
									  	// Display rating value and add appropriate schema
									  	echo 'Rating: <span itemprop="ratingValue">' . get_field('review_rating') . '</span>';
								  	echo '</div>';
								  	// Display 'review_quote'
								  	echo '<div itemprop="reviewBody"><i class="fa fa-quote-left"></i>' . get_field('review_quote', false, false) . '<i class="fa fa-quote-right"></i></div>';
							  	echo '</div>';
							echo '</div>';
							echo '<div class="testi-editor-content">' . get_the_content() . '</div>';
							echo '<div class="testimonial-buttons"><a class="button-default" href="http://octaneraceway.com/feedback/">Review Us</a><a class="button-default" href="http://octaneraceway.com/book-a-race/">Book A Race</a></div>';
						echo '</div>';
						?>
						<div class="list-social">
							<a class="facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
							<a class="twitter customer share" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title();?>&hashtags=octaneraceway" target="_blank"><i class="fa fa-twitter"></i></a>
							<a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
						</div>
						<div class="clearfix idea-box-main">
							<div class="idea-box">
								<a href="http://www.octaneraceway.com/testimonials">All Testimonials</a>
								<?php previous_post_link('%link', 'Previous'); ?>
								<?php next_post_link( '%link', 'Next' ); ?>
							</div>
						</div>
						<?php
						endwhile;
					endif; 
					?>

				</div>
			</div>
	<!-- End IW Testominials Section -->	
			<div id="sidebar">
				<h2>CUSTOMER REVIEWS</h2>
				<div class="sidebar-box tripadvisor">
					<div id="TA_selfserveprop392" class="TA_selfserveprop">
						<ul id="VpEj6jU3F" class="TA_links 1RqK371zDQeG">
							<li id="sGfazHHiZx" class="sJ5NI3b6fSdE">
								<a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
							</li>
						</ul>
					</div>
						<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=392&amp;locationId=2243652&amp;lang=en_US&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
			        </div>
				</div>	
			</div>
		</div>
	</div>



<div class="iw-review-us">
	<div  class=" hours-heading-sec">
		<div class="row"> 
			<h2>REVIEW US</h2>
		</div>
	</div>
	<div class="testi-arc-footer" style="height: 300px;background-image: url('<?php the_field('test_arc_footer_background_image', 'option') ?>');">
	 	<div class="testi-arc-footer-text-wrap">
	 		<h2 style="padding-top: 100px;"><?php the_field('test_arc_footer_title', 'option'); ?></h2>
	 		<p><?php the_field('test_arc_footer_description', 'option'); ?></p>
	 		<a class="button-default review-us" href="<?php the_field('test_arc_footer_button_link', 'option') ?>"><?php the_field('test_arc_footer_button_text', 'option') ?></a>
	 	</div>
	</div>
</div><!----review-us-ends-here--->

<div class=" follow-bg">
<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } else {
$instagram_link= get_option( THEME_PREFIX.'instagram_link');
if($instagram_link!="")
{
	?>
	<div class=" row">
		<div class="follow-content">
			<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
		</div>
	</div>
	
	<?php }
}
?>
	
</div><!---follow-bg end here---->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/validation-index.js"></script>
<?php
get_footer();
?>