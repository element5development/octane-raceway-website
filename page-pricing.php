<?php
/**
 * Template Name: pricing
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
                  <div class="calendar-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
                   <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
                  </div>
                  <?php //include 'sb.php'; 
                  ?>
        <div id="main-menu-sec" class="menu-innerpage">
        <div class=" row clearfix">
        <?php
							$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
							if(!empty($inner_logo_img))
							{
								?>
								<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
								<?php	
							}
							?>
				<div id="main-menu">
				<?php
										$defaults = array( 'menu' => 'Middle Menu' );
										wp_nav_menu($defaults);
										?>
				</div>
				<?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
								if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
								<?php
								} ?>
        </div>
		<?php include 'mm.php'; ?>
		</div>
  </div>

<ul class="parallax-menu-buttons">
  <li>
    <a class="scroll_by_class " href="javascript:void(0)" data-value="racing-license ">RACING MEMBERSHIPS</a>
  </li>
  <!-- <li>
    <a class="scroll_by_class " href="javascript:void(0)" data-value="annual-license-sec">ANNUAL LICENSE SPECIALS</a>
  </li> -->
  <li>
    <a class="scroll_by_class " href="javascript:void(0)" data-value="junior-racing-sec">ADULT & JUNIOR RACING</a>
  </li>
</ul>

<!--racing-license START here-->
<?php
$rlr_title = get_field('rlr_title', 414);
$rlr_row_background = get_field('rlr_row_background', 414);
$rlr_row_background_parallax = get_field('rlr_row_background_parallax', 414);

$_background = '';
if(!empty($rlr_row_background)) {
	if($ppffr_row_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$rlr_row_background.'"';
	}else{
		$_background = 'style="background: url('.$rlr_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div class="racing-license <?php echo (($rlr_row_background_parallax == 'parallaxon' && !empty($rlr_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="row">
		<div class="racing-license-in txtcenter">
			<h2><?php echo $rlr_title; ?></h2>
			<div class="racing-value-main">
				<div class="clearfix common2">
					<?php 
					for ($rlr = 1; $rlr <= 3; $rlr++) :
						$rlr_title = get_field('rlr_o'.$rlr.'title', 414);
						$rlr_price = get_field('rlr_o'.$rlr.'price', 414);
						$rlr_content = get_field('rlr_o'.$rlr.'content', 414);
						$rlr_button_title = get_field('rlr_o'.$rlr.'button_title', 414);
						$rlr_button_url = get_field('rlr_o'.$rlr.'button_url', 414);
					?>

          <?php if ( $rlr_title ) { //NO TITLE DONT DISPLAY TABLE ?>
						<div class="racing-value <?php echo (($rlr == 2)? 'racing-best-value' : ''); ?>">
							<?php if($rlr == 2) : ?>
								<div class="racing-best-value-head">
									<h2>Best Value</h2>
								</div>
							<?php endif; ?>
							<h4><?php echo $rlr_title; ?></h4>
							<span class="price <?php echo (($rlr != 2)? 'box'.$rlr : ''); ?>"><?php echo $rlr_price; ?></span>
							<?php echo $rlr_content; ?>
							<?php if( !empty($rlr_button_title) ) { ?>
								<a href="<?php echo (!empty($rlr_button_url)? $rlr_button_url : 'javascript:void(0)'); ?>"><?php echo $rlr_button_title; ?></a>
							<?php } ?>
							<?php if($rlr == 3) : ?>
								<div class="vip-tag">
									<img src="<?php bloginfo('template_url'); ?>/images/vip-tag-img.png" alt="img">
								</div>
							<?php endif; ?>
						</div><!--racing-value ends here-->
          <?php } ?>

					<?php endfor; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--racing-license ends here-->
<!--annual-license-sec START here-->
<?php
$alsi_title = get_field('alsi_title', 414);
$alsi_row_background = get_field('alsi_row_background', 414);
$row_background_parallaxon = get_field('row_background_parallaxon', 414);

$_background2 = '';
if(!empty($alsi_row_background)) {
	if($row_background_parallaxon == 'parallaxon') {
		$_background2 = 'data-parallax="scroll" data-position="top" data-image-src="'.$alsi_row_background.'"';
	}else{
		$_background2 = 'style="background: url('.$alsi_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background2 = 'style="background: #FFF;"';
}
?>
<?php if ( $alsi_title ) { //no content dont display ?>
  <div class="annual-license-sec txtcenter <?php echo (($row_background_parallaxon == 'parallaxon' && !empty($alsi_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background2; ?>>
  	<img src="<?php echo get_template_directory_uri();?>/images/white-down-arrow.png" alt="img">
  	<div class="row">
  		<div class="annual-license-sec-in">
  			<h2><?php echo $alsi_title; ?></h2>
  			<div class="clearfix common3"> 
  				<?php 
  				for ($alsi = 1; $alsi <= 7; $alsi++) :
  					$day = "";
  					if($alsi == 1) { $day = 'MON'; }
  					if($alsi == 2) { $day = 'TUE'; }
  					if($alsi == 3) { $day = 'WED'; }
  					if($alsi == 4) { $day = 'THU'; }
  					if($alsi == 5) { $day = 'FRI'; }
  					if($alsi == 6) { $day = 'SAT'; }
  					if($alsi == 7) { $day = 'SUN'; }
  					$alsi_ctitle = get_field('alsi_c'.$alsi.'title', 414);
  					$alsi_cprice = get_field('alsi_c'.$alsi.'price', 414);
  					$alsi_ctag_line = get_field('alsi_c'.$alsi.'tag_line', 414);
  					$alsi_ctag_line = get_field('alsi_c'.$alsi.'tag_line', 414);
  				?>
  					<div class="one-seventh">
  						<h2><?php echo $day; ?></h2>
  						<div class="one-seventh-cont">
  							<img src="<?php bloginfo('template_url'); ?>/images/down-red-arrow.png" alt="img" />
  							<h4><?php echo $alsi_ctitle; ?></h4>
  							<?php echo (!empty($alsi_cprice)? '<span class="cont-price">'.$alsi_cprice.'</span>' : ''); ?>
  							<?php echo $alsi_ctag_line; ?>
  						</div>
  					</div>
  				<?php endfor; ?>
  			</div>
  		</div>
  	</div>
  </div>
<?php } ?>
<!--annual-license-sec ends here-->
<!--JUNIOR RACING START here-->
<?php
$jro_title = get_field('jro_title', 414);
$jro_row_background = get_field('jro_row_background', 414);
$jro_row_background_parallax = get_field('jro_row_background_parallax', 414);

$_background3 = '';
if(!empty($jro_row_background)) {
	if($jro_row_background_parallax == 'parallaxon') {
		$_background3 = 'data-parallax="scroll" data-position="top" data-image-src="'.$jro_row_background.'"';
	}else{
		$_background3 = 'style="background: url('.$jro_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background3 = 'style="background: #FFF;"';
}
?>
<div class="junior-racing-sec txtcenter <?php echo (($jro_row_background_parallax == 'parallaxon' && !empty($jro_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background3; ?>>
	<div class="row">
		<h2><?php echo $jro_title; ?></h2> 
    <p>Races are for one person (Adults first to 14 laps) (Juniors first to 10 laps)</p>
		<div class="clearfix common3">
			<?php 
			for ($jro = 1; $jro <= 5; $jro++) :
				$jro_otitle = get_field('jro_o'.$jro.'title', 414);
				$jro_oprice = get_field('jro_o'.$jro.'price', 414);
				$jro_oday = get_field('jro_o'.$jro.'day', 414);
				$jro_orules = get_field('jro_o'.$jro.'rules', 414); ?>
				<div class="one-fifth junior-racing <?php echo (($jro == 3)? 'junior-racing-popular' : ''); ?>" style="min-height: 125px;">
					<?php if($jro == 3): ?>
						<div class="racing-best-value-head">
							<h2>MOST POPULAR</h2>
						</div>
					<?php endif; ?>
					<h4><?php echo $jro_otitle; ?></h4>
					<span class="price" style="border: none;"><?php echo $jro_oprice; ?> <small><?php echo $jro_oday; ?></small></span>
					<?php echo $jro_orules; ?>
				</div>
			<?php endfor; ?>
		</div>
	</div>
</div>
<!--JUNIOR RACING END here-->

        <?php 
} } ?>  
        
        <div class=" regular-hours-bg">
                <div  class=" hours-heading-sec">
                <div class="row"> 
                <h2>HOURS</h2>
                </div>
                </div>
                <div class=" row">
                <div class=" regular-hours-in clearfix">
                <?php if ( is_active_sidebar( 'contact_regular_hours_sidebar' ) ) {  dynamic_sidebar( 'contact_regular_hours_sidebar' );  } ?>
                </div>
                </div>
		</div><!----regular-hours end here--->
		<?php if ( is_active_sidebar( 'contact_holiday_hours_sidebar' ) ) { ?>
        <div class=" holiday-bg">
            <div class=" row">
            <div class=" regular-hours-in clearfix">
            <?php   dynamic_sidebar( 'contact_holiday_hours_sidebar' ); ?>
            </div>
            </div>
		</div><!----holiday-sec end here--->
		<?php } ?>
<?php
get_footer();
?>