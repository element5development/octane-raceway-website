jQuery(document).ready(function() {	    
	
			  
			  
	jQuery('a.scroll_by_class').click(function(){
		var a = jQuery(this).attr('data-value');
		jQuery('html , body').animate({scrollTop:jQuery('.'+a).offset().top - 99 },400);	
	});
	jQuery('a.scroll_by_id').click(function(){
		var a = jQuery(this).attr('data-value');
		jQuery('html , body').animate({scrollTop:jQuery('#'+a).offset().top - 99 },400);	
	});
	  
	jQuery('.change_image').click(function(){
		var full_imagepath_small = jQuery(this).attr('data-value-small');
		var full_imagepath_big = jQuery(this).attr('data-value-big');

		var getID = jQuery(this).parent().parent().attr('id');
		jQuery('#'+getID+' li').removeClass('current_thumbnail');

		//alert(full_imagepath);
		jQuery(this).parents('.racing-col-2').find('a.fancybox').attr('href', full_imagepath_big);
		jQuery(this).parents('.racing-col-2').find('img.target_image').hide();

		var imageID = jQuery(this).attr('id');
		jQuery(this).parents('.racing-col-2').find('img.'+imageID).fadeIn();

		jQuery(this).parent().addClass('current_thumbnail');
	});
	  
	jQuery('.change_gallery').click(function(){
		var full_imagepath_small = jQuery(this).attr('data-value-small');
		var full_imagepath_big = jQuery(this).attr('data-value-big');

		var getID = jQuery(this).parent().parent().attr('id');
		jQuery('#'+getID+' li').removeClass('current_thumbnail');

		//alert(full_imagepath);
		jQuery(this).parents('.images-wapper').find('a.fancybox').attr('href', full_imagepath_big);
		jQuery(this).parents('.images-wapper').find('img.target_image').hide();

		var imageID = jQuery(this).attr('id');
		jQuery(this).parents('.images-wapper').find('img.'+imageID).fadeIn();

		jQuery(this).parent().addClass('current_thumbnail');
	});
	  
	jQuery('.changeimage').click(function(){
		var full_imagepath_small = jQuery(this).attr('data-value');
		var full_imagepath_big = jQuery(this).attr('data-value-big');
		
		//alert(full_imagepath);
		jQuery(this).parents('#group_imgwrap').find('a.fancybox').attr('href', full_imagepath_big);
		jQuery(this).parents('#group_imgwrap').find('img.target_image').hide();

		var imageID = jQuery(this).attr('id');
		jQuery(this).parents('#group_imgwrap').find('img.'+imageID).fadeIn();
	});
	
	// $( ".main-submenu" ).hover(function() {
	// 	$( '.main-submenu' ).show( );
	// 	$(this).addClass('hovered_submenu');
	// });
	$("li.ithasmegamenu").find('li').each(function(){
		$(this).parent().append('<li class="showme"><a href="'+$(this).find('a').attr('field-link')+'"><img src="'+$(this).find('a').attr('field-image')+'" alt="'+$(this).find('a').attr('field-title')+'"><span>'+$(this).find('a').attr('field-title')+'</span><p>'+$(this).find('a').attr('field-excerpt')+'</p></a></li>');
	});
	$("ul.sub-menu").wrapInner("<div class='row'></div>");
	// $( "li.ithasmegamenu" ).hover(function() {
	// 	$('.main-submenu').show();
	// 	// $(this).addClass('hovered_submenu');
	// 	// var abc = $('#theGrandMenu').html();
	// 	// alert(abc);
	// 	// if (abc == '') {
	// 		$(this).find('li').each(function(){
	// 			$('#theGrandMenu').append('<li><a href="'+$(this).find('a').attr('field-link')+'"><img src="'+$(this).find('a').attr('field-image')+'" alt="'+$(this).find('a').attr('field-title')+'"><span>'+$(this).find('a').attr('field-title')+'</span><p>'+$(this).find('a').attr('field-excerpt')+'</p></a></li>');
	// 		});
	// 	// };
	// }, function(){
	// 	$('.main-submenu').hide();
	// 	// $( '.main-submenu' ).hide( );
	// 	// $(this).removeClass('hovered_submenu');
	// 	$('#theGrandMenu').html('');

	// });
				// $('#theGrandMenu li, #theGrandMenu li a').mouseover(function(){
				// 	$('.main-submenu').show();
				// });

	$('li.ithasmegamenu, .main-submenu, #theGrandMenu, #theGrandMenu li').mouseover(function(){
		// $('.main-submenu').show();
	});
	$(document).mouseout(function(){
		// $('#theGrandMenu').html('');
		// $('.main-submenu').hide();
		// alert();
	});
// $( "li.ithasmegamenu, .main-submenu" ).mouseover(function() {
// 		$( '.main-submenu' ).show( );
// 		$(this).addClass('hovered_submenu');
// 		$(this).find('li').each(function(){
// 			$('#theGrandMenu').append('<li><a href="'+$(this).find('a').attr('field-link')+'"><img src="'+$(this).find('a').attr('field-image')+'" alt="'+$(this).find('a').attr('field-title')+'"><span>'+$(this).find('a').attr('field-title')+'</span><p>'+$(this).find('a').attr('field-excerpt')+'</p></a></li>');
// 		});
// }).mouseout(function() {
// 		$( '.main-submenu' ).hide( );
// 		$(this).removeClass('hovered_submenu');
// 		$('#theGrandMenu').html('');
// });

	
	if ($(".calmonth").length > 0) {
		var months = { January:1, February:2, March:3, April:4,
						May:5, June:6, July:7, August:8,
						September:9, October:10, November:11, December:12 };
		
		for (i = 0; i < 12; i++) {
			$('.months_menu').removeClass('month-'+i);
		}
		
		var monthNum = $(".calmonth h2").text().split(' ')[0];
		$('#prev_month').addClass('month-'+(months[monthNum]-1)+'_wrap');
		$('#next_month').addClass('month-'+(months[monthNum]+1)+'_wrap');
		$('.months_menu').addClass('month-'+months[monthNum]);
		$('.months_menu li:nth-of-type('+months[monthNum]+')').addClass('current_month');
    }
	
	$('#prev_month').click(function(e){
		e.preventDefault();
		
		prev_month = $(this).attr('class').split('_')[0].split('-')[1];
		
		if(prev_month != 1) {
			for (i = 0; i < 13; i++) {
				$('#prev_month').removeClass('month-'+i+'_wrap');
				$('#next_month').removeClass('month-'+i+'_wrap');
			}
			$('#prev_month').addClass('month-'+(parseInt(prev_month)-1)+'_wrap');
			$('#next_month').addClass('month-'+(parseInt(prev_month)+1)+'_wrap');
			for (i = 0; i < 12; i++) {
				$('.months_menu').removeClass('month-'+i);
			}
			$('.months_menu').addClass('month-'+prev_month);
		}
	});
	
	$('#next_month').click(function(e){
		e.preventDefault();
		
		next_month = $(this).attr('class').split('_')[0].split('-')[1];
		
		if(next_month != 12) {
			for (i = 0; i < 13; i++) {
				$('#prev_month').removeClass('month-'+i+'_wrap');
				$('#next_month').removeClass('month-'+i+'_wrap');
			}
			$('#prev_month').addClass('month-'+(parseInt(next_month)-1)+'_wrap');
			$('#next_month').addClass('month-'+(parseInt(next_month)+1)+'_wrap');
			for (i = 0; i < 12; i++) {
				$('.months_menu').removeClass('month-'+i);
			}
			$('.months_menu').addClass('month-'+next_month);
		}
	});
	
	$('.calendar-contant-right .tabs ul li a').click(function(event) {
		event.preventDefault();
		//$('#qem-calendar').load( $(this).attr('href') );
	});
	
	$('.main-nav-mobile > ul > li:last-of-type > a').click(function(event) {
		event.preventDefault();
		$(this).parent().find('.mobile-menu').slideDown();
	});
	$('.main-nav-mobile ul li ul.mobile-menu li span.close-button').click(function(event) {
		event.preventDefault();
		$(this).parents('.mobile-menu').slideUp();
	});
	
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	
	//IF USER USING MOBILE DEVICES
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$('a.popup-with-form').each(function() {
			var popup_id = $(this).attr('href').split('#')[1];
			var popup_class = $(this).attr('class');
			var title = $(this).html();
			var pdf_url = $('#'+popup_id).find('embed').attr('src');
			if(pdf_url != undefined) {
				$(this).replaceWith('<a href="'+pdf_url+'" class="'+popup_class+'" download>'+title+'</a>');
			}
		});
	}
	else{
	jQuery('.popup-with-form').magnificPopup();
	}		  
});
	
jQuery('.go-to-bottom').click(function(){
	$("html, body").animate({ scrollTop: $(document).height() }, 1000);
});

jQuery(window).bind("scroll",function(){
	a=jQuery(".home-banner");
	b=jQuery("#main-menu-sec");
	jQuery(window).scrollTop()>a.height()?(b.addClass("menufix")):(b.removeClass("menufix"));
});
///jQuery(window).bind("scroll",function(){
///	a=jQuery(".home-banner");
//	b=jQuery("#special-banner");
	//jQuery(window).scrollTop()>a.height()?(b.addClass("has-sbanner")):(b.removeClass("has-sbanner"));
//});
//jQuery(window).bind("scroll",function(){
//	a=jQuery("#special-banner");
//	b=jQuery("#special-banner");
//	c=jQuery("#main-menu-sec");
//	jQuery(window).scrollTop()>a.height()?(b.addClass("menufixinner")):(b.removeClass("menufixinner"));
//	jQuery(window).scrollTop()>a.height()?(c.addClass("menufixc")):(c.removeClass("menufixc"));
//});

jQuery(window).scroll(function() {
   if(jQuery(window).scrollTop()) {
		jQuery('.spinit').removeClass('TscrolledTransition');
		jQuery('.spinit, .spinit > img').addClass('Tscrolled');
		setTimeout(function(){
			jQuery('.spinit').addClass('TscrolledTransition');
			jQuery('.spinit, .spinit > img').removeClass('Tscrolled');
		}, 500);
   }
});

jQuery( window ).load(function() {
	setTimeout(function(){
		jQuery('.line-from-left').addClass('active_line');
		jQuery('.line-from-right').addClass('active_line');
		jQuery('.black-line-banner').fadeIn('slow');
	}, 1000);
});

jQuery(document).ready(function() {
    if ( $(this).width() > 1023 ) {
        setTimeout(function(){
		var width = jQuery("#owl-carousel-849 .owl-item").width();
        var width2 = ( width + 18 ) + "px";
        jQuery( "#owl-carousel-849 .owl-item" ).css( "width", width2 );
	}, 1000);
    }
});

jQuery( window ).resize(function() {
   if ( $(this).width() > 1023 ) {
        setTimeout(function(){
		var width = jQuery("#owl-carousel-849 .owl-item").width();
        var width2 = ( width + 18 ) + "px";
        jQuery( "#owl-carousel-849 .owl-item" ).css( "width", width2 );
	}, 1000);
    }
});

jQuery(document).ready(function(){
     if(jQuery( window ).width() <= 420){
     jQuery( ".fts-jal-fb-group-display.fts-simple-fb-wrapper" ).insertAfter( "#more_list_post" );
   }
});

jQuery(document).ready(function(){
     if(jQuery( window ).width() > 420){
     $( '<div class="row clearfix"></div>' ).insertAfter( ".blog-page-main" );
   }
});

$( window ).resize(function() {
  if($( window ).width() <= 420){
     $( ".fts-jal-fb-group-display.fts-simple-fb-wrapper" ).insertAfter( "#more_list_post" );
   }
   if($( window ).width() > 420){
     $( ".fts-jal-fb-group-display.fts-simple-fb-wrapper" ).insertBefore( "#more_list_post" );
     $( '<div class="row clearfix"></div>' ).insertAfter( ".blog-page-main" );
   }
});
jQuery(document).ready(function() {
jQuery('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});
});
jQuery(document).ready(function() {
	jQuery(".contact-form.conform").replaceWith(jQuery(".row.gform-events"));
});
jQuery(document).ready(function() {
jQuery('.foogallery-owl-carousel .owl-item a').removeClass('fancybox').addClass('nofancybox');
});
function calRep(){	
	if(jQuery( window ).width() > 668){
		
	jQuery(".simcal-event-title").each(function(){
 	   var attrT = jQuery( this ).html();
     jQuery( this ).attr( "data-attr",attrT );
   });
 jQuery("li.simcal-event.simcal-events-calendar-869.simcal-tooltip").mouseover(function(){
      	//e.preventDefault();
        var childDiv= jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").clone();
        var childat = jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").children(".simcal-event-title").data("attr");
        jQuery( ".calender-contant-left-cont" ).html( childDiv );
        var childat = jQuery(".calender-contant-left .simcal-event-title").data("attr");
        jQuery(".calender-contant-left .simcal-event-title").text(childat);
        jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
    });
 jQuery(".simcal-day-has-events .simcal-day-number").mouseover(function(){
 	     //e.preventDefault();
       var childDiv1= jQuery(this).next(".simcal-events").children().children().next("div.simcal-event-details.simcal-tooltip-content").clone();
       var childat2 = jQuery(this).next(".simcal-events").children().children().next("div.simcal-event-details.simcal-tooltip-content").children(".simcal-event-title").data("attr");
       jQuery( ".calender-contant-left-cont" ).html( childDiv1 );
       var childat2 = jQuery(".calender-contant-left .simcal-event-title").data("attr");
       jQuery(".calender-contant-left .simcal-event-title").text(childat2);
       jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
    });
    
    jQuery( "#qem-calendar .simcal-event-title" ).text( "•" );
  }
 jQuery('.simcal-week-day').addClass('calday');
 jQuery('.simcal-day-void ').addClass('blankday');
 jQuery('.simcal-past ').addClass('oldday');
 jQuery('.simcal-past.simcal-day ').addClass('day oldday');
 jQuery('.simcal-future.simcal-day ').addClass('day ');
 jQuery('.simcal-present.simcal-day ').addClass('day ').attr('id', 'current_day');
 //jQuery( "#qem-calendar .simcal-event-title" ).text( "•" );
 jQuery(".calender-contant-left-in").css({'min-height':($(".calendar-contant-right").height()+'px')});
 //$('.day .simcal-day-label').each(function(){
 //$(this).replaceWith(function() {
 	//var urltxt = $(this).text();
   // var url = "http://e5octane.wpengine.com/book-race/";
    //return '<a href="' + url + '">' + urltxt + '</a>';
 //});
 //});
 jQuery( ".simcal-future.simcal-day-has-events:first .simcal-event:first" ).trigger("mouseenter");
 jQuery( ".simcal-present.simcal-day-has-events:first .simcal-event:first" ).trigger("mouseenter");
 jQuery( ".calendar-main" ).css( "display","block" );
}

jQuery(document).ready(function() {
 calRep();
});

//jQuery(window).resize(calRep);

jQuery(document).ready(scrollEvent);
//jQuery(window).resize(scrollEvent);

function scrollEvent() {
    if(jQuery( window ).width() <= 668){
    	
    	jQuery(".simcal-event-title").each(function(){
 	      var attrf = jQuery( this ).html();
       jQuery( this ).attr( "data-attr",attrf );
      });
    	
    	jQuery(".simcal-future.simcal-day-has-events:first .simcal-event:first, .simcal-present.simcal-day-has-events:first .simcal-event:first").one("mouseover", function(){
      	//e.preventDefault();
        var childDiv= jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").clone();
        var childat = jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").children(".simcal-event-title").data("attr");
        jQuery( ".calender-contant-left-cont" ).html( childDiv );
        var childat = jQuery(".calender-contant-left .simcal-event-title").data("attr");
        jQuery(".calender-contant-left .simcal-event-title").text(childat);
        jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
    });
    
    	
    jQuery("li.simcal-event.simcal-events-calendar-869.simcal-tooltip").click(function() {
       var childDiv= jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").clone();
       var childat = jQuery(this).children().next("div.simcal-event-details.simcal-tooltip-content").children(".simcal-event-title").data("attr");
       jQuery( ".calender-contant-left-cont" ).html( childDiv );
       var childat = jQuery(".calender-contant-left .simcal-event-title").data("attr");
       jQuery(".calender-contant-left .simcal-event-title").text(childat);
       jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
    jQuery('html, body').animate({
        scrollTop: jQuery(".calender-contant-left").offset().top
    }, 800);
    $('body').stop().animate();
    jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
   });

    jQuery(".simcal-day-has-events .simcal-day-number").click(function(){
 	     //e.preventDefault();
       var childDiv1= jQuery(this).next(".simcal-events").children().children().next("div.simcal-event-details.simcal-tooltip-content").clone();
       var childat2 = jQuery(this).next(".simcal-events").children().children().next("div.simcal-event-details.simcal-tooltip-content").children(".simcal-event-title").data("attr");
       jQuery( ".calender-contant-left-cont" ).html( childDiv1 );
       var childat2 = jQuery(".calender-contant-left .simcal-event-title").data("attr");
       jQuery(".calender-contant-left .simcal-event-title").text(childat2);
       jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
       jQuery('html, body').animate({
       scrollTop: jQuery(".calender-contant-left").offset().top
    }, 800);
    $('body').stop().animate();
    jQuery(".simcal-event-details.simcal-tooltip-content" ).not( ":first" ).css("display","none");
    });
    jQuery( ".simcal-future.simcal-day-has-events:first .simcal-event:first" ).trigger("mouseover");
    jQuery( ".simcal-present.simcal-day-has-events:first .simcal-event:first" ).trigger("mouseover");
    //jQuery( ".cd-top" ).trigger("click");
    jQuery( "#qem-calendar .simcal-event-title" ).text( "•" );
}
}


jQuery(window).load(function() {
 jQuery(".calender-contant-left-in").css({'min-height':($(".calendar-contant-right").height()+'px')});
});

jQuery(document).ready(function() {
	jQuery('.parallax-mirror:last').addClass('parallax-mirror2');
$(window).scroll(function() {    
  a=jQuery(".innerpage-banner");
	b=jQuery(".parallax-mirror:last");
	jQuery(window).scrollTop()<a.height()?(b.addClass("parallax-mirror2")):(b.removeClass("parallax-mirror2"));
});
});
jQuery(window).load(function() {
 jQuery("#foogallery-gallery-871 .foo-item a img").css({'min-height':($("#foogallery-gallery-871").height()+'px !important')});
 //jQuery( ".owl-prev" ).prependTo( ".owl-theme" ).attr('style', 'display: inline-block !important');
 $(".foogallery-owl-carousel .owl-prev").each(function() {
  $(this).closest(".owl-theme").prepend(this);
  $(".owl-prev").attr('style', 'display: inline-block !important');
});
jQuery("#foogallery-gallery-871 .owl-prev").css({'display':'none !important'});
});
jQuery(window).resize(function() {
 jQuery("#foogallery-gallery-871 .foo-item a img").css({'min-height':($("#foogallery-gallery-871").height()+'px !important')});
 jQuery("#foogallery-gallery-871 .owl-prev").css({'display':'none !important'});
});

jQuery(document).ready(function() {
if ( $(window).width() < 694) { $('#special-banner').detach().appendTo('#header-mobile').attr("style","position: relative !important; display: block; top:0!important;");}
});


function getVisible() { 
  if ( $(window).width() < 694) {
   //$('#special-banner').detach().appendTo('#header-mobile').attr("style","position: relative !important; display: block; top:0!important;");
   var distance = $('#header-mobile .row.clearfix').height()+$('#wpadminbar').height();
   if ( $(window).scrollTop() >= distance) { 
   $('#special-banner').attr("style"," position: fixed !important; display: block; top:0!important; z-index:99999;");
   }
   else {
   $('#special-banner').attr("style"," position: relative !important; display: block; top:0!important;-webkit-transform: translate3d(0,0,0);");
   }
   }
}

jQuery(document).ready(function() {
getVisible();
});
//$(window).on('scroll resize', getVisible);
//var userAgent = navigator.userAgent || navigator.vendor || window.opera;
 if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
	 $(window).on('scroll resize', getVisible);
 }
 else {
   $(window).on('scroll resize', getVisible);
 }

jQuery(document).ready(function() {
if(jQuery( window ).width() <= 693){	
$('#menu-item-36 .sub-menu .row .menu-item').each(function (){
   jQuery(this).insertAfter('#menu-item-36');
});
$('#menu-item-36 .sub-menu').detach();
}
});