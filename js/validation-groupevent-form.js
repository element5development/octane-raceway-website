jQuery(document).ready(function($){
	
	jQuery('.main-form input[type="text"], .main-form textarea').keypress(function() {
		jQuery('.fields_required').fadeOut('fast');
		jQuery('.main-form input[type="text"], .main-form textarea').removeClass('error_field');
	});
	jQuery('.main-form input[name="email"]').keypress(function() {
		jQuery('.email_validation').fadeOut('fast');
		jQuery('.main-form input[name="email"]').removeClass('error_field');
	});
	jQuery('.main-form input[name="phone"]').keypress(function() {
		jQuery('.phone_validation').fadeOut('fast');
		jQuery('.main-form input[name="phone"]').removeClass('error_field');
	});
	
	jQuery(".group_event_submit").click(function(){
		var name = $('.main-form input[name="name"]').val();
		var email = $('.main-form input[name="email"]').val();
		var phone = $('.main-form input[name="phone"]').val();
		var question = $('.main-form input[name="question"]').val();
		
		var checkedValue = null;
		var inputElements = document.getElementsByClassName('check-box');
		for(var i=0; inputElements[i]; ++i){
			  if(inputElements[i].checked){
				   checkedValue = inputElements[i].value;
				   break;
			  }
		}
		
		if (jQuery('.main-form input#email').prop('checked')==true){
			jQuery('.main-form input[name="response_email"]').val(email);
		} else {
			jQuery('.main-form input[name="response_email"]').val('');
		}
		
		if (jQuery('.main-form input#phone').prop('checked')==true){
			jQuery('.main-form input[name="response_phone"]').val(phone);
		} else {
			jQuery('.main-form input[name="response_phone"]').val('');
		}
		
		var valid_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		var valid_phone = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
		
		$error = '';
		if( name == "" || email == "" || phone == "" || question == "" || checkedValue==null ) {
			$error += '<p class="fields_required">Fields with (*) cannot be leave empty.</p>';
			jQuery('.main-form input[type="text"], .main-form textarea').addClass('error_field');
		}
		if( !valid_email.test( email ) && email != "" ) {
			$error += '<p class="email_validation">Please enter your valid email address.</p>'; 
			jQuery('.main-form input[name="email"]').addClass('error_field');
		}
		if( !valid_phone.test( phone ) && phone != "" ) {
			$error += '<p class="phone_validation">Your Phone Number is invalid. Please enter your Area Code followed by your 7 Digit Phone Number.</p>'; 
			jQuery('.main-form input[name="phone"]').addClass('error_field');
		}
		
		if(!!$error){
			jQuery('.error_msg').html($error);
			return false;	
		}
		else{
			
			var v = grecaptcha.getResponse();
     	if(v.length == 0){
     		//event.preventDefault();
     		alert("Please complete the captcha.");
     		return false;
     	}
     	if(v.length != 0){
     		//document.getElementById('captcha').innerHTML="Captcha completed";
     		//isCaptchaValid =true;
     	  var url = jQuery(this).closest('form').attr("action");
			
			  jQuery.post(url, jQuery(this).closest('form').serialize(),
				 function( data ) {
					//alert(data);
					if(data==1)
					{
					//jQuery('.conform').fadeOut();
					jQuery(".main-form").html("Thank you for your message! We will get back to you as soon as possible.");
					}
				
				}
				);
     	
     	}
			
		}
		
	});
	


});
