jQuery(document).ready(function($){
		
	jQuery('.contact-form input[type="text"]').keypress(function() {
		jQuery('.contact-form input[type="text"], .contact-form input[type="email"]').removeClass('error_field');
	});
	jQuery('.contact-form input[name="email"]').keypress(function() {
		jQuery('.contact-form input[name="email"]').parent().removeClass('invalid_email');
	});
	jQuery('.contact-form input[name="phone"]').keypress(function() {
		jQuery('.contact-form input[name="phone"]').parent().removeClass('invalid_phone');
	});
	
	jQuery(".contact_button").click(function(){
		
		$is_error_msg = '';
		jQuery(this).closest('form').find(".require_field:visible").each(function(){
			var first_name = $('.contact-form input[name="first_name"]').val();
			var last_name = $('.contact-form input[name="last_name"]').val();
			var email = $('.contact-form input[name="email"]').val();
			var phone = $('.contact-form input[name="phone"]').val();
			
			var valid_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var valid_phone = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
			
			if( first_name == "" || last_name == "" || email == "" || phone == "" ) {
				jQuery('.contact-form input[type="text"], .contact-form input[type="email"]').addClass('error_field');
				$is_error_msg = 'error';
			}
			if( !valid_email.test( email ) && email != "" ) {
				jQuery('.contact-form input[name="email"]').parent().addClass('invalid_email');
				$is_error_msg = 'error';
			}
			if( !valid_phone.test( phone ) && phone != "" ) {
				jQuery('.contact-form input[name="phone"]').parent().addClass('invalid_phone');
				$is_error_msg = 'error';
			}
		});
		if(!!$is_error_msg){
			return false;	
		}
		else{
			
			var url = jQuery(this).closest('form').attr("action");
			
			jQuery.post(url, jQuery(this).closest('form').serialize(),
				function( data ) {
					
					if(data==1)
					{
						
					jQuery('#thanx-msg').show();
					jQuery('.conform').hide();	
					}
				
				}
				);
		}
		
	});
	


});
