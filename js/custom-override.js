(function(jQuery) {
    "use strict";
    jQuery(document).ready(function() {
      
	    jQuery(".parallax-banner").addClass("fixed").each(function() {
                    jQuery(this).parallax("50%", 0.5);
            });
		
		//jQuery Animation add here.
		onScrollInit( jQuery('.os-animation') );
		
		
		

		function onScrollInit( items, trigger ) {
			
  items.each( function() {
    var osElement = jQuery(this),
	
        osAnimationClass = osElement.attr('data-os-animation'),
        osAnimationDelay = osElement.attr('data-os-animation-delay');
      
        osElement.css({
          '-webkit-animation-delay':  osAnimationDelay,
          '-moz-animation-delay':     osAnimationDelay,
          'animation-delay':          osAnimationDelay
        });

        var osTrigger = ( trigger ) ? trigger : osElement;
        
        osTrigger.waypoint(function() {
          osElement.addClass('animated').addClass(osAnimationClass);
          },{
              triggerOnce: true,
              offset: '90%'
        });
  });
}
		
		
	
    });
				
         
})(jQuery);










