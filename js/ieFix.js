jQuery(window).bind("load", function(){
  if (navigator.userAgent.indexOf('MSIE') != -1)
  var detectIEregexp = /MSIE (\d+\.\d+);/ //test for MSIE x.x
  else // if no "MSIE" string in userAgent
  var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/ //test for rv:x.x or rv x.x where Trident string exists

 if (detectIEregexp.test(navigator.userAgent)){ //if some form of IE
  window.setTimeout(function(){
   $(window).scroll();
   jQuery(".home .owl-carousel").owlCarousel(
   {
		URLhashListener: false,
		dots: false,
				responsiveClass: true,
		responsive:{
			0:{
				items: 3,
			},
			480:{
				items: 3,
			},
			960:{
				items: 3,
			}
		}, 
						items: 1,
		nav:true,
		margin: 5,
		loop:true,
		autoplay: false,
		autoplaySpeed: 3000,
		smartSpeed:250,
		navSpeed: 1250,
		navText: ['prev', 'next'],
		autoplayHoverPause: true,
		lazyLoad: true,
		autoWidth: 0	}
   );
   $(".foogallery-owl-carousel .owl-prev").each(function() {
   $(this).closest(".owl-theme").prepend(this);
   $(".owl-prev").attr('style', 'display: inline-block !important');
   });
   jQuery("#foogallery-gallery-871").attr('style','height:auto; max-height:250px!important');
   jQuery("#foogallery-gallery-871 img").attr('style','height:250px !important');
   jQuery("#foogallery-gallery-871 .owl-prev").remove();
    },100);
	
  }
});
	
