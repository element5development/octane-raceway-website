jQuery(document).ready(function($){

    // Instantiates the variable that holds the media library frame.
    var gvp_logo_frame;

    $(document.body).on('click.gvpOpenMediaManager', '.gvp_settings_upload_button', function(e){
        e.preventDefault();

        if ( gvp_logo_frame ) {
            gvp_logo_frame.open();
            return;
        }

        gvp_logo_frame = wp.media.frames.gvp_logo_frame = wp.media({
            className: 'media-frame gvp-media-frame',
            frame: 'select',
            multiple: false,
            title: logo.title,
            library: {
                type: 'image'
            },
            button: {
                text:  logo.button
            }
        });

        gvp_logo_frame.on('select', function(){
            var media_attachment = gvp_logo_frame.state().get('selection').first().toJSON();

            $('#bsimage_url').val(media_attachment.url);
            $('.gvp_settings_upload_preview').attr('src',media_attachment.url);
        });

        gvp_logo_frame.open();
    });
});