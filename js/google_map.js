jQuery(document).ready(function($){
		
	/*---------------------------------------------------------------
	 *Google Map Add
	 *--------------------------------------------------------------*/
	if(jQuery('#map_show_on_page').length > 0)
	{
		
		
		var map_address_id_data = $('#map_fill_address').html();
	//alert(map_address_id_data);
		var map_pop_content = $('#map_fill_address').html();
		
	
		
		var geocoder = new google.maps.Geocoder();
		
		var address = map_address_id_data.replace('<br>','') ;
		
		geocoder.geocode( { 'address': address}, function(results, status) {
				
			if (status == google.maps.GeocoderStatus.OK)
			{
				var latitude =  results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
	
		var all_address = map_address_id_data;
		
		var locations = [
							 [all_address, latitude, longitude, 1] ,
						];
		
		
		var map = new google.maps.Map(document.getElementById('map_show_on_page'), {
						zoom:15,
						center: new google.maps.LatLng(latitude, longitude), 
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						panControl: false,
           				scrollwheel: false,
						navigationControl: false,
						mapTypeControl: false,
						scaleControl: false,
						
						
					});
		
		/*  var styles = [
						 {
		stylers: [
					{ hue: "#DED3A7" },
					{ saturation:  -20  },
				]
		 }
					   ];
	
		map.setOptions({styles: styles});*/
		var infowindow = new google.maps.InfoWindow();
		
		var marker, i;
		var iconBase = base_path;
		
		
		
			for (i = 0; i < locations.length; i++) 
			{  
			  
			  marker = new google.maps.Marker({
							position: new google.maps.LatLng(locations[i][1], locations[i][2]),                     map: map,
							icon: iconBase + 'octane-map-icon.png'
						});
			  
			  jQuery(window).load(function(){
										   
				infowindow.setContent(map_pop_content);
				infowindow.open(map, marker);
								
			  });
			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(map_pop_content);
								infowindow.open(map, marker);
							}
						})(marker, i));
			}
		  }
		 });//End google map
	}
	
		
	
	
	
	
});