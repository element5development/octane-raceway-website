jQuery(document).ready(function($){
	
	jQuery(".book_button").click(function(){
		
		$index = 0;
		$is_error_msg = '';
		jQuery(this).closest('form').find(".required:visible").each(function(){
			
			if( jQuery(this).val().trim() == '' ){
				switch( jQuery(this).attr('name').trim() ){
					
					case 'first_name':	
									$is_error_msg += "Please enter your first name. \n";
									
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;
									 
				
					case 'last_name':	
									$is_error_msg += "Please enter your last name. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;
									}
									break;
									 
				
					case 'zip_code':	
									$is_error_msg += "Please enter your zip code. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;
									}
									break;
									 
					
					
					case 'phone':	
									$is_error_msg += "Please enter your phone number. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
										 $index++;	
									}
									break;	
									 
					
					case 'email':	
									$is_error_msg += "Please enter your email. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;
					case 'party_type':	
									$is_error_msg += "Please select your party type. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;
					case 'guests_count':	
									$is_error_msg += "Please select your guests number. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;
					case 'heard_about_us':	
									$is_error_msg += "Please select from where you heard about us. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;	
					case 'heard_about_us_other_option':	
									$is_error_msg += "Please select from where you heard other options. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;	
									}
									break;					
					case 'message':	
									$is_error_msg += "Please enter your message. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
										
									 	$index++;
									}
									break;
					
									 
				}
			}else{
				switch( jQuery(this).attr('name').trim() ){
					
					case 'first_name':	
					
									var re = /\d/g;

    								if(re.test(jQuery(this).val())){
										$is_error_msg += "Please enter valid first name. \n";
										
									
									if($index==0)
									{
										jQuery(this).focus();
									 	$index++;	
									}
									}
									 break;
									 
				
					case 'last_name':	
									var re = /\d/g;

    								if(re.test(jQuery(this).val())){
										$is_error_msg += "Please enter valid last name. \n";
									
									if($index==0)
									{
										jQuery(this).focus();
									 	$index++;
									}
									}
									 break;
					
							
									 
					case 'email':	
   									 var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
									 if( ! re.test(jQuery(this).val()) ){
										$is_error_msg += "Please enter your valid email address. \n";
										
										if($index==0)
										{
											jQuery(this).focus();	
											$index++;
										}
									 }
										break;
					case 'phone':	
									
									if(jQuery(this).val().length< 10 )
									{
										$is_error_msg += "Please enter valid phone number. \n";
										
										if($index==0)
										{
											jQuery(this).focus();
											$index++;
										}
									}
									
									 break;
					
					
				}
			}
		});
		if(!!$is_error_msg){
			alert($is_error_msg);
			return false;	
		}
		else{
			
			var v = grecaptcha.getResponse();
			if(v.length == 0){
				//event.preventDefault();
				alert("Please complete the captcha.");
				return false;
			}
			if(v.length != 0){
				
				var url = jQuery(this).closest('form').attr("action");
			
				jQuery.post(url, jQuery(this).closest('form').serialize(),
					function( data ) {
					
						if(data==1)
						{
						jQuery('#thanx').fadeIn();	
						jQuery('.book_form').fadeOut();	
						}
				
					}
					);
				//document.getElementById('captcha').innerHTML="Captcha completed";
				//isCaptchaValid =true;
			}
			
			
		}
		
	});
	


});
