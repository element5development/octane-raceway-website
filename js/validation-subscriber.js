jQuery(document).ready(function(){
	
	jQuery('.subscriber-form-area input[name="name"]').keypress(function() {
		jQuery('.subscriber-form-area input[name="name"]').parent().removeClass('important_field');
	});
	jQuery('.subscriber-form-area input[name="email"]').keypress(function() {
		jQuery('.subscriber-form-area input[name="email"]').parent().removeClass('important_field');
		jQuery('.subscriber-form-area input[name="email"]').parent().removeClass('invalid_field');
	});
	
	jQuery(".subscriberbtn").click(function(){
		
		$is_error_msg = '';
		jQuery(this).closest('form').find(".field_required:visible").each(function(){
			var name = $('.subscriber-form-area input[name="name"]').val();
			var email = $('.subscriber-form-area input[name="email"]').val();
			
			var valid_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			
			if( name == "" || email == "" ) {
				jQuery('.subscriber-form-area .one-third:nth-of-type(1), .subscriber-form-area .one-third:nth-of-type(2)').addClass('important_field');
				$is_error_msg = 'error';
			}
			if( !valid_email.test( email ) && email != "" ) {
				jQuery('.subscriber-form-area .one-third:nth-of-type(2)').addClass('invalid_field');
				$is_error_msg = 'error';
			}
		});
		if(!!$is_error_msg){
			return false;	
		}
		else{
			
			var url = jQuery(this).closest('form').attr("action");
			
			jQuery.post(url, jQuery(this).closest('form').serialize(),
				function( data ) {
					//alert(data);
					if(data==1)
					{
						jQuery('.page-id-133 .stay-informed-from').addClass('success_subcribe');
						jQuery('.page-id-133 .stay-informed-from form').remove();
					}
					else{
						jQuery('.page-id-133 .stay-informed-from form').addClass('already_subcribe');
					}
				
				}
				);
		}
		
	});
	


});
