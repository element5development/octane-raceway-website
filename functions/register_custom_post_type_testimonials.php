<?php

// =============================================================================
//
//  This function file registers the custom post type testimonials.
// 
//  This file has been included in the functions.php file in the child theme
//
//
//
// =============================================================================

// Creates Testimonials Custom Post Type
  function custom_post_type_iw_testimonials() {

    // Set UI labels for Custom Post Type
      $labels = array(
        'name'                => _x( 'Testimonials', 'Post Type General Name'),
        'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name'),
        'menu_name'           => __( 'IW Testimonials'),
        'parent_item_colon'   => __( 'Testimonial Parent'),
        'all_items'           => __( 'All Testimonials'),
        'view_item'           => __( 'View Testimonial'),
        'add_new_item'        => __( 'Add New Testimonial'),
        'add_new'             => __( 'Add New Testimonial'),
        'edit_item'           => __( 'Edit Testimonial'),
        'update_item'         => __( 'Update Testimonial'),
        'search_items'        => __( 'Search Testimonial'),
        'not_found'           => __( 'Testimonial Not Found'),
        'not_found_in_trash'  => __( 'Testimonial Not found in Trash'),
      );
      
    // Set other options for Custom Post Type
      
      $args = array(
        'label'               => __( 'Testimonials'),
        'description'         => __( 'Posts specifically for displaying customer testimonials.'),
        'labels'              => $labels,
        'rewrite' => array('slug' => 'testimonials'),
            'query_var' => true,
            'menu_icon' => 'dashicons-megaphone',

        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes','tags'),

        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */  
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
      );
      
      // Registering your Custom Post Type
      register_post_type( 'iw-testimonials', $args );

  }

  add_action( 'init', 'custom_post_type_iw_testimonials', 0 );
// End of testimonials custom post type


// Add Custom Fields to IW Testimonial Posts
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_58e81e90cce89',
    'title' => 'Testimonial Content',
    'fields' => array (
        array (
            'key' => 'field_58e81eb36796a',
            'label' => 'Review By:',
            'name' => 'review_by',
            'type' => 'text',
            'instructions' => 'Enter the name of the person that completed the review/survey.',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array (
            'key' => 'field_58e81f1e6796b',
            'label' => 'Review Date:',
            'name' => 'review_date',
            'type' => 'date_picker',
            'instructions' => 'Enter the date that the review/survey was completed.',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'display_format' => 'F j, Y',
            'return_format' => 'Y-m-d',
            'first_day' => 0,
        ),
        array (
            'key' => 'field_58e81fb46796c',
            'label' => 'Review Rating:',
            'name' => 'review_rating',
            'type' => 'select',
            'instructions' => 'Select the star rating given in the review/survey.',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array (
                5 => 5,
                '4.5' => '4.5',
                4 => 4,
                '3.5' => '3.5',
                3 => 3,
                '2.5' => '2.5',
                2 => 2,
                '1.5' => '1.5',
                1 => 1,
            ),
            'default_value' => array (
                0 => 5,
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'ajax' => 0,
            'placeholder' => '',
            'disabled' => 0,
            'readonly' => 0,
        ),
        array (
            'key' => 'field_58e820ff3a2f2',
            'label' => 'Review Quote:',
            'name' => 'review_quote',
            'type' => 'textarea',
            'instructions' => 'Enter the review/survey quote. Do not include quotation marks, they are added automatically.',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => 'wpautop',
            'readonly' => 0,
            'disabled' => 0,
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'iw-testimonials',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => array (
        0 => 'excerpt',
        1 => 'custom_fields',
        2 => 'discussion',
        3 => 'comments',
        4 => 'revisions',
        5 => 'author',
        6 => 'page_attributes',
    ),
    'active' => 1,
    'description' => '',
));

endif;

// Add options pages for settings and help
if(function_exists(acf_add_options_page))
{
    acf_add_options_page(array(
        'page_title'    => 'IW Testimonials Settings',
        'menu_title'    => 'Settings',
        'menu_slug'     => 'testimonials_settings',
        'capability'    => 'edit_posts', 
        'parent_slug'   => 'edit.php?post_type=iw-testimonials',
        'position'  => false,
        'redirect'  => false,
    ));

    acf_add_options_page(array(
        'page_title'    => 'IW Testimonials Help',
        'menu_title'    => 'Help',
        'menu_slug'     => 'testimonials_help',
        'capability'    => 'edit_posts', 
        'parent_slug'   => 'edit.php?post_type=iw-testimonials',
        'position'  => false,
        'redirect'  => false,
    ));
}

// Add custom fields to Testimonials settings page
if( function_exists('acf_add_local_field_group') ):
    // Add Testimonials Settings
    acf_add_local_field_group(array (
        'key' => 'group_58eff19463914',
        'title' => 'Testimonials Settings',
        'fields' => array (
            array (
                'key' => 'field_58eff1b41b311',
                'label' => 'Testimonials Default Image',
                'name' => 'testimonials_default_image',
                'type' => 'image',
                'instructions' => 'Add a default image to act as a place holder if a featured image is not added to an individual testimonial post. Image must be 768px (width) x 768px (height). Acceptable file types are jpg, jpeg, png, and gif. Maximum file size is 50kb. ',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => 768,
                'min_height' => 768,
                'min_size' => '',
                'max_width' => 768,
                'max_height' => 768,
                'max_size' => '.05',
                'mime_types' => 'jpg, jpeg, png, gif',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
    
    // Add Testimonials Header Settings
    acf_add_local_field_group(array (
        'key' => 'group_58e82accefa6c',
        'title' => 'Testimonials Header',
        'fields' => array (
            array (
                'key' => 'field_58e82aeeaf7e0',
                'label' => 'Header Title',
                'name' => 'test_arc_header_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_58e82b2aaf7e1',
                'label' => 'Header Description',
                'name' => 'test_arc_header_description',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_58e82b4eaf7e2',
                'label' => 'Header Background Image',
                'name' => 'test_arc_header_background_image',
                'type' => 'gallery',
                'instructions' => 'Add up to 6 different images to show a random image when the Testimonials Archive is viewed. Images must be 1920px (width) x 300px (height). Image size must be less than 200kb. Accepted file types are jpg, jpeg, png, and gif. ',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'min' => 1,
                'max' => 6,
                'preview_size' => 'medium',
                'insert' => 'append',
                'library' => 'all',
                'min_width' => 1920,
                'min_height' => 300,
                'min_size' => '',
                'max_width' => 1920,
                'max_height' => 300,
                'max_size' => '0.2',
                'mime_types' => 'jpeg, jpg, png, gif',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_settings',
                ),
            ),
        ),
        'menu_order' => 1,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
    // Add Testimonials Footer Settings
    acf_add_local_field_group(array (
        'key' => 'group_58ed26c5ecc22',
        'title' => 'Testimonials Footer',
        'fields' => array (
            array (
                'key' => 'field_58ed26c604a9f',
                'label' => 'Footer Title',
                'name' => 'test_arc_footer_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_58ed26c604b1a',
                'label' => 'Footer Description',
                'name' => 'test_arc_footer_description',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_58ed277f2ae38',
                'label' => 'Footer Button Text',
                'name' => 'test_arc_footer_button_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_58ed27982ae39',
                'label' => 'Footer Button Link',
                'name' => 'test_arc_footer_button_link',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
            array (
                'key' => 'field_58ed26c604b94',
                'label' => 'Footer Background Image',
                'name' => 'test_arc_footer_background_image',
                'type' => 'image',
                'instructions' => 'Add an image for the testimonials archive footer. Images must be 1920px (width) x 300px (height). Image size must be less than 200kb. Accepted file types are jpg, jpeg, png, and gif. ',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => 1902,
                'min_height' => 300,
                'min_size' => '',
                'max_width' => 1920,
                'max_height' => 300,
                'max_size' => '',
                'mime_types' => 'jpg, jpeg, png, gif',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_settings',
                ),
            ),
        ),
        'menu_order' => 2,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

// Add Help Page Custom Fields
if( function_exists('acf_add_local_field_group') ):
    acf_add_local_field_group(array (
        'key' => 'group_58f15aa775c34',
        'title' => 'InterWebing Support',
        'fields' => array (
            array (
                'key' => 'field_58f15ab6721b0',
                'label' => 'Contact Support',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 33,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'If you need support for this Testimonials post type, please contact InterWebing Support at: <a href="mailto:support@interwebing.com?Subject=Support Request From OctaneRaceway.com" target="_top">Support@InterWebing.com</a>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_help',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_58f1386737a2e',
        'title' => 'Add New Testimonial',
        'fields' => array (
            array (
                'key' => 'field_58f1388421be1',
                'label' => 'Add New Testimonial Tutorial',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<video width="640" height="360" controls>
        <source src="http://octaneraceway.com/wp-content/uploads/2017/04/add_testimonials.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_help',
                ),
            ),
        ),
        'menu_order' => 1,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_58f138c16a878',
        'title' => 'Testimonials Settings',
        'fields' => array (
            array (
                'key' => 'field_58f138d73cc37',
                'label' => 'Testimonials Settings Tutorial',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<video width="640" height="360" controls>
        <source src="http://octaneraceway.com/wp-content/uploads/2017/04/testimonials_settings.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_help',
                ),
            ),
        ),
        'menu_order' => 2,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_58f138ffd3770',
        'title' => 'SEO Tips',
        'fields' => array (
            array (
                'key' => 'field_58f1391a739e1',
                'label' => 'Go Kart Racing vs Kart Racing',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 33,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Some quick research on Google Trends shows that the words "Go Kart Racing" far outrank "Kart Racing" and "Indoor Kart Racing." This implies that it would be a very good idea to feature the words "Go Kart Racing" very prominently on OctaneRaceway.com.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array (
                'key' => 'field_58f14443e123e',
                'label' => 'Always Feature City and State',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 33,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Anytime you are posting a testimonial be sure to feature the city and state of the reviewer in the title and text of the review. The heading of the content section below the review should also feature the city and state.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array (
                'key' => 'field_58f1445ae123f',
                'label' => 'Editor Content',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 34,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'In the editor section, while adding a testimonial, it is a good idea to write specifically about the activities that the reviewer(s) took part in. Again, be sure to feature their city and state in the heading and main content of this section. The main idea with this section is to expand on the content of the testimonial to help that specific testimonial rank for a search term in their city.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array (
                'key' => 'field_58f1446ae1240',
                'label' => 'Never Duplicate Content',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 33,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Never copy the content from any other page on the website, or even other testimonials, and use it on another testimonial. Google will see this as duplicate content. This will harm the ranking of the new testimonial, as well as the ranking for the entire website.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array (
                'key' => 'field_58f14480e1241',
                'label' => 'SEO Is A Slow Process',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => 33,
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'SEO is a slow game. There really are no shortcuts. You can no longer buy links or fill pages with tons of keywords. If Google determines that you have a large number of links from suspicious sources they will down rank you. If they determine that your pages are filled with keywords, also referred to as "keyword stuffing," they will down rank you. All of this is automatic and if OctaneRaceway.com is penalized in anyway for shady SEO techniques, there is nothing you can do about it. The best thing you can do is create new content on a regular basis. This is where Testimonials can be very powerful. They provide an easy we of creating new, relevant content on a regular basis. With that in mind, it is also best to not save up reviews from customers, then post them all at once. Your ranking in Google will see better improvement if are posting these as they come in, rather than posting 20 of them one day. With some practice, you should be able to post a new testimonial in about 15-20 minutes.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array (
                'key' => 'field_58f141bdf5609',
                'label' => 'SEO Tips Video',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<video width="640" height="360" controls>
        <source src="http://octaneraceway.com/wp-content/uploads/2017/04/seo_tips.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'testimonials_help',
                ),
            ),
        ),
        'menu_order' => 3,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;
