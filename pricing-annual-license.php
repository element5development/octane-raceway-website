<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }
 ?>

<div class="annual-license-sec txtcenter" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center top / cover ; padding: 0 0 60px;"<?php } ?>>
         <img src="<?php echo get_template_directory_uri();?>/images/white-down-arrow.png" alt="img">
           <div class="row">
               <div class="annual-license-sec-in">
                 <h2><?php the_title();?></h2>
                 <div class="clearfix common3"> 
                	 
					 <?php 
				       //remove_filter( 'the_content', 'wpautop' );
					   the_content();
					   //add_filter( 'the_content', 'wpautop' );
				   ?>
					 
                 </div>
               </div>
           </div>
        </div><!--annual-license-sec ends here-->