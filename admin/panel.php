<?php
// Global directory framework theme menu 
// add_action('admin_menu', 'custom_mega_menu_menu');
add_action('admin_menu', 'custom_theme_framework_admin_theme_menu');
function custom_theme_framework_admin_theme_menu() {
	add_theme_page(__( 'Theme Options', THEME_TEXTDOMAIN ), __( 'Theme Options', THEME_TEXTDOMAIN ), 'edit_theme_options', THEME_URL_PREFIX.'theme-options', 'custom_theme_framework_admin_panel');
}

function custom_mega_menu_menu() {
	add_theme_page('Mega Menu', 'Mega Menu', 'edit_theme_options', 'octane-theme-options&menu=general-options&sub_menu=mega-menu', 'custom_mega_menu_function');
}

// Register all options with default value 
add_action('admin_init' , 'custom_theme_register_options') ;
function custom_theme_register_options()
{
	global $custom_theme_options;
	//DESIGN OPTIONS - COLORS
	$custom_theme_options['custom_theme_header_background'] = '#ffffff';
	$custom_theme_options['custom_theme_header_text_color'] = '#333333';
	$custom_theme_options['custom_theme_header_hover_color'] = '#1570a6';

	$custom_theme_options['custom_theme_background_color'] = '#f5f5f5';
	$custom_theme_options['custom_theme_primary_color'] = '#1570a6';
	$custom_theme_options['custom_theme_hyperlink_color'] = '#ffffff';

	$custom_theme_options['custom_theme_hyperlink_hover_color'] = '#000000';
	$custom_theme_options['custom_theme_primary_hover_color'] = '#000000';
	$custom_theme_options['custom_theme_footer_background'] = '#1570a6';

	$custom_theme_options['custom_theme_footer_text_color'] = '#ffffff';

	$custom_theme_options['custom_theme_footer_hover_color'] = '#f8f8f8';

	

	//DESIGN OPTIONS - Basic Font

	$custom_theme_options['custom_theme_basic_font_family'] = 'arial';

	$custom_theme_options['custom_theme_basic_font_size'] = '13';

	$custom_theme_options['custom_theme_basic_line_height'] = '19';

	

	//DESIGN OPTIONS - HEADINGS

	$custom_theme_options['custom_theme_h1_heading_font_size'] = '28';

	$custom_theme_options['custom_theme_h1_heading_line_height'] = '34';

	

	$custom_theme_options['custom_theme_h2_heading_font_size'] = '20';

	$custom_theme_options['custom_theme_h2_heading_line_height'] = '25';

	

	$custom_theme_options['custom_theme_h3_heading_font_size'] = '17';

	$custom_theme_options['custom_theme_h3_heading_line_height'] = '22';

	

	$custom_theme_options['custom_theme_h4_heading_font_size'] = '15';

	$custom_theme_options['custom_theme_h4_heading_line_height'] = '18';

	

	$custom_theme_options['custom_theme_h5_heading_font_size'] = '14';

	$custom_theme_options['custom_theme_h5_heading_line_height'] = '17';

	

	$custom_theme_options['custom_theme_h6_heading_font_size'] = '14';

	$custom_theme_options['custom_theme_h6_heading_line_height'] = '17';

											

	apply_filters('custom_theme_options' ,$custom_theme_options ) ;

	

	if(!empty($custom_theme_options)){

	

		foreach ($custom_theme_options as $key => $value)

		{

			if(false === get_option($key))

				update_option($key,$value) ;

		}

	

	}

	

}



// Handel Form Submittion 

add_action('admin_init' , 'custom_theme_admin_option_form_submit_handler') ;

function custom_theme_admin_option_form_submit_handler()

{

	

	global $action , $custom_theme_option_page ;

	

	wp_reset_vars(array('action', 'custom_theme_option_page'));

	if(isset($action) && $action=='update-octanef-options')

	{

		// check nonce of security purpose 

		if ( ! current_user_can( 'manage_options' ) )

			wp_die( __( 'You do not have sufficient permissions to manage options for this site.', THEME_TEXTDOMAIN ) );

		

		

		check_admin_referer( $custom_theme_option_page . '-options' );

		

		

		foreach($_POST as $key => $value)

		{

			if(strpos($key , THEME_PREFIX )==0 )

			{

				if ( ! is_array( $value ) )

					$value = trim( $value );

				$value = wp_unslash( $value );

				//echo "Key: " . trim($key) . " Value: " . $value . "<br  />";

				update_option( trim($key), $value );

			}

		}

		

		$goback = add_query_arg( 'message', 'saved',  wp_get_referer() );

		wp_redirect( $goback );

		exit;

	}

	//exit();

}



function custom_theme_settings_fields($option_group) 

{

	echo "<input type='hidden' name='custom_theme_option_page' value='" . esc_attr($option_group) . "' />";

	echo '<input type="hidden" name="action" value="update-octanef-options" />';

	wp_nonce_field("$option_group-options");

}





// function to draw enter admin panel 

function custom_theme_framework_admin_panel()

{

	global $custom_theme_admin_main_menu ,  $custom_theme_add_sub_menu , $custom_theme_admin_option_field; 

	global $custom_theme_admin_active_menu , $custom_theme_admin_active_sub_menu ;

	global $octane_admin_option_form_heading;

	

	global $whitelist_options ;

	do_action( 'custom_theme_admin_panel_init' );

	do_action( 'octane_before_admin_panel' );

	?>

    <script type="text/javascript">

	jQuery(function() {

		<?php if(isset($_REQUEST['menu']) && !empty($_REQUEST['menu'])){ ?>	

			var index = jQuery( "#<?php echo $_REQUEST['menu']; ?>" ).attr('index');

			jQuery( "#octane_accordion" ).accordion({ heightStyle: "content", active : parseInt(index) });

		<?php }else{ ?>

			jQuery( "#octane_accordion" ).accordion({ heightStyle: "content", active : 0 });

		<?php } ?>	

	});

    </script>

	<div class="custom-theme-wrapper">

        <h2 class="custom-theme-title"><?php echo THEME_NAME; _e('Options', THEME_TEXTDOMAIN); ?></h2>

        <div class="custom-theme-wrapper-in">

           <div class="clearfix">

              <div class="custom-theme-left-nav">

              <?php do_action('octane_before_admin_menu') ; ?>

                 <?php custom_theme_display_admin_menu() ;?>

               <?php do_action('octane_after_admin_menu')?>

              </div> <!-- custom-theme-left-nav ends here-->

              <div class="custom-theme-content-wrapper">  
               <?php
                // Display notification if we did something
                if ( ! empty( $_GET['message'] ) ) 
                {

                    if ( $_GET['message'] == 'saved' ) 

                    ?>

                    <div class="notification"><i class="fa fa-info-circle "></i> <?php _e('Options have been saved successfully.'  , THEME_TEXTDOMAIN) ;?></div>             

                    <?php

                }

                ?>           

               <div class="custom-theme-content-wrapper-in"> 

                    <h3 class="form-header"><?php 

                    // Option form Heading goese here///

                    echo $octane_admin_option_form_heading ;

                    ?></h3>                      

                    <?php 

                    // check if main menu has any callback function 

                    // Run foreach to get active main menu item 

                    $main_menu; 

                    $sub_menu;

                    foreach($custom_theme_admin_main_menu as $main_menu_item)

                    {

                        if($main_menu_item['menu_slug'] == $custom_theme_admin_active_menu)

                        {

                            $main_menu =$main_menu_item ;

                            break ; 

                        }

                    }

                    

                    // check if there is this main menu does not have any sub menu 

                    if(!empty($main_menu) && is_array($main_menu)  && empty($custom_theme_admin_active_sub_menu))

                    {

                        if(!empty($main_menu['option_page_callback']))

                        {

                            call_user_func_array($main_menu['option_page_callback'], $custom_theme_admin_active_menu);		}

                        else

                        {

                            $option_form_path = apply_filters('octane_admin_option_form_path' , 'option-forms/' .$custom_theme_admin_active_menu.'.php');

                            require_once($option_form_path);			

                        }

                        

                    }

                    else

                    {

                        // check for current main menu's active sub menu 

                        foreach($custom_theme_add_sub_menu[$custom_theme_admin_active_menu] as $sub_menu_item)

                        {

                            if($sub_menu_item['sub_menu_slug'] == $custom_theme_admin_active_sub_menu)

                            {

                                $sub_menu =$sub_menu_item ;

                                break ; 

                            }

                        }

                         

                        

                        if(!empty($sub_menu) && is_array($sub_menu) )

                        {

                            if(!empty($sub_menu['option_page_callback']))

                                call_user_func_array($sub_menu['option_page_callback'], array('menu'=>$custom_theme_admin_active_sub_menu));	

                            else

                            {

                                $option_form_path = apply_filters('octane_admin_option_form_path' , 'option-forms/' . $custom_theme_admin_active_menu . '/' .$custom_theme_admin_active_sub_menu.'.php');

                                require_once($option_form_path);	

                            

                            }

                        }

                    }

                    ?>

               </div>

           </div> <!-- custom-theme-content-wrapper ends here-->

           </div> <!-- clearfix ends here-->

        </div> <!-- custom-theme-wrapper-in ends here-->

     </div> <!-- custom-theme-wrapper ends here--> 

			 <?php // end of wrapper ?>

	<?php 

		do_action( 'octane_after_admin_panel' );  

}



function custom_theme_set_active_menus()  
{

	global $custom_theme_admin_active_menu, $custom_theme_admin_active_sub_menu ;

	

	global $custom_theme_admin_main_menu , $custom_theme_add_sub_menu;

	$custom_theme_admin_active_menu = '' ; 

	$custom_theme_admin_active_sub_menu = '' ; 

	

	if(isset($_REQUEST['menu']) && $_REQUEST['menu']!='')

		$custom_theme_admin_active_menu = $_REQUEST['menu'];

	else

	{ 

		ksort($custom_theme_admin_main_menu);

		$first_main_menu =current($custom_theme_admin_main_menu);

		$main_menu = $first_main_menu['menu_slug'] ;

		

		ksort($custom_theme_add_sub_menu[$main_menu]);

		$first_sub_menu = current($custom_theme_add_sub_menu[$main_menu]) ;

		$sub_menu = $first_sub_menu['sub_menu_slug']; 

		$custom_theme_admin_active_menu= apply_filters('octane_default_active_menu' , $main_menu) ;

		$custom_theme_admin_active_sub_menu= apply_filters('octane_default_active_sub_menu' ,$sub_menu ) ;

	}	

	

	if(isset($_REQUEST['sub_menu']) && $_REQUEST['sub_menu']!='')

		$custom_theme_admin_active_sub_menu = $_REQUEST['sub_menu'];


}



function custom_theme_display_admin_menu()

{

	global $custom_theme_admin_main_menu , $custom_theme_add_sub_menu;

	global $custom_theme_admin_active_menu, $custom_theme_admin_active_sub_menu ;

	global $octane_admin_option_form_heading ;

	$octane_admin_option_form_heading='';

	ksort($custom_theme_admin_main_menu);

	

	$custom_theme_admin_main_menu  = apply_filters('custom_theme_admin_main_menu' , $custom_theme_admin_main_menu ) ;

	

	if(is_array($custom_theme_admin_main_menu) && count($custom_theme_admin_main_menu) > 0 )

	{

		echo "<ul id='octane_accordion'>" ;

		$index = 0;

		foreach ($custom_theme_admin_main_menu as $main_menu)

		{

			$sub_menu_query_string= '' ;

			if($custom_theme_admin_active_menu == $main_menu['menu_slug'])

			{

				$main_menu_active_class = ' class="active" ' ; 

				$octane_admin_option_form_heading = $main_menu['title'] ;

			}

			else

				$main_menu_active_class = '' ;

			if(!empty($custom_theme_add_sub_menu[$main_menu['menu_slug']]) && is_array($custom_theme_add_sub_menu[$main_menu['menu_slug']]))

			{

				ksort($custom_theme_add_sub_menu[$main_menu['menu_slug']]);

				$sub_menu_item=current($custom_theme_add_sub_menu[$main_menu['menu_slug']]);

				$sub_menu_query_string = '&sub_menu=' . $sub_menu_item['sub_menu_slug'] ;

			}

			

			echo "<li><a id='" . $main_menu['menu_slug']  ."' index='".$index."'><i class='fa ".$main_menu['icon_class']."'></i><span class='leftmenu-text'>" . $main_menu['title']  ."</span></a>" ;

			if(!empty($custom_theme_add_sub_menu[$main_menu['menu_slug']]) && is_array($custom_theme_add_sub_menu[$main_menu['menu_slug']]))

			{

				ksort($custom_theme_add_sub_menu[$main_menu['menu_slug']]);

				if(is_array($custom_theme_add_sub_menu) && count($custom_theme_add_sub_menu) > 0 )

				{

					echo "<ul>" ;

					foreach ($custom_theme_add_sub_menu[$main_menu['menu_slug']] as $sub_menu)

					{

						if($custom_theme_admin_active_sub_menu == $sub_menu['sub_menu_slug'])

						{

							$sub_menu_active_class = ' class="active" ' ; 

							$octane_admin_option_form_heading .= ' - ' . $sub_menu['title'] ;

						}

						else

							$sub_menu_active_class = '' ;

						

						$sub_menu_query_string = '&sub_menu=' . $sub_menu['sub_menu_slug'] ;

						echo "<li $sub_menu_active_class ><a href='". get_admin_url(). "admin.php?page=".$_REQUEST['page']. "&menu=" .$main_menu['menu_slug'] .  $sub_menu_query_string   ."'><i class='fa ".$sub_menu['icon_class']."'></i><span class='leftmenu-text'>" .  $sub_menu['title']   ."</span></a></li>" ;

					}

					echo "</ul>" ;

				}

			}

			echo "</li>";

			$index++;

		}

		echo "</ul>" ;

	}

}



// Action to add global directory main menu 

add_action('custom_theme_admin_panel_init', 'custom_theme_admin_panel_callback',0) ;

function custom_theme_admin_panel_callback()
{

	    // Add main general setting menu

	    custom_theme_add_main_menu('General Options' , 'general-options' ,'', '' , 'fa-cogs' , 10 ) ;

		// Add general Options email setup sub menu

		custom_theme_add_sub_menu('general-options' ,'Header' , 'header' ,'', '' , 'fa-cogs' , 20 );

		
		custom_theme_add_sub_menu('general-options' ,'Contact' , 'contact' ,'', '' , 'fa-cogs' , 21 ) ;
		
		

		custom_theme_add_sub_menu('general-options' ,'Home' , 'home-page' ,'', '' , 'fa-cogs' , 22 ) ;
		// custom_theme_add_sub_menu('general-options' ,'Mega Menu' , 'mega-menu' ,'', '' , 'fa-cogs' , 24 ) ;
		
		
	
		
		

		//custom_theme_add_sub_menu('general-options' ,'Login' , 'sigin-up' ,'', '' , 'fa-cogs' , 23 ) ;

		

		

		// Add general Options upload image size sub menu	

		//custom_theme_add_sub_menu('general-options' ,'Upload Image Size' , 'upload-image-size' ,'', '' , 'fa-crop' , 30 ) ;

		

		

		// Add general option custom code sub menu

		custom_theme_add_sub_menu('general-options' ,'Footer' , 'footer-code' ,'', '' , 'fa-code' ,80 ) ;

		

		



	// Set global active tabs based on query string 

	custom_theme_set_active_menus() ;

	

}







// function to add a new main tab in admin panel 

function custom_theme_add_main_menu( $menu_title,  $menu_slug, $external_url='' , $function = '', $icon_class = '', $position = null ) 
{

	global $custom_theme_admin_main_menu;

	$menu = 	array(	'title'=> ( $menu_title ),

							'menu_slug' => sanitize_title($menu_slug) ,

							'external_url' => $external_url , 

							'option_page_callback' => $function , 

							'icon_class' => $icon_class

					);

					

	if ( null === $position )

		$custom_theme_admin_main_menu[] =$menu;

	else

		$custom_theme_admin_main_menu[$position] = $menu;			

}


function custom_theme_add_sub_menu( $parent_menu_slug, $sub_menu_title, $sub_menu_slug, $external_url='' , $function = '', $icon_class = '', $position = null ) 
{

	global $custom_theme_add_sub_menu;

	$sub_menu = 	array(	'title'=>  $sub_menu_title ,

							'sub_menu_slug' =>sanitize_title( $sub_menu_slug) ,

							'external_url' => $external_url , 

							'option_page_callback' => $function , 

							'icon_class' => $icon_class 

					);

					

	if ( null === $position )

		$custom_theme_add_sub_menu[$parent_menu_slug][] =$sub_menu;

	else

		$custom_theme_add_sub_menu[$parent_menu_slug][$position] = $sub_menu;	

}