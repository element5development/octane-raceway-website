                                                                                                                                                                                                <?php
/*----------------------------------------
/* Home page top section
/*----------------------------------------*/
?>
<script type="text/javascript">
        jQuery(document).ready(function()
        {
            var last_option_name = jQuery('#last_option_name').val()-1;
            jQuery('#add_option_button').click(function()
                {
                    last_option_name = last_option_name+1;
                    jQuery('#add_option_box').append('<input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_display_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Display Value" /><input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Value" /><br>');
                });
        });
            </script>
<script type="text/javascript">

                (function($){

                    "use strict";

                    var attachment;

                    $("body").on("click", ".fileupload", function(e){


                        var t = $(this).data("target");

                        var i = $(this).data("targetimg");


                        e.preventDefault();


                        var file_frame;


                        if(file_frame){ file_frame.open(); return; }


                        file_frame = wp.media.frames.file_frame = wp.media({


                        title: jQuery( this ).data( 'uploader_title' ),


                        button: {


                        text: jQuery( this ).data( 'uploader_button_text' ),


                        },


                        multiple: false


                        });


                        file_frame.on( 'select', function(){


                        attachment = file_frame.state().get('selection').first().toJSON();


                        $(i).attr('src', attachment.url);
                        $(t).val(attachment.url);


                        });


                        file_frame.open();


                        // Upload field reset button


                    }).on("click", ".fileuploadcancel", function(){


                        var t = $(this).attr("tar");


                        $("input[type='text'][tar='" + t + "']").val("");


                        $("img[tar='" + t + "']").prop("src", "");


                    });


                })(jQuery);
                </script>
<form class="main-form" method="post">
 <?php custom_theme_settings_fields('general-homepage-setup'); ?>
    <span class="octane-seprator"></span>
    <div class="field-row clearfix">
    <div class="field-head">
      <h4>
        <?php _e('Site Logo Content', THEME_TEXTDOMAIN) ;  ?>
      </h4>
    </div>
    <div class="field-main"> <span class="field-options">
      <textarea  name="<?php echo THEME_PREFIX; ?>site_logo_content"><?php echo get_option(THEME_PREFIX.'site_logo_content'); ?></textarea>
      </span> <small class="octane-short-desc">(<?php echo htmlentities("Every<span>[</span>one<span>]</span>"); ?>)</small></div>
  </div>
  <span class="octane-seprator"></span>
  <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Home Page Logo', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display logo at Home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home page logo url', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_page_logo = get_option(THEME_PREFIX.'home_page_logo');?>
           <!-- <img id="custom_theme_home_page_logo_img" src="<?php if(!empty($custom_theme_home_page_logo)){echo $custom_theme_home_page_logo;} else{ echo ' '; }?>" alt="custom_theme_home_page_logo" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_page_logo" id="<?php echo THEME_PREFIX;?>home_page_logo" class="custom_theme_home_page_logo" value="<?php echo $custom_theme_home_page_logo; ?>" />

                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>home_page_logo" data-targetimg = "#<?php echo THEME_PREFIX;?>home_page_logo_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Logo will be shown at Home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>
         <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Home Background Image', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display background image at Home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home background image url', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_bg_image = get_option(THEME_PREFIX.'home_bg_image');?>
           <!-- <img id="custom_theme_home_bg_image_img" src="<?php if(!empty($custom_theme_home_bg_image)){echo $custom_theme_home_bg_image;} else{ echo ' '; }?>" alt="custom_theme_home_bg_image" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_bg_image" id="<?php echo THEME_PREFIX;?>home_bg_image" class="custom_theme_home_bg_image" value="<?php echo $custom_theme_home_bg_image; ?>" />

                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>home_bg_image" data-targetimg = "#<?php echo THEME_PREFIX;?>home_bg_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Background image will be shown at Home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>

  <span class="octane-seprator"></span>
         <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Home Background Video', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display background video at Home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home background video url(.mp4)', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_bg_video = get_option(THEME_PREFIX.'home_bg_video');?>
           <!-- <img id="custom_theme_home_bg_image_img" src="<?php if(!empty($custom_theme_home_bg_image)){echo $custom_theme_home_bg_image;} else{ echo ' '; }?>" alt="custom_theme_home_bg_image" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_bg_video" id="<?php echo THEME_PREFIX;?>home_bg_video" class="custom_theme_home_bg_video" value="<?php echo $custom_theme_home_bg_video; ?>" />

                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>home_bg_video" data-targetimg = "#<?php echo THEME_PREFIX;?>home_bg_video_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Background video will be shown at Home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home background video url(.webm)', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_bg_video2 = get_option(THEME_PREFIX.'home_bg_video2');?>
           <!-- <img id="custom_theme_home_bg_image_img" src="<?php if(!empty($custom_theme_home_bg_image)){echo $custom_theme_home_bg_image;} else{ echo ' '; }?>" alt="custom_theme_home_bg_image" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_bg_video2" id="<?php echo THEME_PREFIX;?>home_bg_video2" class="custom_theme_home_bg_video2" value="<?php echo $custom_theme_home_bg_video2; ?>" />

                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>home_bg_video2" data-targetimg = "#<?php echo THEME_PREFIX;?>home_bg_video2_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Background video will be used for fallback.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
    </div>
 </div>

    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Home Heading Section 1', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display heading at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home Heading Section 1', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_heading_section_1 = get_option(THEME_PREFIX.'home_heading_section_1');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_home_heading_section_1)){echo $custom_theme_home_heading_section_1;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_heading_section_1" id="<?php echo THEME_PREFIX;?>home_heading_section_1" class="custom_theme_home_heading_section_1" value="<?php echo $custom_theme_home_heading_section_1; ?>" />

             <small class="octane-short-desc">(<?php _e('Heading will be shown at home banner image.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>

    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Home Heading Section 2', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display heading at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Home Heading Section 2', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_home_heading_section_2 = get_option(THEME_PREFIX.'home_heading_section_2');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_home_heading_section_2)){echo $custom_theme_home_heading_section_2;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>home_heading_section_2" id="<?php echo THEME_PREFIX;?>home_heading_section_2" class="custom_theme_home_heading_section_2" value="<?php echo $custom_theme_home_heading_section_2; ?>" />

             <small class="octane-short-desc">(<?php _e('Heading will be shown at home banner image.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>
    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 1', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 1 at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 1', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_1 = get_option(THEME_PREFIX.'box_title_1');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_1)){echo $custom_theme_box_title_1;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_1" id="<?php echo THEME_PREFIX;?>box_title_1" class="custom_theme_box_title_1" value="<?php echo $custom_theme_box_title_1; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 1 will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>

    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 1 Link', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 1 link at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 1 Link', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_1_link = get_option(THEME_PREFIX.'box_title_1_link');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_1_link)){echo $custom_theme_box_title_1_link;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_1_link" id="<?php echo THEME_PREFIX;?>box_title_1_link" class="custom_theme_box_title_1_link" value="<?php echo $custom_theme_box_title_1_link; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 1 link will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>

    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 2', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 2 at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 2', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_2 = get_option(THEME_PREFIX.'box_title_2');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_2)){echo $custom_theme_box_title_2;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_2" id="<?php echo THEME_PREFIX;?>box_title_2" class="custom_theme_box_title_2" value="<?php echo $custom_theme_box_title_2; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 2 will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>
    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 2 Link', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 2 link at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 2 Link', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_2_link = get_option(THEME_PREFIX.'box_title_2_link');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_2_link)){echo $custom_theme_box_title_2_link;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_2_link" id="<?php echo THEME_PREFIX;?>box_title_2_link" class="custom_theme_box_title_2_link" value="<?php echo $custom_theme_box_title_2_link; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 2 link will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>

    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 3', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 3 at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 3', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_3 = get_option(THEME_PREFIX.'box_title_3');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_3)){echo $custom_theme_box_title_3;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_3" id="<?php echo THEME_PREFIX;?>box_title_3" class="custom_theme_box_title_3" value="<?php echo $custom_theme_box_title_3; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 3 will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>
    <div class="field-row clearfix">
    <div class="field-head">
        <h4><?php _e('Box Title 3 Link', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display box title 3 link at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Box Title 3 Link', THEME_TEXTDOMAIN) ;  ?></label>
             <?php  $custom_theme_box_title_3_link = get_option(THEME_PREFIX.'box_title_3_link');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_box_title_3_link)){echo $custom_theme_box_title_3_link;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->

                <input type="text" name="<?php echo THEME_PREFIX;?>box_title_3_link" id="<?php echo THEME_PREFIX;?>box_title_3_link" class="custom_theme_box_title_3_link" value="<?php echo $custom_theme_box_title_3_link; ?>" />

             <small class="octane-short-desc">(<?php _e('Box title 3 link will be shown at home page.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>

    </div>
 </div>
    <span class="octane-seprator"></span>

 <?php submit_button(); ?>
</form>