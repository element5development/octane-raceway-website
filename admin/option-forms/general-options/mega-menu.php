<script type="text/javascript">
		jQuery(document).ready(function()
		{
			var last_option_name = jQuery('#last_option_name').val()-1;
			jQuery('#add_option_button').click(function()
				{
					last_option_name = last_option_name+1;
					jQuery('#add_option_box').append('<input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_display_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Display Value" /><input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Value" /><br>');
				});
		});
			</script>
<script type="text/javascript">
			
			(function($){
			
				"use strict";
				
				var attachment;
				
				$("body").on("click", ".fileupload", function(e){
				
				
					var t = $(this).data("target");
					
					var i = $(this).data("targetimg");
					
					
					e.preventDefault();
					
					
					var file_frame;
					
					
					if(file_frame){ file_frame.open(); return; }
					
					
					file_frame = wp.media.frames.file_frame = wp.media({
					
					
					title: jQuery( this ).data( 'uploader_title' ),
					
					
					button: {
					
					
					text: jQuery( this ).data( 'uploader_button_text' ),
					
					
					},
					
					
					multiple: false
					
					
					});
					
					
					file_frame.on( 'select', function(){
					
					
					attachment = file_frame.state().get('selection').first().toJSON();
					
					
					$(i).attr('src', attachment.url);
					$(t).val(attachment.url);
					
					
					});
					
					
					file_frame.open();
					
					
					// Upload field reset button
					
				
				}).on("click", ".fileuploadcancel", function(){
				
				
					var t = $(this).attr("tar");
					
					
					$("input[type='text'][tar='" + t + "']").val("");
					
					
					$("img[tar='" + t + "']").prop("src", "");
				
				
				});
				
			
			})(jQuery);
			</script>           
<form class="main-form" method="post">
 <?php custom_theme_settings_fields('general-email-setup') ;?>     





<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #1 Image', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_1_image = get_option(THEME_PREFIX.'menu_item_1_image');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_1_image" id="<?php echo THEME_PREFIX;?>menu_item_1_image" class="custom_theme_menu_item_1_image" value="<?php echo $custom_theme_menu_item_1_image; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>menu_item_1_image" data-targetimg = "#<?php echo THEME_PREFIX;?>menu_item_1_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #1 Title', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_1_title = get_option(THEME_PREFIX.'menu_item_1_title');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_1_title" id="<?php echo THEME_PREFIX;?>menu_item_1_title" class="custom_theme_menu_item_1_title" value="<?php echo $custom_theme_menu_item_1_title; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #1 Link', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_1_link = get_option(THEME_PREFIX.'menu_item_1_link');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_1_link" id="<?php echo THEME_PREFIX;?>menu_item_1_link" class="custom_theme_menu_item_1_link" value="<?php echo $custom_theme_menu_item_1_link; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #1 Content', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_1_content = get_option(THEME_PREFIX.'menu_item_1_content');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_1_content" id="<?php echo THEME_PREFIX;?>menu_item_1_content" class="custom_theme_menu_item_1_content" value="<?php echo $custom_theme_menu_item_1_content; ?>" />
                
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator" style="margin-top: 16px;"></span>



<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #2 Image', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_2_image = get_option(THEME_PREFIX.'menu_item_2_image');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_2_image" id="<?php echo THEME_PREFIX;?>menu_item_2_image" class="custom_theme_menu_item_2_image" value="<?php echo $custom_theme_menu_item_2_image; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>menu_item_2_image" data-targetimg = "#<?php echo THEME_PREFIX;?>menu_item_2_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #2 Title', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_2_title = get_option(THEME_PREFIX.'menu_item_2_title');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_2_title" id="<?php echo THEME_PREFIX;?>menu_item_2_title" class="custom_theme_menu_item_2_title" value="<?php echo $custom_theme_menu_item_2_title; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #2 Link', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_2_link = get_option(THEME_PREFIX.'menu_item_2_link');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_2_link" id="<?php echo THEME_PREFIX;?>menu_item_2_link" class="custom_theme_menu_item_2_link" value="<?php echo $custom_theme_menu_item_2_link; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #2 Content', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_2_content = get_option(THEME_PREFIX.'menu_item_2_content');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_2_content" id="<?php echo THEME_PREFIX;?>menu_item_2_content" class="custom_theme_menu_item_2_content" value="<?php echo $custom_theme_menu_item_2_content; ?>" />
                
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator" style="margin-top: 16px;"></span>



<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #3 Image', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_3_image = get_option(THEME_PREFIX.'menu_item_3_image');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_3_image" id="<?php echo THEME_PREFIX;?>menu_item_3_image" class="custom_theme_menu_item_3_image" value="<?php echo $custom_theme_menu_item_3_image; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>menu_item_3_image" data-targetimg = "#<?php echo THEME_PREFIX;?>menu_item_3_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #3 Title', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_3_title = get_option(THEME_PREFIX.'menu_item_3_title');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_3_title" id="<?php echo THEME_PREFIX;?>menu_item_3_title" class="custom_theme_menu_item_3_title" value="<?php echo $custom_theme_menu_item_3_title; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #3 Link', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_3_link = get_option(THEME_PREFIX.'menu_item_3_link');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_3_link" id="<?php echo THEME_PREFIX;?>menu_item_3_link" class="custom_theme_menu_item_3_link" value="<?php echo $custom_theme_menu_item_3_link; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #3 Content', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_3_content = get_option(THEME_PREFIX.'menu_item_3_content');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_3_content" id="<?php echo THEME_PREFIX;?>menu_item_3_content" class="custom_theme_menu_item_3_content" value="<?php echo $custom_theme_menu_item_3_content; ?>" />
                
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator" style="margin-top: 16px;"></span>



<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #4 Image', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_4_image = get_option(THEME_PREFIX.'menu_item_4_image');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_4_image" id="<?php echo THEME_PREFIX;?>menu_item_4_image" class="custom_theme_menu_item_4_image" value="<?php echo $custom_theme_menu_item_4_image; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>menu_item_4_image" data-targetimg = "#<?php echo THEME_PREFIX;?>menu_item_4_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #4 Title', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_4_title = get_option(THEME_PREFIX.'menu_item_4_title');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_4_title" id="<?php echo THEME_PREFIX;?>menu_item_4_title" class="custom_theme_menu_item_4_title" value="<?php echo $custom_theme_menu_item_4_title; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #4 Link', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_4_link = get_option(THEME_PREFIX.'menu_item_4_link');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_4_link" id="<?php echo THEME_PREFIX;?>menu_item_4_link" class="custom_theme_menu_item_4_link" value="<?php echo $custom_theme_menu_item_4_link; ?>" />
                
         </span>
         
    </div>
 </div> 
	
<div class="field-row clearfix" style="padding: 6px 0 0 0;">                                
    <div class="field-head">
        <h5 style="padding-top:6px;"><?php _e('Menu Item #4 Content', THEME_TEXTDOMAIN) ;  ?></h5>
    </div>
    <div class="field-main">
         <span class="field-options" style="padding: 0px;">
			 <?php  $custom_theme_menu_item_4_content = get_option(THEME_PREFIX.'menu_item_4_content');?>
           
                <input type="text" name="<?php echo THEME_PREFIX;?>menu_item_4_content" id="<?php echo THEME_PREFIX;?>menu_item_4_content" class="custom_theme_menu_item_4_content" value="<?php echo $custom_theme_menu_item_4_content; ?>" />
                
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator" style="margin-top: 16px;"></span>
	
	<?php submit_button(); ?>                            
</form>