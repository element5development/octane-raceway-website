<script type="text/javascript">
		jQuery(document).ready(function()
		{
			var last_option_name = jQuery('#last_option_name').val()-1;
			jQuery('#add_option_button').click(function()
				{
					last_option_name = last_option_name+1;
					jQuery('#add_option_box').append('<input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_display_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Display Value" /><input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Value" /><br>');
				});
		});
			</script>
<script type="text/javascript">
			
			(function($){
			
				"use strict";
				
				var attachment;
				
				$("body").on("click", ".fileupload", function(e){
				
				
					var t = $(this).data("target");
					
					var i = $(this).data("targetimg");
					
					
					e.preventDefault();
					
					
					var file_frame;
					
					
					if(file_frame){ file_frame.open(); return; }
					
					
					file_frame = wp.media.frames.file_frame = wp.media({
					
					
					title: jQuery( this ).data( 'uploader_title' ),
					
					
					button: {
					
					
					text: jQuery( this ).data( 'uploader_button_text' ),
					
					
					},
					
					
					multiple: false
					
					
					});
					
					
					file_frame.on( 'select', function(){
					
					
					attachment = file_frame.state().get('selection').first().toJSON();
					
					
					$(i).attr('src', attachment.url);
					$(t).val(attachment.url);
					
					
					});
					
					
					file_frame.open();
					
					
					// Upload field reset button
					
				
				}).on("click", ".fileuploadcancel", function(){
				
				
					var t = $(this).attr("tar");
					
					
					$("input[type='text'][tar='" + t + "']").val("");
					
					
					$("img[tar='" + t + "']").prop("src", "");
				
				
				});
				
			
			})(jQuery);
			</script>   
<form class="main-form" method="post">
 <?php custom_theme_settings_fields('general-email-setup') ;?>                    
 <div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Copyright text', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc"><?php _e('Add copyright text', THEME_TEXTDOMAIN) ;  ?></small>
    </div> 
    <div class="field-main">
        <span class="field-options">
           <textarea  name="<?php echo THEME_PREFIX; ?>copy_right"><?php echo get_option(THEME_PREFIX.'copy_right'); ?></textarea> 
        </span>                             
    </div>
 </div> 
 <span class="octane-seprator"></span>
 
 
 
  <div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Footer logo image', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display on  page footer section', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Footer logo image url', THEME_TEXTDOMAIN);  ?></label>
			 <?php 
			  $footer_logo_image = get_option(THEME_PREFIX.'footer_logo_image');?>
           <!-- <img id="footer_logo_image_img" src="<?php if(!empty($footer_logo_image)){echo $footer_logo_image;} else{ echo ' '; }?>" alt="footer_logo_image" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>footer_logo_image" id="<?php echo THEME_PREFIX;?>footer_logo_image" class="footer_logo_image" value="<?php echo $footer_logo_image; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>footer_logo_image" data-targetimg = "#<?php echo THEME_PREFIX;?>footer_logo_image_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Display on  page footer section' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
    </div>
 </div> 
 <span class="octane-seprator"></span> 
 
 
 
 
 
 
 <?php submit_button(); ?>                            
</form>