<script type="text/javascript">
		jQuery(document).ready(function()
		{
			var last_option_name = jQuery('#last_option_name').val()-1;
			jQuery('#add_option_button').click(function()
				{
					last_option_name = last_option_name+1;
					jQuery('#add_option_box').append('<input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_display_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Display Value" /><input type="text" name="<?php echo THEME_PREFIX ."main_page_get_help_now_options";?>'+last_option_name+'" type="text" class="field-small" value="" placeholder="Value" /><br>');
				});
		});
			</script>
<script type="text/javascript">
			
			(function($){
			
				"use strict";
				
				var attachment;
				
				$("body").on("click", ".fileupload", function(e){
				
				
					var t = $(this).data("target");
					
					var i = $(this).data("targetimg");
					
					
					e.preventDefault();
					
					
					var file_frame;
					
					
					if(file_frame){ file_frame.open(); return; }
					
					
					file_frame = wp.media.frames.file_frame = wp.media({
					
					
					title: jQuery( this ).data( 'uploader_title' ),
					
					
					button: {
					
					
					text: jQuery( this ).data( 'uploader_button_text' ),
					
					
					},
					
					
					multiple: false
					
					
					});
					
					
					file_frame.on( 'select', function(){
					
					
					attachment = file_frame.state().get('selection').first().toJSON();
					
					
					$(i).attr('src', attachment.url);
					$(t).val(attachment.url);
					
					
					});
					
					
					file_frame.open();
					
					
					// Upload field reset button
					
				
				}).on("click", ".fileuploadcancel", function(){
				
				
					var t = $(this).attr("tar");
					
					
					$("input[type='text'][tar='" + t + "']").val("");
					
					
					$("img[tar='" + t + "']").prop("src", "");
				
				
				});
				
			
			})(jQuery);
			</script>           
<form class="main-form" method="post">
 <?php custom_theme_settings_fields('general-email-setup') ;?>                    
    <div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Favicon', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display favicon image in browser tab', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Favicon image url', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_favicon = get_option(THEME_PREFIX.'favicon');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_favicon)){echo $custom_theme_favicon;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>favicon" id="<?php echo THEME_PREFIX;?>favicon" class="custom_theme_favicon" value="<?php echo $custom_theme_favicon; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>favicon" data-targetimg = "#<?php echo THEME_PREFIX;?>favicon_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Favicon image will be shown browser tabs.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator"></span>
	
	<div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Inner Page Logo', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display logo at inner pages', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Inner page logo url', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_inner_page_logo = get_option(THEME_PREFIX.'inner_page_logo');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_inner_page_logo)){echo $custom_theme_inner_page_logo;} else{ echo ' '; }?>" alt="custom_theme_inner_page_logo" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>inner_page_logo" id="<?php echo THEME_PREFIX;?>inner_page_logo" class="custom_theme_inner_page_logo" value="<?php echo $custom_theme_inner_page_logo; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>inner_page_logo" data-targetimg = "#<?php echo THEME_PREFIX;?>inner_page_logo_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Logo will be shown at inner pages.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator"></span>
    
    	<div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Inner Page Banner Image', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display banner at inner pages', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Inner page banner image url', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_inner_page_banner = get_option(THEME_PREFIX.'inner_page_banner');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_inner_page_logo)){echo $custom_theme_inner_page_logo;} else{ echo ' '; }?>" alt="custom_theme_inner_page_logo" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>inner_page_banner" id="<?php echo THEME_PREFIX;?>inner_page_banner" class="custom_theme_inner_page_banner" value="<?php echo $custom_theme_inner_page_banner; ?>" />
                
                <button class="fileupload button-primary" data-target="#<?php echo THEME_PREFIX;?>inner_page_banner" data-targetimg = "#<?php echo THEME_PREFIX;?>inner_page_banner_img"><?php _e('Upload', THEME_TEXTDOMAIN);?></button>
             <small class="octane-short-desc">(<?php _e('Banner will be shown at inner pages.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
    <div class="field-head">
        <h4><?php _e('Inner Page Banner Link URL', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Banner url at inner pages', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Inner page banner link url', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_inner_page_banner_link  = get_option(THEME_PREFIX.'inner_page_banner_link');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_inner_page_logo)){echo $custom_theme_inner_page_logo;} else{ echo ' '; }?>" alt="custom_theme_inner_page_logo" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>inner_page_banner_link" id="<?php echo THEME_PREFIX;?>inner_page_banner_link" class="custom_theme_inner_page_banner_link" value="<?php echo $custom_theme_inner_page_banner_link; ?>" />
                
                
             <small class="octane-short-desc">(<?php _e('Banner url at inner pages.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
 </div> 
 
 
 <span class="octane-seprator"></span>
    <div class="field-row clearfix">
    <div class="field-head">
      <h4>
        <?php _e('Header Banner Content', THEME_TEXTDOMAIN) ;  ?>
      </h4>
    </div>
    <div class="field-main"> <span class="field-options">
      <label class="lbl-block"><?php _e('Inner page banner content', THEME_TEXTDOMAIN) ;  ?></label>
	  <?php  $custom_theme_site_banner_content  = get_option(THEME_PREFIX.'site_banner_content');?>
      <textarea  name="<?php echo THEME_PREFIX; ?>site_banner_content"><?php echo get_option(THEME_PREFIX.'site_banner_content'); ?></textarea>
      </span> <small class="octane-short-desc">(<?php _e('Banner content at above all pages.' , THEME_TEXTDOMAIN ) ; ?>)</small></div>
  </div>
 
 
    <span class="octane-seprator"></span>
	
	<div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Phone', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Display phone number at home page', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Phone Number', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_phone = get_option(THEME_PREFIX.'phone');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_phone)){echo $custom_theme_phone;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>phone" id="<?php echo THEME_PREFIX;?>phone" class="custom_theme_phone" value="<?php echo $custom_theme_phone; ?>" />
                
             <small class="octane-short-desc">(<?php _e('Phone Number will be shown home header section.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator"></span>
	<div class="field-row clearfix">                                
    <div class="field-head">
        <h4><?php _e('Map & Direction Link', THEME_TEXTDOMAIN) ;  ?></h4>
        <small class="octane-short-desc">(<?php _e('Link for map & direction at home page header section', THEME_TEXTDOMAIN) ;  ?>)</small>
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Map & Direction Link', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_map_direction_link = get_option(THEME_PREFIX.'map_direction_link');?>
           <!-- <img id="custom_theme_favicon_img" src="<?php if(!empty($custom_theme_map_direction_link)){echo $custom_theme_map_direction_link;} else{ echo ' '; }?>" alt="custom_theme_favicon" /> -->
           
                <input type="text" name="<?php echo THEME_PREFIX;?>map_direction_link" id="<?php echo THEME_PREFIX;?>map_direction_link" class="custom_theme_map_direction_link" value="<?php echo $custom_theme_map_direction_link; ?>" />
                
             <small class="octane-short-desc">(<?php _e('Map & Direction Link will be shown at home header section.' , THEME_TEXTDOMAIN ) ; ?>)</small>
         </span>
         
    </div>
 </div> 
    <span class="octane-seprator"></span>
	
	<div class="field-row clearfix">                                
    <h4>Days Open Timings-</h4>
	<?php $day_list=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	foreach($day_list as $days)
	{?>
	
	<div class="field-head">
        <h4><?php _e($days, THEME_TEXTDOMAIN) ;  ?></h4>
        
    </div>
    <div class="field-main">
         <span class="field-options">
             <label class="lbl-block"><?php _e('Open Timing', THEME_TEXTDOMAIN) ;  ?></label>
			 <?php  $custom_theme_open_timing = get_option(THEME_PREFIX.'open_timing');?>
           
           
                <input type="text" name="<?php echo THEME_PREFIX;?>open_timing[<?php echo $days;?>]" id="<?php echo THEME_PREFIX;?>open_timing[<?php echo $days;?>]" class="custom_theme_open_timing[<?php echo $days;?>]" value="<?php echo $custom_theme_open_timing[$days]; ?>" />
                
            
         </span>
         
    </div>
	<?php } ?>
 </div> 
    <span class="octane-seprator"></span>
	
	<?php submit_button(); ?>                            
</form>