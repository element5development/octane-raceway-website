<?php
add_action( 'admin_enqueue_scripts', 'custom_theme_enqueue_all_script_and_css' );

function custom_theme_enqueue_all_script_and_css( $hook_suffix ) {
    // first check that $hook_suffix is appropriate for your admin page
	wp_enqueue_script( 'jquery-ui-accordion' );
	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script( 'suggest' );
	wp_enqueue_script( 'jquery-ui-sortable' );
	wp_enqueue_style( 'wp-color-picker' );
	
	wp_register_style('jquery-ui','//code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css' );
  	wp_enqueue_style('jquery-ui' );
	
	wp_enqueue_style('octane-admin-css' , THEME_URL . 'admin/assets/css/admin/style.css') ;  
	wp_enqueue_style('octane-admin-media-css' , THEME_URL . 'admin/assets/css/admin/media.css') ;
	
	//GDP all form validator.
	wp_enqueue_script('octane-form_validation' , THEME_URL .'admin/assets/js/admin/admin-form-validation.js' ) ;
	
	//MEDIA ENQUE
	wp_enqueue_media();
}

?>