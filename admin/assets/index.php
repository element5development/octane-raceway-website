<?php
/*
 * Plugin Name: Global Directory Plugin
 * Plugin URI: http://globaldirectoryplugin.com/
 * Description: An intuitive plugin for easily creating, managing and displaying locations using Google Maps.
 * Author: GlobalDirectoryPlugin
 * Author URI: http://globaldirectoryplugin.com/
 * Version: 1.0.1
 * License: GPL3
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * Copyright 2014 by GlobalDirectoryPlugin - All rights reserved.
 */

define( 'GLOBAL_DIRECTORY_PLUGIN_VERSION', '1.0.1' );
define( 'GLOBAL_DIRECTORY_PLUGIN_FILE', __FILE__ );

if ( !defined('WP_POST_REVISIONS') )
	define( 'WP_POST_REVISIONS', 0);
	
require( dirname( __FILE__ ) . '/core/init.php' );
