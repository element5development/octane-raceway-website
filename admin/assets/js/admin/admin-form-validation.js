/*--------------------------------------------------------
/* Global direcrory forms validation
/* Version: 1.0
/*-------------------------------------------------------*/

;(function($,window,document){
	
	$(document).ready(function(){
		
		var f,t,e,f_val,f_type;
		
		f = false;
		t = true;
		
		$("#submit").on('click',function(){
										 
			
			e = false;
		
			$(this).closest("form").find("input.octane_required_field, textarea.octane_required_field, select.octane_required_field").each(function(){
				
				f_val = $(this).val().trim();
				
				if ( ! f_val ){
					e = true;
					$(this).focus()
					$(this).addClass("octane_error");
					return f;
					
				
				}
				else {
					
					 f_type = $(this).attr("data-type");
					 
					 if( f_type == 'email' ) {
						
						var email_v = $(this).val();
						
							var filter_email =  /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
								if(!filter_email.test(email_v)){
									e = true;				 
									$(this).focus();
									$(this).addClass("octane_error");
									return f;
								}
						 
					 }
					 
					 
					 if( f_type == 'number' ){
						
						var num_val = $(this).val();
					 	
						if( ! $.isNumeric(num_val) ){
							e = true;	
							$(this).focus();
							$(this).addClass("octane_error");
							return f;
						}
					 
					 }
					 
					 if( f_type == 'checkbox' ){
						
						var check_val = $(this).is(":checked");
					 	
						if( ! check_val ){
							e = true;	
							$(this).focus();
							$(this).addClass("octane_error");
							return f;
						}
					 
					 }
					
				}
				
																	  
			});
						
			if(e == false)
				return t;
			else
				return f;	
				
		});
		
		
		//On key up remove octane_error class 
		$("input.octane_required_field, textarea.octane_required_field, select.octane_required_field").on('keyup',function(){
																												  			$(this).removeClass('octane_error');
																												  		});
	
	});


})(jQuery);