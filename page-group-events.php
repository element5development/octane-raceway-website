<?php
/**
 * Template Name: group events
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
               <div class=" group-events-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-position="top" data-image-src="<?php echo $thumb_url; ?>"<?php } ?>>
               <div class="row"> 
                  <div class="innerpage-banner-in">
            <div class=" banner-top-content">
            <div class="group-big-circle">
                  <img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
                </div>
                 <div>
				 <img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
       <div><h1>
	   <?php 
								 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
								 
								 if($pagetitle1!="" || $pagetitle2!="")
									 {
										 echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
									 }
								 else
									 {
										 the_title();
									 } ?>
	   
	   </h1></div><img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" /> 
      </div>
             </div>
               </div>
              </div>
              <?php //include 'sb.php'; 
              ?>
               <div id="main-menu-sec" class="menu-innerpage">
                 <div class=" row clearfix">
             <?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
             <div id="main-menu">
               <?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
             </div>
             <?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
           </div>
		<?php include 'mm.php'; ?>
		    </div>
               <!----main-menu-sec end here--->
        </div>

        <div class=" wemake-events-sec">
         <div class=" row">
         <div class="clearfix">
         <div class=" wemake-content">
         <?php the_content();?>
         </div>
         
		 
		 <?php if ( is_active_sidebar( 'group_event_sidebar' ) ) {  dynamic_sidebar( 'group_event_sidebar' );  } ?>
		 
		 
		 
         </div>
         </div>
        </div><!--wemake-events-sec ends here-->
<?php
$parent = new WP_Query(array(
    'post_type'       => 'page',
    'post_parent'       => $post->ID,                               
    'order'             => 'ASC',
	'orderby'           => 'menu_order'
));
$var="1";
if ($parent->have_posts()) : 
	while ($parent->have_posts()) : $parent->the_post(); 
	$gei_image = get_field('geip_image', $post->ID);
	$gei_container_background = get_field('geip_background_image', $post->ID);
	$gei_background_parallax = get_field('geip_background_image_parallax', $post->ID);
	$geip_text_color = get_field('geip_text_color', $post->ID);
	$geip_button_color = get_field('geip_button_color', $post->ID);
	
	if ( has_post_thumbnail() ) {
		$thumb_page=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$thumb_url_page=$thumb_page['0'];
	} 

$_background = '';
if(!empty($gei_container_background)) {
	if($gei_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$gei_container_background.'"';
	}else{
		$_background = 'style="background: url('.$gei_container_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div id="group-corporate-bg <?php if($var%2==0) { ?> <?php echo (($gei_background_parallax == 'parallaxon' && !empty($gei_container_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?><?php } else { echo '" style="background: url('.$gei_container_background.') center top no-repeat; background-size: cover;"'; } ?>>
	<div class=" row">
		<div class=" group-corporate-in">
			<div class=" clearfix">
			<?php if($var%2==0) { ?>
				<?php if(!empty($gei_image)) : ?>
					<div class=" group-corporate-right-image"><img src="<?php echo $gei_image['sizes']['group_event_image']; ?>" alt="img"></div>
				<?php endif; ?>
				<div class=" group-corporate-right group-corp-content text-content-left">
					<h4 style="color: <?php echo $geip_text_color; ?>;"><?php the_title();?></h4>
					<p style="color: <?php echo $geip_text_color; ?>;"><?php 
						remove_filter( 'excerpt', 'wpautop' );
						echo excerpt(30);
						add_filter( 'excerpt', 'wpautop' ); ?>
					</p>
					<a href="<?php echo get_page_link($post->ID);?>" class=" learn-more <?php if($geip_button_color == 'red') { ?>learn-more-red<?php } ?>">learn more</a>
				</div>
			<?php
			} else {
				if(!empty($gei_image)) : ?>
					<div class=" group-corporate-left"><img src="<?php echo $gei_image['sizes']['group_event_image']; ?>" alt="img"></div>
				<?php endif; ?>
				<div class=" group-corporate-right group-corp-content">
					<h4 style="color: <?php echo $geip_text_color; ?>;"><?php the_title();?></h4>
					<p style="color: <?php echo $geip_text_color; ?>;"><?php 
						remove_filter( 'excerpt', 'wpautop' );
						echo excerpt(30);
						add_filter( 'excerpt', 'wpautop' ); ?>
					</p>
					<a href="<?php echo get_page_link($post->ID);?>" class=" learn-more <?php if($geip_button_color == 'red') { ?>learn-more-red<?php } ?>">learn more</a>
				</div>
				<?php
			} ?>

			</div>
		</div> 
	</div>  
</div><!--group-corporate-bg ends here--> 
<?php
unset($thumb_url_page);
$var++;
endwhile;
//unset($parent);
endif;
wp_reset_query(); ?> 
 <div class="row" style="display:none;"><?php include 'ot.php'; ?></div>      
<?php 
$gelr = get_field('gelr_text', 532); 
	if(!empty($gelr)) : ?>
	<div class="fun-kart-race-sec">
		<div class="row">
			<div class="fun-kart-race-sec-in txtcenter">
				<h2><?php echo $gelr; ?></h2>
				<a href="javascript:void(0)" class="scroll_by_id" data-value="people"><img src="<?php echo get_template_directory_uri();?>/images/down-arrow-with-box.png" alt="img"></a>
			</div>
		</div> 
	</div>
<?php endif; ?>
        <div class="people-image" id="people">
		<div class=" follow-bg"><style>.page-id-532 #foogallery-gallery-871.owl-carousel.owl-theme .owl-prev{display: none !important;}</style>
<?php echo do_shortcode('[foogallery id="871"]'); ?>

	
</div><!---follow-bg end here---->
		</div>
<?php } } ?>
<script type="text/javascript">
jQuery(document).ready(function($){
	jQuery('#email').prop( "checked", false );
	jQuery('#phone').prop( "checked", false );
});
function open_input()
{
 if (document.getElementById('phone').checked==true) 
  {
      jQuery('input#response_phone').attr('type', 'text');
  } else
  {
      jQuery('input#response_phone').attr('type', 'hidden');
  }
  if (document.getElementById('email').checked==true) 
  {
      jQuery('input#response_email').attr('type', 'text');
  } else
  {
      jQuery('input#response_email').attr('type', 'hidden');
  }
}
jQuery(document).ready(function(){

jQuery(".testimonials-slider").owlCarousel({
    loop:true,
    autoplay:true,
    margin:15,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            
        }
    }
});
});
jQuery(window).load(function(){
jQuery(".testimonials-slider.owl-carousel.owl-theme .owl-prev, #foogallery-gallery-871.owl-carousel.owl-theme .owl-prev").attr("style","display: none !important;");
jQuery(".owl-prev").hide();
});
</script>
<style type="text/css">
.gform_wrapper textarea,
.gform_wrapper input:not([type=radio]):not([type=checkbox]):not([type=submit]):not([type=button]):not([type=image]):not([type=file]) {
    font-size: inherit !important;
    margin: 0 !important;
    font-family: inherit !important;
    padding: 6px 15px !important;
    letter-spacing: normal !important;
    background: #fff !important;
    width: 100% !important;
}
.gform_wrapper .gform_footer input.button, .gform_wrapper .gform_footer input[type=submit] {
    background: #d62129 !important;
    display: block !important;
    font-size: 19px !important;
    color: #FFFFFF !important;
    font-weight: 700 !important;
    text-transform: uppercase !important;
    padding: 12px 15px !important;
    border-bottom: 5px rgba(143, 24, 36, 0.95) solid !important;
    cursor: pointer !important;
    text-align: center !important;
    max-width: 260px !important;
    width: 100% !important;
    border-radius: 7px !important;
    margin: 0px auto !important;
}
ul#input_4_17 li {
    width: 50%;
    float: left;
    margin-bottom: 30px;
}
.wemake-content-form h4 {
	font-size: 22px !important;
}
.gform_wrapper .gform_footer,
.gform_wrapper ul.gform_fields li.gfield {
	margin: 0px !important;
	padding: 0px !important;
}
.gform_wrapper label.gfield_label {
	display: none !important;
}
body #field_4_17 div.ginput_container_checkbox {
	margin: 0px !important;
}
body #field_4_17 label.gfield_label {
	display: block !important;
}
body #field_4_17 {
	height: 60px;
}
body .gform_wrapper textarea.medium {
	height: 60px;
	min-height: 60px;
	max-height: 60px;
}
</style>
<?php
get_footer();
?>