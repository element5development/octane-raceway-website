<?php
/**
 * Template Name: play
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
//$container_galleryp1= do_shortcode('[foogallery id="872"]');
//$container_galleryp2= do_shortcode('[foogallery id="873"]');
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
    
	 <div id="banner" class="innerpage-banner">
          <div class="play-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>">
             <div class="row" style="position: relative; z-index: 2;"> 
              <div class="innerpage-banner-in">
                <div class="banner-top-content">
				 <div class="play-big-circle">
				 <div class=" play-icon"> <img alt="img" src="<?php echo get_template_directory_uri();?>/images/play-icon.png"></div>
                  <img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
                </div>
                 <div class="trans-bg">
				 <img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" /><h1>
				 <?php 
						 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
						 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
						 
						 if($pagetitle1!="" && $pagetitle2!="")
							 {
								 echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
							 }
						 else
							 {
								 the_title();
							 } ?>
				 </h1><img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
				 </div>
                </div>
              </div>
           </div>
		   <div class="black-line-banner" style="display: none;"></div>
          </div>
         <?php //include 'sb.php'; 
         ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
								$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
								if(!empty($inner_logo_img))
								{
									?>
									<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
									<?php	
								}
								?>
							<div id="main-menu">
			<?php
								$defaults = array( 'menu' => 'Middle Menu' );
								wp_nav_menu($defaults);
								?>
							</div>
			<?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
							if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
								<?php
								} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
    </div>
	
<!----FIRST ROW START---->
<?php
$pr1_title = get_field('pr1_title', 155);
$pr1_icon = get_field('pr1_icon', 155);
$pr1_description = get_field('pr1_description', 155);
$pr1_container_background = get_field('pr1_container_background', 155);
$pr1_background_parallax = get_field('pr1_background_parallax', 155);
//$pr1_defaultimage1 = get_field('pr1_first_image', 155);
//$pr1_defaultimage2 = get_field('pr1_second_image', 155);
//$pr1_defaultimage3 = get_field('pr1_third_image', 155);
//$pr1_defaultimage4 = get_field('pr1_fourth_image', 155);

$_background = '';
if(!empty($pr1_container_background)) {
	if($pr1_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$pr1_container_background.'"';
	}else{
		$_background = 'style="background: url('.$pr1_container_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div id="arcade" class="arcode-sec <?php echo (($pr1_background_parallax == 'parallaxon' && !empty($pr1_container_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="row clearfix">
		<div class="catering-sec-left clearfix" id="group_imgwrap">
			<?php echo do_shortcode('[foogallery id="872"]'); ?>
		</div>
		<div class="catering-sec-right">
			<h2>
				<?php if(!empty($pr1_icon)) : ?>
					<img width="46" src="<?php echo $pr1_icon; ?>" alt="img" class="arcode-img" />		  
				<?php endif; ?>
				<?php echo $pr1_title; ?>		  
			</h2>
			<?php echo $pr1_description; ?>
		</div>
	</div>
</div>
<!----FIRST ROW END---->
<!----SECOND ROW START---->
<?php
$pr2_title = get_field('pr2_title', 155);
$pr2_icon = get_field('pr2_icon', 155);
$pr2_description = get_field('pr2_description', 155);
$pr2_container_background = get_field('pr2_container_background', 155);
$pr2_background_parallax = get_field('pr2_background_parallax', 155);
//$pr2_defaultimage1 = get_field('pr2_first_image', 155);
//$pr2_defaultimage2 = get_field('pr2_second_image', 155);
//$pr2_defaultimage3 = get_field('pr2_third_image', 155);
//$pr2_defaultimage4 = get_field('pr2_fourth_image', 155);

$_background = '';
if(!empty($pr2_container_background)) {
	if($pr2_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$pr2_container_background.'"';
	}else{
		$_background = 'style="background: url('.$pr2_container_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div id="mini-bowling" class="mini-bowling-sec <?php echo (($pr2_background_parallax == 'parallaxon' && !empty($pr2_container_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="row clearfix">
		<div class=" mini-bowling-right clearfix" id="group_imgwrap">
			<?php echo do_shortcode('[foogallery id="873"]'); ?>
		</div>

		<div class="mini-bowling-left">
			<h2>
				<img src="<?php echo $pr2_icon; ?>" alt="img" class="mini-bowl-img">
				<?php echo $pr2_title; ?>
			</h2>
			<?php echo $pr2_description; ?>
		</div>
	</div>
</div>
<!----THIRD ROW START---->
<?php
$pr3_title = get_field('pr3_title', 155);
$pr3_container_background = get_field('pr3_container_background', 155);
$pr3_background_parallax = get_field('pr3_background_parallax', 155);

$_background = '';
if(!empty($pr3_container_background)) {
	if($pr3_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$pr3_container_background.'"';
	}else{
		$_background = 'style="background: url('.$pr3_container_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div class="outdoor-game <?php echo (($pr3_background_parallax == 'parallaxon' && !empty($pr3_container_background)) ? 'parallax-window' : ''); ?>" <?php echo $_backround; ?>>
	<div class="heading-arrow-sec">
		<div class="row"> 
			<h2><?php echo $pr3_title; ?></h2>
		</div>
	</div>
	<div class="row">
		<div class="clearfix common outdoorgame-content-wrap">
      <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2sus!4v1488225146464!6m8!1m7!1sGB3f8Rg307AAAAQvxYeBhQ!2m2!1d33.53616439484158!2d-111.884346069131!3f3.8986317570272164!4f-10.33653447580403!5f0.7820865974627469" width="100%" height="450" frameborder="0" style="border:0; margin-top: 1rem;" allowfullscreen></iframe>
			<!-- <?php
			for ($pr3 = 1; $pr3 <= 2; $pr3++) :
				$pr3_title = get_field('pr3_c'.$pr3.'title', 155);
				$pr3_icon = get_field('pr3_c'.$pr3.'icon', 155);
				$pr3_description = get_field('pr3_c'.$pr3.'description', 155);
				$pr3_banner = get_field('pr3_c'.$pr3.'banner', 155); ?>
				<div class="one-half outdoor-game-sec1">
					<?php if(!empty($pr3_banner)) { ?><img src="<?php echo $pr3_banner['url']; ?>" alt="img"><?php } ?>
					<div class="outdoor-game-sec1-cont <?php echo (($pr3 == 2) ? 'outdoor-game-sec2' : ''); ?>"> 
						<h2>
							<?php if(!empty($pr3_icon)) { ?><img width="70" src="<?php echo $pr3_icon; ?>" alt="img" /><?php } ?>
							<?php echo $pr3_title; ?>
						</h2>
						<?php echo $pr3_description; ?>
					</div>
				</div>
			<?php endfor; ?> -->
		</div>
	</div>
</div>
<!----THIRD ROW END---->
<!----FOURTH ROW START---->
<?php
$pr4_title = get_field(' pr4_title', 155);
$pr4_container_background = get_field(' pr4_container_background', 155);
$pr4_background_parallax = get_field(' pr4_background_parallax', 155);

$_background = '';
if(!empty($pr4_container_background)) {
	if($pr4_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$pr4_container_background.'"';
	}else{
		$_background = 'style="background: url('.$pr4_container_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div id="exclusive-group-events" class="ex-group-event <?php echo (($pr4_background_parallax == 'parallaxon' && !empty($pr4_container_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="heading-arrow-sec">
		<div class="row">
			<h2><?php echo $pr4_title; ?></h2>
		</div>
	</div>
	<div class="row">         
		
		<div class=" clearfix common2 exclusivegroup-content-wrap">
			<?php
			for ($pr4 = 1; $pr4 <= 3; $pr4++) : 
				$pr4_ctitle = get_field(' pr4_c'.$pr4.'title', 155);
				$pr4_cicon = get_field(' pr4_c'.$pr4.'icon', 155);
				$pr4_cdescription = get_field(' pr4_c'.$pr4.'description', 155);
				$pr4_cimage = get_field(' pr4_c'.$pr4.'image', 155); ?>
				<div class="grup-one-third">
					<span class="group-item" style="background-image:url(<?php echo $pr4_cimage['url']; ?>);"></span>
					<div class="grup-one-third-cont">
						<div class="exclusivegroup-head-wrap">
							<?php if(!empty($pr4_cicon)) { ?>
								<span class="head-img">
									<img width="50" src="<?php echo $pr4_cicon; ?>" alt="img">
								</span>
							<?php } ?>
							<span class="head"><?php echo $pr4_ctitle; ?></span>
						</div>
						<?php echo $pr4_cdescription; ?>
					</div>
				</div><!--grup-one-third ends here-->
			<?php endfor; ?>
		</div>
	</div>
</div>
<!----FOURTH ROW END---->
<!----FIFTH ROW START---->
<?php
$pr5_title = get_field('pr5_title', 155);
$pr5_tag_line = get_field('pr5_tag_line', 155);
$pr5_button_title = get_field('pr5_button_title', 155);
$button_url = get_field('button_url', 155);
$pr5_background = get_field('pr5_background', 155);
$pr5_background_parallax = get_field('pr5_background_parallax', 155);

$_background = '';
if(!empty($pr5_background)) {
	if($pr5_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$pr5_background.'"';
	}else{
		$_background = 'style="background: url('.$pr5_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div class="group-event-sec txtcenter <?php echo (($pr5_background_parallax == 'parallaxon' && !empty($pr5_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="row">
		<img class="spinit bookaraceBig-tachometer" src="<?php bloginfo('template_url'); ?>/images/big-tachometer.png" alt="" />
		<img class="spinit bookaraceSmall-tachometer" src="<?php bloginfo('template_url'); ?>/images/small-tachometer.png" alt="" />
		<div class="group-event-content">
			<span class="subhead-event"><?php echo $pr5_tag_line; ?></span>
			<h1><?php echo $pr5_title; ?></h1>
			<?php if(!empty($pr5_button_title)) { ?><a href="<?php echo $button_url; ?>" class="clickhere"><?php echo $pr5_button_title; ?></a><?php } ?>
		</div>
	</div>
</div>
<!----FIFTH ROW END---->

<?php
	}
} ?>
<script type="text/javascript">
//jQuery('.catering-sec-left.clearfix').html(<?php echo json_encode($container_galleryp1); ?>);
//jQuery('.mini-bowling-right.clearfix').html(<?php echo json_encode($container_galleryp2); ?>);
function changeimage(varid)
{
var name='clickid'+varid;
var image=jQuery('#'+name).attr( 'data-value' );
jQuery('div .catering-sec-left-two img').attr('src',image);
}
function changeimagemin(varid)
{
var name='clickidmin'+varid;
var image=jQuery('#'+name).attr( 'data-value' );
jQuery('div .mini-bowling-right-two img').attr('src',image);
}
</script> 
<?php
get_footer();
?>