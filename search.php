<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
?>
    
	 <div id="banner" class="innerpage-banner">
                  <div class="about-us-banner">
                  	 <div class="row"> 
                      <div class="innerpage-banner-in txtcenter">
                <div class="banner-top-content">
                 <div class="trans-bg"><h1>Search</h1></div>
                 <div class="search-circle"><img src="<?php echo get_template_directory_uri();?>/images/blog-circle.png" alt="circle" class="spinit"/></div>
                </div>
              </div>
                   </div>
                  </div>
                <div id="main-menu-sec" class="menu-innerpage">
						<div class=" row clearfix">
							<?php
							$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
							if(!empty($inner_logo_img))
							{
								?>
								<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
								<?php	
							}
							?>
							<div id="main-menu">
								<?php
								$defaults = array( 'menu' => 'Middle Menu' );
								wp_nav_menu($defaults);
								?>
							</div>
							<?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
								if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<div class=" book-box"><a href="<?php echo $box_title_1_link;?>"><?php echo $box_title_1;?></a></div>
								<?php
								} ?>
						</div>
				</div><!----main-menu-sec end here--->
      </div>
		
            <div class="about-us-list-sec">
      <div class="row">
        <div class="about-us-list-sec-in">
		<?php if ( have_posts() ) : ?>
          <h2 class="txtcenter"><?php printf( __( 'Search Results for: %s', 'twentysixteen' ),  esc_html( get_search_query() )  ); ?></h2>
           <ul class="about-content-list">
		  <?php while ( have_posts() ) : the_post();?>
             <li>
              <div class="clearfix">
			  <?php
			  if ( has_post_thumbnail() )
								{
                                        $thumb_img=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_img_url=$thumb_img['0'];?>
										<div class="about-content-list-left">
                 <span class="about-list-img" style="background-image:url(<?php echo $thumb_img_url;?>);"></span>
               </div><!--about-content-list-left ends here-->
										<?php
                                } ?>
               
               <div class="about-content-list-right">
                 <div class=" clearfix">
                   <div class="about-us-list-head">
                      <h3><?php the_title();?></h3>
                      
                   </div>
                   
                 </div>
                  <p><a href="<?php the_permalink();?>">View More</a></p>
                 
               </div><!--about-content-list-right ends here-->
               </div>
             </li>
             <?php endwhile; 
			  else :?>
             <li>
              <div class="clearfix">
			  <div class="about-content-list-right">
                 <div class=" clearfix">
                   <div class="about-us-list-head">
                      <h3><?php _e( 'Nothing Found' ); ?></h3>
                      
                   </div>
                   
                 </div>
                  <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.' ); ?></p>
                 
               </div>
			  </div></li>
			  <?php endif;?>
          </ul>
        </div>
      </div>
    </div>
	
 <?php get_footer(); ?>