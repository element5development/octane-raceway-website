<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }
 ?>

<div class="download-sec txtcenter group-event-sec" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll 0 0 / 100% 100%;"<?php } ?>>
          <div class="row">
              <?php the_content();?>
          </div>
        </div><!--download-sec ends here-->