<?php
/**
 * Template Name: contact
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
<script>
base_path='<?php echo get_template_directory_uri().'/images/';?>';
</script>
	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
		   <div class="row" style="position: relative; z-index: 2;"> 
              <div class="innerpage-banner-in txtcenter">
              <div class=" banner-top-content">
				 <div>
					<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
					<h1>
						<div class="about-big-circle">
							<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
						</div> 
						<?php 
							 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
							 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
							 $image_url=get_post_meta($post->ID,'image_url', true);
							 $googlemap_place_address=get_post_meta($post->ID,'googlemap_place_address', true);
							 if($pagetitle1!="" || $pagetitle2!="")
								 {
									 echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								 }
							 else
								 {
									 the_title();
								 } ?>
					</h1>
					<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
				 </div>
              </div>
              </div>
           </div>
		   <div class="black-line-banner" style="display: none;"></div>
          </div>
          <?php //include 'sb.php'; 
          ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
     </div>  <!--about-us-banner ends here-->
	
    
          
	<div class="container">
		<div class=" row clearfix">
			<div class=" octane-recaway-left">
				<div class=" octane-address clearfix">
				    <?php
					$additional_information=get_option(THEME_PREFIX.'additional_information');
					$additional_information=!empty($additional_information) ? $additional_information : "OCTANE RACEWAY";
					$contact_permanent_address=get_option(THEME_PREFIX.'contact_permanent_address');
					$contact_phone=get_option(THEME_PREFIX.'contact_phone');
					$gmap_link=get_option(THEME_PREFIX.'gmap_link');
					?>
					<h2><?php echo $additional_information;?></h2>
					<?php
					if(!empty($contact_permanent_address))
					{
						?>
						<div class=" address-left-text">
						<span><img src="<?php echo get_template_directory_uri(); ?>/images/octane-map-icon.png" alt="img"></span>
						<?php if(!empty($gmap_link))
						{
							?>
							<a href="<?php echo $gmap_link;?>" target="_blank"><?php echo $contact_permanent_address;?></a>
							<?php
							
						}
						else{
							echo $contact_permanent_address;
						} ?>
						
						</div>
						<?php
					}
					
					if(!empty($contact_phone))
					{
						?>
						<div class=" address-right-text">
						<span><img src="<?php echo get_template_directory_uri(); ?>/images/mobile-icon.png" alt="img"></span><a href="tel:<?php echo $contact_phone;?>"><?php echo $contact_phone;?></a>
						</div>
						<?php
					}
					?>
					
				</div>
				<?php
				if(!empty($image_url))
				{
					?>
					<div class=" octane-recaway-img">
			
					<img src="<?php echo $image_url;?>" alt="img">
					</div>
					<?php
					
				} ?>
				
			</div>
			<div id="map_fill_address" style="display:none;"><?php echo $googlemap_place_address;?></div>
			<div class=" octane-recaway-right" id="map_show_on_page" style="height:506px; width:420px;">
			
			</div>
			
					<?php 
					wp_enqueue_script( 'octane-map-script-site' );
					wp_enqueue_script( 'octane-extends-map-script' );
					?>
		</div>
	</div><!----container end here--->
	<?php the_content();?>
        
	<?php 
	}
}
	
	?>
	<div class="row gform-events"> <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?></div>
<div class=" regular-hours-bg">
<div  class=" hours-heading-sec">
<div class="row"> 
<h2>HOURS</h2>
</div>
<h2 id="hours"></h2>
</div>
<div class=" row">
<div class=" regular-hours-in clearfix">
<?php if ( is_active_sidebar( 'contact_regular_hours_sidebar' ) ) {  dynamic_sidebar( 'contact_regular_hours_sidebar' );  } ?>
</div>
</div>
</div><!----regular-hours end here--->
<?php if ( is_active_sidebar( 'contact_holiday_hours_sidebar' ) ) {  ?>
<div class=" holiday-bg">
<div class=" row">
<div class=" regular-hours-in clearfix">

<?php dynamic_sidebar( 'contact_holiday_hours_sidebar' ); ?>

</div>
</div>
</div><!----holiday-sec end here---> <?php } ?>
<div class=" follow-bg">
<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } else {
$instagram_link= get_option( THEME_PREFIX.'instagram_link');
if($instagram_link!="")
{
	?>
	<div class=" row">
		<div class="follow-content">
			<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
		</div>
	</div>
	
	<?php }
}
?>
	
</div><!---follow-bg end here---->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/validation-index.js"></script>
<?php
get_footer();
?>