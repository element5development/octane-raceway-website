<?php
/**
 * Template Name: eat-drink
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
$special_banner= get_option( THEME_PREFIX.'inner_page_banner');
$container_gallery_e= do_shortcode('[foogallery id="868"]');
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
                  <div class="drink-eat-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
                  	 <div class="row" data-os-animation="fadeIn" data-os-animation-delay="0s" style="position: relative; z-index: 2;"> 
                      <div class="innerpage-banner-in">
					  <div class=" banner-top-content">
						<div class="eat-drink-circle">
						<div class=" drink-icon"> <img src="<?php echo get_template_directory_uri();?>/images/eat-drink-icon.png" alt="img" width="44"></div>
							<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
						</div> 
						<div class="eat-offset">
						<div ><img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" /><h1>
						<?php 
								 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
								 
								 if($pagetitle1!="" || $pagetitle2!="")
									 {
										 echo '<span class="trans-bg">'.$pagetitle1.'</span> '.$pagetitle2;
									 }
								 else
									 {
										 the_title();
									 } ?>
						</h1><img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" /></div>
						
						</div>

					  </div>
                        
						 
                      </div>
                   </div>
				   <div class="black-line-banner" style="display: none;"></div>
                  </div>
                <?php //include 'sb.php'; 
                ?>
                <div id="main-menu-sec" class="menu-innerpage">
						<div class=" row clearfix">
							<?php
							$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
							if(!empty($inner_logo_img))
							{
								?>
								<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
								<?php	
							}
							?>
							<div id="main-menu">
								<?php
								$defaults = array( 'menu' => 'Middle Menu' );
								wp_nav_menu($defaults);
								?>
							</div>
							<?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
								if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
								<?php
								} ?>
						</div>
		<?php include 'mm.php'; ?>
				</div><!----main-menu-sec end here--->
      </div>
				
            <div class="container">
            		<div class=" row clearfix">
                    <div class="content">
                        <?php the_content();?>
                    </div>
					<?php 
	}
} ?>
                    <?php get_sidebar('foodmenu');?>
                    </div>
            </div><!--trackside-bar-sec ends here-->
			<?php
			$top_heading=get_post_meta($post->ID,'top_heading', true);					 $middle_heading=get_post_meta($post->ID,'middle_heading', true);
			$timings=get_post_meta($post->ID,'timings', true);					 $background_image=get_post_meta($post->ID,'background_image', true);
			$catering_postname=get_post_meta($post->ID,'catering_postname', true);
			$top_heading= !empty($top_heading) ? $top_heading : "Join us for";
			$middle_heading= !empty($middle_heading) ? $middle_heading : "HAPPY HOUR!";
			$timings= !empty($timings) ? $timings : "Monday – Friday 4 – 7PM";
			
			$hhi_background = get_field('hhi_background', 153);
			$hhi_background_parallax = get_field('hhi_background_parallax', 153);
			$hhi_title = get_field('hhi_title', 153);
			$hhi_tag_line = get_field('hhi_tag_line', 153);
			$hhi_schedules = get_field('hhi_schedules', 153);
			?>
            <div class="happy-hour-sec  <?php echo (($hhi_background_parallax == 'parallaxon' && !empty($hhi_background)) ? 'parallax-window' : ''); ?>" <?php echo (!empty($hhi_background) ? 'data-parallax="scroll" data-image-src="'.$hhi_background.'"' : ''); ?>>
                <div class="row">
					<img class="spinit bookaraceBig-tachometer" src="<?php bloginfo('template_url'); ?>/images/big-tachometer-white.png" alt="" />
					<img class="spinit bookaraceSmall-tachometer" src="<?php bloginfo('template_url'); ?>/images/small-tachometer-white.png" alt="" />
                   <div class="happy-hour-sec-in">
                     <span class="join-text"><?php echo $hhi_tag_line;?></span>
                     <h1><?php echo $hhi_title;?></h1>
                     <span class="trans-bg day-text"><?php echo $hhi_schedules;?></span>
                   </div>
                </div>
            </div> <!--happy-hour-sec ends here-->
    
	<!--WE DO CATERING START-->
	<?php
		$wdc_title = get_field('wdc_title', 153);
		$wdc_description = get_field('wdc_description', 153);
		$wdc_button_title = get_field('wdc_button_title', 153);
		$wdc_popup_button_file = get_field('wdc_popup_button_file', 153);
		$wdc_first_image = get_field('wdc_first_image', 153);
		$wdc_second_image = get_field('wdc_second_image', 153);
		$wdc_third_image = get_field('wdc_third_image', 153);
		$wdc_fourth_image = get_field('wdc_fourth_image', 153);
	?>
	<div class="catering-sec">
		<div class="row clearfix">
			<div class="catering-sec-left clearfix" id="group_imgwrap">
				<div class="catering-sec-left-one">
					<?php if(!empty($wdc_first_image)) { ?>
					<div class="one">
						<a href="javascript:void(0)" data-value="<?php echo $wdc_first_image['sizes']['eat_drink_big']; ?>" data-value-big="<?php echo $wdc_first_image['url']; ?>" class="changeimage" id="list-image1">
							<div class="one-img" id="change_img1">
								<img src="<?php echo $wdc_first_image['sizes']['eat_drink_small']; ?>" alt="" />
								<div class="one-img-hov">+</div>
							</div>
						</a>
					</div>
					<?php } ?>
					<?php if(!empty($wdc_second_image)) { ?>
					<div class="one">
						<a href="javascript:void(0)" data-value="<?php echo $wdc_second_image['sizes']['eat_drink_big']; ?>" data-value-big="<?php echo $wdc_second_image['url']; ?>" class="changeimage" id="list-image2">
							<div class="one-img" id="change_img2">
								<img src="<?php echo $wdc_second_image['sizes']['eat_drink_small']; ?>" alt="" />
								<div class="one-img-hov">+</div>
							</div>
						</a>
					</div>
					<?php } ?>
					<?php if(!empty($wdc_third_image)) { ?>
					<div class="one">
						<a href="javascript:void(0)" data-value="<?php echo $wdc_third_image['sizes']['eat_drink_big']; ?>" data-value-big="<?php echo $wdc_third_image['url']; ?>" class="changeimage" id="list-image3">
							<div class="one-img" id="change_img3">
								<img src="<?php echo $wdc_third_image['sizes']['eat_drink_small']; ?>" alt="" />
								<div class="one-img-hov">+</div>
							</div>
						</a>
					</div>
					<?php } ?>
					<?php if(!empty($wdc_fourth_image)) { ?>
					<div class="one">
						<a href="javascript:void(0)" data-value="<?php echo $wdc_fourth_image['sizes']['eat_drink_big']; ?>" data-value-big="<?php echo $wdc_fourth_image['url']; ?>" class="changeimage" id="list-image4">
							<div class="one-img" id="change_img4">
								<img src="<?php echo $wdc_fourth_image['sizes']['eat_drink_small']; ?>" alt="" />
								<div class="one-img-hov">+</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
				<div class="catering-sec-left-two">
					<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $wdc_first_image['url']; ?>">
						<?php if(!empty($wdc_first_image)) { ?>
							<img class="target_image list-image1" src="<?php echo $wdc_first_image['sizes']['eat_drink_big']; ?>" alt="img">
						<?php } ?>
						<?php if(!empty($wdc_second_image)) { ?>
							<img class="target_image list-image2" src="<?php echo $wdc_second_image['sizes']['eat_drink_big']; ?>" alt="img" style="display: none;">
						<?php } ?>
						<?php if(!empty($wdc_third_image)) { ?>
							<img class="target_image list-image3" src="<?php echo $wdc_third_image['sizes']['eat_drink_big']; ?>" alt="img" style="display: none;">
						<?php } ?>
						<?php if(!empty($wdc_fourth_image)) { ?>
							<img class="target_image list-image4" src="<?php echo $wdc_fourth_image['sizes']['eat_drink_big']; ?>" alt="img" style="display: none;">
						<?php } ?>
					</a>
				</div>
			</div>
			<div class="catering-sec-right">
				<h2><?php echo $wdc_title; ?></h2>
				<?php echo $wdc_description; ?>
				<p class="hideMobileButton"><a href="#popup-terms1" class="popup-with-form"><button class=" submit-btn1 download-btn"><?php echo $wdc_button_title; ?></button></a></p>
				<p class="showMobileButton"><a href="<?php echo $wdc_popup_button_file; ?>" target="_blank"><button class=" submit-btn1 download-btn"><?php echo $wdc_button_title; ?></button></a></p>
			</div>
		</div>
	</div>
	<!--WE DO CATERING END-->
						 
<div class=" regular-hours-bg">
<div  class=" hours-heading-sec">
<div class="row"> 
<h2>HOURS</h2>
</div>
</div>
<div class=" row">
<div class=" regular-hours-in clearfix">
<?php if ( is_active_sidebar( 'contact_regular_hours_sidebar' ) ) {  dynamic_sidebar( 'contact_regular_hours_sidebar' );  } ?>
</div>
</div>
</div><!----regular-hours end here--->
<?php if ( is_active_sidebar( 'contact_holiday_hours_sidebar' ) ) { ?>
<div class=" holiday-bg">
<div class=" row">
<div class=" regular-hours-in clearfix">

<?php   dynamic_sidebar( 'contact_holiday_hours_sidebar' );  ?>

</div>
</div>
</div><!----holiday-sec end here--->
				<?php } ?>		 
<script type="text/javascript">
function changeimage(varid)
{
var name='clickid'+varid;
var image=jQuery('#'+name).attr( 'data-value' );
jQuery('div .catering-sec-left-two img').attr('src',image);
}
</script>

<?php
if(!empty($wdc_popup_button_file)) :
?>
<div id="popup-terms1" class="popupbox-main white-popup-block mfp-hide">
	<div class="popup-content">
		<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $wdc_popup_button_file; ?>" type="application/pdf" internalinstanceid="27"></div>
	</div>
</div>
<?php endif; ?>

<?php
$pdf_file = get_field('menuboards_pdf_file', 153);
if(!empty($pdf_file)) :
?>
<div id="popup-terms2" class="popupbox-main mfp-hide white-popup-block">
	<div class="popup-content">
		<div class="popupbox-inner popupbox-inner-scroll"><embed src="<?php echo $pdf_file; ?>" type="application/pdf" width="100%" height="450"></embed></div>
	</div>
</div>
<?php endif; ?>
<script>
jQuery('.catering-sec-left.clearfix').html(<?php echo json_encode($container_gallery_e); ?>);
</script>
<?php
get_footer();
?>