<style> 
.export_action{
    width: 60px;
    height: 30px;
    font-weight: 600;
	border-radius: 3px !important;	
	}
.export_search_logs{    
	background:#fafafa;
	border-color:#999;
	border-radius: 3px !important;
	   
    /*left: 510px*/
	
	}
.free_sample{
position:absolute;
	
	border-color:#999;
	border-radius: 3px !important;
	    top: 7px;
		font-size:16px;
		left: 80px
		
}	
</style>
<h1> Subscribers List-</h1>

<?php
class gdp_search_log_list_table extends WP_List_Table {
    
	//var $example_data;

	 function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'sample_log',     //singular name of the listed records
            'plural'    => 'sample_logs',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }
	
    
    function column_default($item, $column_name){
        switch($column_name){
			case 'to':
            case 'subject':
			    return $item[$column_name];			
            default:
                return $item[$column_name]; //Show the whole array for troubleshooting purposes
        }
    }

    function column_sent($item){
	
        
        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            /*$1%s*/ $item['sent'],
            /*$2%s*/ $item['ID'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }
   
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
           'name'     	=> __('Name'),			
            'email_address'     	=> __('Email Address')
            
        );
        return $columns;
    }

    


    function get_bulk_actions() {
	
        $actions = array(
            'delete'    => __('Delete',GDPTEXTDOMAIN)
        );
        return $actions;
    }

	

    function process_bulk_action() {
         global $wpdb;
        //Detect when a bulk action is being triggered...
		
		
		
        if( 'delete'===$this->current_action() ) {
			
			
			
            //wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }


   
    function prepare_items() {
	
        global $wpdb,$table_prefix;//This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 10;
     
     
        $columns = $this->get_columns();
		
        $hidden = array();
        
        
        
   
        $this->_column_headers = array($columns, $hidden, $sortable);
        
   
        $this->process_bulk_action();
        
       	$where_clause = '';
		$arr_query_vars = '';
		$arr_query_vars[] = 1;
		/*if(isset($_REQUEST['s']) && !empty($_REQUEST['s'])){
			$where_clause .= ' AND email_to  like %s ';	
			$arr_query_vars[] = '%'.$_REQUEST['s'].'%';		
		}
		
		
		
		if(isset($_REQUEST['filter']) && !empty($_REQUEST['filter'])){
			$where_clause .= ' AND email_type = %s ';
			$arr_query_vars[] = $_REQUEST['filter'];
		}*/
        
		
		$sort_order = ''; 
		if(isset($_REQUEST['orderby']) && !empty($_REQUEST['orderby'])){
			
			$order_by = $_REQUEST['orderby'];
			
			$order = $_REQUEST['order'];
			
			$sort_order = "ORDER BY ".$order_by." ".$order;	
			
		}else{
			$sort_order = "ORDER BY sub_id DESC";	
		}
		
		
		$query = $wpdb->prepare("SELECT * FROM  ".$table_prefix."subscribe_users ".$where_clause." ". $sort_order , $arr_query_vars );
		//print_r($query);
		//exit();
		
		$_SESSION['log_query'] = $query;		
		$results =  $wpdb->get_results($query);
		
		$data_arr = array();
		
		if($results){
		
			foreach($results as $result){
			
		//exit();			
			  	//$data_arr[] = array('ID' => $result->id, 'sent' => $result->email_date, 'to' => $result->email_to, 'subject' => $result->email_subject, 'content' => $result->content);	
			$data_arr[] = array('ID' => $result->sub_id,'name'=> $result->name, 'email_address' => $result->email);	
			}
		
		} 
		
		$data = $data_arr;	
		//gdp_print_r($data);
        $current_page = $this->get_pagenum();
        
       
        $total_items = count($data);
        
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        
      
        $this->items = $data;
        
        
      
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }

	function extra_tablenav($which){
		
	}
}


$gdp_search_log = new gdp_search_log_list_table();

$gdp_search_log->prepare_items();
?>



<style type="text/css">
.white-popup{
position: relative;
background: #FFF;
padding: 20px;
width:auto;
max-width: 70%;
margin: 20px auto;
}

</style>

<form class="main-form" method="get">
    
	<?php //$gdp_search_log->search_box('Search' , 'search_id'); ?>	
    <?php $gdp_search_log->display(); ?>
</form>

