<?php
/**
 * Template Name: private-parties
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
               <div class="private-parties-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
               <div class="row"> 
                  <div class="innerpage-banner-in">
                     <div class="banner-top-content">			 
					   <div class="private-big-circle">
					   <img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
					  </div>
					  <img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
					   <h1>
					 <?php 
								 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
								 if($pagetitle1!="" && $pagetitle2!="")
									 {
										 echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
									 }
								 else
									 {
										 the_title();
									 } ?>
					 
					 </h1>
					 <img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
                     </div>
                  </div>
               </div>
              </div>
              <?php //include 'sb.php'; 
              ?>
               <div id="main-menu-sec" class="menu-innerpage">
                 <div class=" row clearfix">
             <?php
							$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
							if(!empty($inner_logo_img))
							{
								?>
								<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
								<?php	
							}
							?>
             <div id="main-menu">
               <?php
										$defaults = array( 'menu' => 'Middle Menu' );
										wp_nav_menu($defaults);
										?>
             </div>
             <?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
								if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
								<?php
								} ?>
           </div>
		<?php include 'mm.php'; ?>
		      </div><!----main-menu-sec end here--->
        </div>

<!-- FIRST ROW START-->
<?php
$ppfr_title = get_field('ppfr_title', $post->ID);
$ppfr_content = get_field('ppfr_content', $post->ID);
$row_background = get_field('row_background', $post->ID);
$row_background_parallax = get_field('row_background_parallax', $post->ID);

$_background = '';
if(!empty($row_background)) {
	if($row_background_parallax == 'parallaxon') {
		$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$row_background.'"';
	}else{
		$_background = 'style="background: url('.$row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background = 'style="background: #FFF;"';
}
?>
<div class="party-idea-sec txtcenter <?php echo (($row_background_parallax == 'parallaxon' && !empty($row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
	<div class="row">
		<div class="party-idea-sec txtcenter">
			<h2><?php echo $ppfr_title; ?></h2>
			<?php echo $ppfr_content; ?>
		</div>
	</div>
</div>
<!-- FIRST ROW END-->


<!-- NEW ROW START 

<?php if ( get_field('ppnr_title') ) { ?>
	<?php
	$ppnr_title = get_field('ppnr_title', $post->ID);
	$ppnr_content = get_field('ppnr_content', $post->ID);
	$ppnr_row_background = get_field('ppnr_row_background', $post->ID);
	$ppnr_row_background_parallax = get_field('ppnr_row_background_parallax', $post->ID);

	$array_files = '';

	$_background2 = '';
	if(!empty($ppnr_row_background)) {
		if($ppnr_row_background_parallax == 'parallaxon') {
			$_background2 = 'data-parallax="scroll" data-position="top" data-image-src="'.$ppnr_row_background.'"';
		}else{
			$_background2 = 'style="background: url('.$ppnr_row_background.') center top no-repeat; background-size: cover;"';
		}
	}else{
		$_background2 = 'style="background: #FFF;"';
	}
	?>
	<div id="birthday-section" class="event-section <?php echo (($ppnr_row_background_parallax == 'parallaxon' && !empty($ppnr_row_background)) ? 'parallax-window' : ''); ?>" <?php //echo $_background2; ?>>
		<div class="row">
			<div class="txtcenter">
				<h1><?php //echo $ppnr_title; ?></h1>
				<?php //echo $ppnr_content; ?>
				<div class="packages-main">
					<h2><?php //echo get_field('ppnr_psecond_title'); ?></h2>
					<p><span class="pkg-downarrow"><i class="fa fa-chevron-down" style="color: #363636;"></i></span></p>
					<div class="clearfix party-package-common">
						<?php 
						for ($ppnr = 1; $ppnr <= 3; $ppnr++) : 
							$ppnr_ctitle = get_field('ppnr_c'.$ppnr.'title', $post->ID);
							$ppnr_cguest_count = get_field('ppnr_c'.$ppnr.'guest_count', $post->ID);
							$ppnr_ccontent = get_field('ppnr_c'.$ppnr.'content', $post->ID);
							$ppnr_cbutton_title = get_field('ppnr_c'.$ppnr.'button_title', $post->ID);
							$ppnr_cbutton_url = get_field('ppnr_c'.$ppnr.'button_url', $post->ID);
							$ppnr_cbutton_file = get_field('ppnr_c'.$ppnr.'button_popup_file', $post->ID);
							$array_files[$ppnr] = $ppnr_cbutton_file;
						?>	
							<div class="party-package">
								<div class="party-pkg-head"><?php //echo $ppnr_ctitle; ?></div>
								<div class="party-pkg-in">
									<span class="guests-count"><?php //echo $ppnr_cguest_count; ?></span>
									<?php //echo $ppnr_ccontent; ?>
									<?php if(!empty($ppnr_cbutton_title)) : ?>
										<?php if(empty($ppnr_cbutton_file) && empty($ppnr_cbutton_url)) { ?>
												<p>
													<a class="red-button red-button-mid scroll_by_class" data-value="private-parties-form"><?php //echo $ppnr_cbutton_title; ?></a>
												</p>
										<?php }else{ ?>
												<p>
													<a class="red-button red-button-mid <?php echo (!empty($ppnr_cbutton_file)? 'popup-with-form' : ''); ?>" href="<?php echo (!empty($ppnr_cbutton_url)? $ppnr_cbutton_url : (!empty($ppnr_cbutton_file)? '#ppnr_c'.$ppnr.'button_popup_file' : 'javascript:void(0)')); ?>">
														<?php //echo $ppnr_cbutton_title; ?>
													</a>
												</p>
										<?php } ?>
									<?php endif; ?>
								</div>
							</div>
						<?php endfor; ?>
					</div>
					
					<?php if(!empty($array_files)) { 
							foreach ($array_files as $f => $file) { ?>
								<div id="ppnr_c<?php echo $f; ?>button_popup_file" class="popupbox-main white-popup-block mfp-hide">
									<div class="popup-content">
										<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $file; ?>" type="application/pdf" internalinstanceid="3"></div>
									</div>
								</div>
					<?php 	}
						 } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
 NEW ROW END-->
 
 
<!-- SECOND ROW START-->
<?php
$ppsr_title = get_field('ppsr_title', $post->ID);
$ppsr_content = get_field('ppsr_content', $post->ID);
$ppsr_row_background = get_field('ppsr_row_background', $post->ID);
$ppsr_row_background_parallax = get_field('ppsr_row_background_parallax', $post->ID);

$array_files = '';

$_background2 = '';
if(!empty($ppsr_row_background)) {
	if($ppsr_row_background_parallax == 'parallaxon') {
		$_background2 = 'data-parallax="scroll" data-position="top" data-image-src="'.$ppsr_row_background.'"';
	}else{
		$_background2 = 'style="background: url('.$ppsr_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background2 = 'style="background: #FFF;"';
}
?>
<div id="birthday-section" class="event-section junior-birthday-parties <?php echo (($ppsr_row_background_parallax == 'parallaxon' && !empty($ppsr_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background2; ?>>
	<div class="row">
		<div class="txtcenter">
			<h1><?php echo $ppsr_title; ?></h1>
			<?php echo $ppsr_content; ?>
			<div class="packages-main">
				<h2><?php echo get_field('ppfor_pheading'); ?></h2>
				<p><span class="pkg-downarrow"><i class="fa fa-chevron-down" style="color: #363636;"></i></span></p>
				<div class="clearfix party-package-common">
					<?php 
					for ($ppsr = 1; $ppsr <= 3; $ppsr++) : 
						$ppsr_ctitle = get_field('ppsr_c'.$ppsr.'title', $post->ID);
						$ppsr_cguest_count = get_field('ppsr_c'.$ppsr.'guest_count', $post->ID);
						$ppsr_ccontent = get_field('ppsr_c'.$ppsr.'content', $post->ID);
						$ppsr_cbutton_title = get_field('ppsr_c'.$ppsr.'button_title', $post->ID);
						$ppsr_cbutton_url = get_field('ppsr_c'.$ppsr.'button_url', $post->ID);
						$ppsr_cbutton_file = get_field('ppsr_c'.$ppsr.'button_popup_file', $post->ID);
						$array_files[$ppsr] = $ppsr_cbutton_file;
					?>	
						<div class="party-package">
							<div class="party-pkg-head"><?php echo $ppsr_ctitle; ?></div>
							<div class="party-pkg-in">
								<span class="guests-count"><?php echo $ppsr_cguest_count; ?></span>
								<?php echo $ppsr_ccontent; ?>
								<?php if(!empty($ppsr_cbutton_title)) : ?>
									<?php if(empty($ppsr_cbutton_file) && empty($ppsr_cbutton_url)) { ?>
											<p>
												<a class="red-button red-button-mid scroll_by_class" data-value="private-parties-form"><?php echo $ppsr_cbutton_title; ?></a>
											</p>
									<?php }else{ ?>
											<p>
												<a class="red-button red-button-mid <?php echo (!empty($ppsr_cbutton_file)? 'popup-with-form' : ''); ?>" href="<?php echo (!empty($ppsr_cbutton_url)? $ppsr_cbutton_url : (!empty($ppsr_cbutton_file)? '#ppsr_c'.$ppsr.'button_popup_file' : 'javascript:void(0)')); ?>">
													<?php echo $ppsr_cbutton_title; ?>
												</a>
											</p>
									<?php } ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endfor; ?>
				</div>
				
				<?php if(!empty($array_files)) { 
						foreach ($array_files as $f => $file) { ?>
							<div id="ppsr_c<?php echo $f; ?>button_popup_file" class="popupbox-main white-popup-block mfp-hide">
								<div class="popup-content">
									<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $file; ?>" type="application/pdf" internalinstanceid="3"></div>
								</div>
							</div>
				<?php 	}
					 } ?>
			</div>
		</div>
	</div>
</div>
<!-- SECOND ROW END-->
<!-- THIRD ROW START-->
<?php
$pptr_title = get_field('pptr_title', $post->ID);
$pptr_content = get_field('pptr_content', $post->ID);
$pptr_row_background = get_field('pptr_row_background', $post->ID);
$pptr_row_background_parallax = get_field('pptr_row_background_parallax', $post->ID);

$_background3 = '';
if(!empty($pptr_row_background)) {
	if($pptr_row_background_parallax == 'parallaxon') {
		$_background3 = 'data-parallax="scroll" data-position="top" data-image-src="'.$pptr_row_background.'"';
	}else{
		$_background3 = 'style="background: url('.$pptr_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background3 = 'style="background: #FFF;"';
}
?>
<div id="adult-section" class="adult-sec-in event-section adult-birthday-parties <?php echo (($pptr_row_background_parallax == 'parallaxon' && !empty($pptr_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background3; ?>>
	<div class="row">
		<div class="txtcenter">
			<h1><?php echo $pptr_title; ?></h1>
			<?php echo $pptr_content; ?>
			<div class="packages-main">
				<h2><?php echo get_field('company_meetings_heading'); ?></h2>
				<p><span class="pkg-downarrow"><i class="fa fa-chevron-down"></i></span></p>
				<div class="clearfix party-package-common">
					<?php 
					for ($pptr = 1; $pptr <= 3; $pptr++) : 
						$pptr_ctitle = get_field('pptr_c'.$pptr.'title', $post->ID);
						$pptr_cguest_count = get_field('pptr_c'.$pptr.'guest_count', $post->ID);
						$pptr_ccontent = get_field('pptr_c'.$pptr.'content', $post->ID);
						$pptr_cbutton_title = get_field('pptr_c'.$pptr.'button_title', $post->ID);
						$pptr_cbutton_url = get_field('pptr_c'.$pptr.'button_url', $post->ID); 
						$pptr_cbutton_file = get_field('pptr_c'.$pptr.'button_popup_file', $post->ID);
						$array_files[$pptr] = $pptr_cbutton_file; ?>
						<div class="party-package">
							<div class="party-pkg-head"><?php echo $pptr_ctitle; ?></div>
							<div class="party-pkg-in">
								<span class="guests-count"><?php echo $pptr_cguest_count; ?></span><p></p>
								<?php echo $pptr_ccontent; ?>
								
								<?php if(!empty($pptr_cbutton_title)) : ?>
									<?php if(empty($pptr_cbutton_file) && empty($pptr_cbutton_url)) { ?>
											<p>
												<a class="red-button red-button-mid scroll_by_class" data-value="private-parties-form"><?php echo $pptr_cbutton_title; ?></a>
											</p>
									<?php }else{ ?>
											<p>
												<a class="red-button red-button-mid <?php echo (!empty($pptr_cbutton_file)? 'popup-with-form' : ''); ?>" href="<?php echo (!empty($pptr_cbutton_url)? $pptr_cbutton_url : (!empty($pptr_cbutton_file)? '#ppsr_c'.$pptr.'button_popup_file' : 'javascript:void(0)')); ?>">
													<?php echo $pptr_cbutton_title; ?>
												</a>
											</p>
									<?php } ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endfor; ?>
				</div>
				
				<?php if(!empty($array_files)) { 
						foreach ($array_files as $f => $file) { ?>
							<div id="ppsr_c<?php echo $f; ?>button_popup_file" class="popupbox-main white-popup-block mfp-hide">
								<div class="popup-content">
									<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $file; ?>" type="application/pdf" internalinstanceid="3"></div>
								</div>
							</div>
				<?php 	}
					 } ?>
			</div>
		</div>
	</div>
</div>
<!-- THIRD ROW END-->
<!-- FOURT ROW START-->
<?php
$ppfor_title = get_field('ppfor_title', $post->ID);
$ppfor_content = get_field('ppfor_content', $post->ID);
$ppfor_row_background = get_field('ppfor_row_background', $post->ID);
$ppfor_row_background_parallax = get_field('ppfor_row_background_parallax', $post->ID);

$_background4 = '';
if(!empty($ppfor_row_background)) {
	if($ppfor_row_background_parallax == 'parallaxon') {
		$_background4 = 'data-parallax="scroll" data-position="top" data-image-src="'.$ppfor_row_background.'"';
	}else{
		$_background4 = 'style="background: url('.$ppfor_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background4 = 'style="background: #FFF;"';
}
?>
<div id="bachelor-section" class="event-section bachelorbachelorette-parties <?php echo (($ppfor_row_background_parallax == 'parallaxon' && !empty($ppfor_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background4; ?>>
	<div class="row">
		<div class="txtcenter">
			<h1><?php echo $ppfor_title; ?></h1>
			<?php echo $ppfor_content; ?>
			<div class="packages-main">
				<h2><?php echo (is_page( 774 ) ? get_field('ppsecond_title') : 'PARTY PACKAGES') ?> </h2>
				<p><span class="pkg-downarrow"><i class="fa fa-chevron-down"></i></span></p>
				<div class="clearfix party-package-common">
					<?php 
					for ($ppfor = 1; $ppfor <= 3; $ppfor++) : 
						$ppfor_c1title = get_field('ppfor_c'.$ppfor.'title', $post->ID);
						$ppfor_c1text = get_field('ppfor_c'.$ppfor.'text', $post->ID);
						$ppfor_c1content = get_field('ppfor_c'.$ppfor.'content', $post->ID);
						$ppfor_c1button_title = get_field('ppfor_c'.$ppfor.'button_title', $post->ID);
						$ppfor_c1button_url = get_field('ppfor_c'.$ppfor.'button_url', $post->ID); 
						$ppfor_cbutton_file = get_field('pptr_c'.$ppfor.'button_popup_file', $post->ID);
						$array_files[$ppfor] = $ppfor_cbutton_file; ?>
						<div class="party-package">
							<div class="party-pkg-head"><?php echo $ppfor_c1title; ?></div>
							<div class="party-pkg-in">
								<span class="guests-count"><?php echo $ppfor_c1text; ?></span><p></p>
								<?php echo $ppfor_c1content; ?>
								
								<?php if(!empty($ppfor_c1button_title)) : ?>
									<?php if(empty($pptr_cbutton_file) && empty($ppfor_c1button_url)) { ?>
											<p>
												<a class="red-button red-button-mid scroll_by_class" data-value="private-parties-form"><?php echo $ppfor_c1button_title; ?></a>
											</p>
									<?php }else{ ?>
											<p>
												<a class="red-button red-button-mid <?php echo (!empty($ppfor_cbutton_file)? 'popup-with-form' : ''); ?>" href="<?php echo (!empty($ppfor_c1button_url)? $ppfor_c1button_url : (!empty($ppfor_cbutton_file)? '#ppsr_c'.$pptr.'button_popup_file' : 'javascript:void(0)')); ?>">
													<?php echo $ppfor_c1button_title; ?>
												</a>
											</p>
									<?php } ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endfor; ?>
				</div>
				
				<?php if(!empty($array_files)) { 
						foreach ($array_files as $f => $file) { ?>
							<div id="ppsr_c<?php echo $f; ?>button_popup_file" class="popupbox-main white-popup-block mfp-hide">
								<div class="popup-content">
									<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $file; ?>" type="application/pdf" internalinstanceid="3"></div>
								</div>
							</div>
				<?php 	}
					 } ?>
			</div>
		</div>
	</div>
</div>
<!-- FOURT ROW END-->
<!-- FIFTH ROW START-->
<?php
$ppffr_title = get_field('ppffr_title', $post->ID);
$ppffr_tag_line = get_field('ppffr_tag_line', $post->ID);
$ppffr_button_title = get_field('ppffr_button_title', $post->ID);
$ppffr_popup_button_file = get_field('ppffr_popup_button_file', $post->ID);
$ppffr_row_background = get_field('ppffr_row_background', $post->ID);
$ppffr_row_background_parallax = get_field('ppffr_row_background_parallax', $post->ID);

$_background5 = '';
if(!empty($ppffr_row_background)) {
	if($ppffr_row_background_parallax == 'parallaxon') {
		$_background5 = 'data-parallax="scroll" data-position="top" data-image-src="'.$ppffr_row_background.'"';
	}else{
		$_background5 = 'style="background: url('.$ppffr_row_background.') center top no-repeat; background-size: cover;"';
	}
}else{
	$_background5 = 'style="background: #FFF;"';
}
?>
<div class="download-sec txtcenter group-event-sec <?php echo (($ppffr_row_background_parallax == 'parallaxon' && !empty($ppffr_row_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background5; ?>>
	<div class="row">
		<p><span class="download-subhead"><?php echo $ppffr_tag_line; ?></span><big><?php echo $ppffr_title; ?></big><a href="#popup-terms" class="popup-with-form"><?php echo $ppffr_button_title; ?></a></p>
		<?php if(!empty($ppffr_popup_button_file)) : ?>
			<div id="popup-terms" class="popupbox-main mfp-hide white-popup-block">
				<div class="popup-content">
					<div class="popupbox-inner popupbox-inner-scroll">
						<iframe width="100%" height="500" src="https://docs.google.com/viewer?url=<?php echo $ppffr_popup_button_file; ?>&embedded=true" type="application/pdf"></iframe>
					</div>
					<!-- popupbox-inner here-->
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
<!-- FIFTH ROW END-->
<!-- SIXTH ROW START-->
<div id="book-your-party" class="contact-form-bg private-parties-form <?php echo $post->post_name;?>">
	<div class=" row">
		<div class="form-main-bg newGravityform">
			<h2><?php //echo (is_page( 774 )? 'BOOK YOUR <br />CORPORATE EVENT!' : 'BOOK YOUR PARTY!'); ?></h2>
			<?php
    	if(is_page(774)) {
    		echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');
      }
    	if(is_page(507)) {
        echo do_shortcode('[gravityform id="11" title="false" description="false" ajax="true"]');
      }
      ?>
		</div>
	</div>
</div><!----contact-form-bg end here--->
<!-- SIXTH ROW END-->

<style>
  .newGravityform .gform_wrapper input[type=text], .newGravityform .gform_wrapper textarea {background: #fff !important;}
  .gform_wrapper label.gfield_label {display: none !important;}
  .gform_wrapper select {background: #fff !important; border: 1px solid #999 !important; font-weight: bold;}
  .newGravityform .racing-select {width: 33.2% !important;}
  .newGravityform .gform_wrapper .gform_footer input[type=submit] { background: #d62129; display: block; font-size: 19px; color: #FFFFFF; font-weight: 700; text-transform: uppercase; padding: 12px 15px; border-bottom: 5px rgba(143, 24, 36, 0.95) solid; cursor: pointer; text-align: center; max-width: 260px; width: 100%; border-radius: 7px; margin: 0px auto; }
</style>
<?php				
	}
} ?>
     
        <div class="fun-kart-race-sec">
              <div class="row">
                  <div class="fun-kart-race-sec-in txtcenter">
                    <h2>CHECK OUT OUR AWESOME PHOTOS!</h2>
					<?php $awesome_event_spaces_link=get_post_meta($postid,'awesome_event_spaces_link', true);?>
                    <a href="javascript:void(0)" class="scroll_by_id" data-value="owl-slider_widget-2"><img src="<?php echo get_template_directory_uri();?>/images/down-arrow-with-box.png" alt="img"></a>
                  </div>
              </div> 
            </div>

<div class=" follow-bg">
	<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } else {
	$instagram_link= get_option( THEME_PREFIX.'instagram_link');
	if($instagram_link!="") { ?>
		<div class=" row">
			<div class="follow-content">
				<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
			</div>
		</div>
	<?php }
	} ?>
</div><!---follow-bg end here---->			
			
<script type="text/javascript">
jQuery(document).ready(function() {
	var x = jQuery('#heard_about_us').val();
	//alert(x);
		if(x=='Other')
		{
			jQuery('#racing_select_hide').fadeIn();
		}
		else{
			jQuery('#racing_select_hide').fadeOut();
		}
	});
	function open_new()
	{
		var z = jQuery('#heard_about_us').val();
	//alert(x);
		if(z=='Other')
		{
			jQuery('#racing_select_hide').fadeIn();
		}
		else{
			jQuery('#racing_select_hide').fadeOut();
		}
		
	}
</script>

<?php
get_footer();
?>