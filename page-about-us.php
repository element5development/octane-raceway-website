<?php
/**
 * Template Name: about-us
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
			<div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
          </div>
          <?php //include 'sb.php'; 
          ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url();?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
     </div>  <!--about-us-banner ends here-->
	
<div class="container">
	<div class="row">
		<div class="about-content-sec-in clearfix common">
			<?php
			for ($auc = 1; $auc <= 2; $auc++) :
				$auc_title = get_field('auc_c'.$auc.'title', 82);
				$auc_content = get_field('auc_c'.$auc.'content', 82); ?>
				<div class="one-half about-content">
					<h4 class="red-text"><?php echo $auc_title; ?></h4>
					<?php echo $auc_content; ?>
				</div>
			<?php endfor; ?>
		</div>
	</div>
</div>
   
    <div class="people-img-sec">
	<div class="owl-carousel">
	<?php 
			          
					   $attachments3 = get_attached_media( 'image', $post->ID );
					   
						 if ( $attachments3 ) {
							foreach ( $attachments3 as $attachment3 ) 
							{
								$full_image3= wp_get_attachment_image_src( $attachment3->ID, 'full' );
							    $full_image_path3=$full_image3[0];
								$thumb_image3= wp_get_attachment_image_src( $attachment3->ID, 'slider' );
							    $thumb_image_path3=$thumb_image3[0];
							   ?>
							   
							<div class="item">
								<a href="<?php echo $full_image_path3;?>" class="fancybox" data-fancybox-group="gallery" >
								  <img src="<?php echo $thumb_image_path3;?>" /></a>
							</div>
							   
							   <?php
							}
						 } ?>
            
            
			</div>
          
	</div><!--people-img-sec ends here-->
     
	<?php 
	}
}
	
	?>
    <div class="about-us-list-sec">
      <div class="row">
        <div class="about-us-list-sec-in">
          <h2 class="txtcenter">MEET OUR TEAM</h2>
           <ul class="about-content-list">
		   <?php $args = array( 'post_type'  => 'team' );
                 $posts_array = get_posts( $args );
				 foreach($posts_array as $post) : setup_postdata( $post ); ?>
             <li class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="0.2s">
              <div class="clearfix">
			  <?php
			  if ( has_post_thumbnail() )
								{
                                        $thumb_img=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_img_url=$thumb_img['0'];?>
										<div class="about-content-list-left">
                 <span class="about-list-img" style="background-image:url(<?php echo $thumb_img_url;?>);"></span>
               </div><!--about-content-list-left ends here-->
										<?php
                                } ?>
               
               <div class="about-content-list-right">
                 <div>
                    <div class="about-us-list-head">
                      <h3><?php the_title();?></h3>
                      <span class="post"><?php echo get_post_meta( $post->ID, 'designation', true );?></span>
                   </div>
                    <div class="about-us-list-icon">
				   <?php
				   $linkedin_url=get_post_meta( $post->ID, 'linkedin_url', true );
				   $email_id=    get_post_meta( $post->ID, 'email_id', true );
				   if(!empty($linkedin_url))
				   {
					?><a href="<?php echo $linkedin_url;?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php
				   }
				   if(!empty($email_id))
				   {
					?><a href="mailto:<?php echo $email_id;?>"><i class="fa fa-envelope"></i></a><?php
				   }
				   ?>
                     
                   </div>
                    <div class="clear"></div>
                 </div>
                  <p><?php the_content();?></p>
                 
               </div><!--about-content-list-right ends here-->
               </div>
             </li>
             <?php endforeach; 
             wp_reset_postdata();?>
          </ul>
        </div>
      </div>
    </div>
	
<?php
get_footer();
?>