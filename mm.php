
		<?php
			$mm1_image = get_option(THEME_PREFIX.'menu_item_1_image');
			$mm1_title = get_option(THEME_PREFIX.'menu_item_1_title');
			$mm1_link = get_option(THEME_PREFIX.'menu_item_1_link');
			$mm1_content = get_option(THEME_PREFIX.'menu_item_1_content');

			$mm2_image = get_option(THEME_PREFIX.'menu_item_2_image');
			$mm2_title = get_option(THEME_PREFIX.'menu_item_2_title');
			$mm2_link = get_option(THEME_PREFIX.'menu_item_2_link');
			$mm2_content = get_option(THEME_PREFIX.'menu_item_2_content');

			$mm3_image = get_option(THEME_PREFIX.'menu_item_3_image');
			$mm3_title = get_option(THEME_PREFIX.'menu_item_3_title');
			$mm3_link = get_option(THEME_PREFIX.'menu_item_3_link');
			$mm3_content = get_option(THEME_PREFIX.'menu_item_3_content');

			$mm4_image = get_option(THEME_PREFIX.'menu_item_4_image');
			$mm4_title = get_option(THEME_PREFIX.'menu_item_4_title');
			$mm4_link = get_option(THEME_PREFIX.'menu_item_4_link');
			$mm4_content = get_option(THEME_PREFIX.'menu_item_4_content');

		?>
		<?php if (!empty($mm1_title) || !empty($mm2_title) || !empty($mm3_title) || !empty($mm4_title)): ?>
		<div class="main-submenu" style="display: none;">
			<div class="row">
				<ul id="theGrandMenu">
					<li>
						<a href="">
							<img src="" alt="">
							<span></span>
							<p></p>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<?php endif ?>