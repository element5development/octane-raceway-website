<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */
?>

<div class="racing-license">
          <div class="row">
            <div class="racing-license-in txtcenter">
              <h2><?php the_title();?></h2>
              <div class="racing-value-main">
                  <div class="clearfix common2">
				  
                   <?php 
				   remove_filter( 'the_content', 'wpautop' );
				   the_content();
				   add_filter( 'the_content', 'wpautop' );
				   ?>
                  </div>
              </div>
            </div>
          </div>
        </div><!--racing-license ends here-->