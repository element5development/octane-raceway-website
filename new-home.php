<?php get_header();
$thumb=wp_get_attachment_image_src( get_post_thumbnail_id(42), 'full' );
$thumb_url=$thumb['0'];
$container_gallery= do_shortcode('[foogallery id="859"]');
$container_gallery2= do_shortcode('[foogallery id="861"]');
$container_gallery3= do_shortcode('[foogallery id="862"]');
$container_gallery4= do_shortcode('[foogallery id="865"]');
$container_gallery5= do_shortcode('[foogallery id="866"]');
$container_gallery6= do_shortcode('[foogallery id="867"]');
$container_gallery7= do_shortcode('[foogallery id="887"]');
$bg_image= get_option( THEME_PREFIX.'home_bg_image');
$home_id = get_the_ID ();
?>
<div id="video-wrap" class="home home-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?> has-sbanner" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>

    <video id="my-video" preload="auto" autoplay muted loop>
        <source src="<?php echo get_option( THEME_PREFIX.'home_bg_video'); ?>" type="video/mp4">
        <source src="<?php echo get_option( THEME_PREFIX.'home_bg_video2'); ?>" type="video/webm">
    </video>
	<div class=" row">
		<div class=" banner-top clearfix">
			<div class=" logo">
				<?php
				   $logo_img= get_option( THEME_PREFIX.'home_page_logo');
				   $logo_img = !empty($logo_img)? $logo_img : get_template_directory_uri().'/images/logo.png';
				   $facebook_link= get_option( THEME_PREFIX.'facebook');
				   $twitter_link= get_option( THEME_PREFIX.'twitter');
				   $instagram_link= get_option( THEME_PREFIX.'instagram_link');
				   $googleplus_link= get_option( THEME_PREFIX.'google_plus');
				   $youtube_link= get_option( THEME_PREFIX.'youtube');
				   ?>

				<a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_img;?>" alt="img"></a>
			</div>
			<div class="banner-top-right">
				<?php
				if(!empty($facebook_link))
				{
					?>
					<span><a target="_blank" href="<?php echo $facebook_link;?>"><i class="fa fa-facebook"></i></a></span>
					<?php
				}
				if(!empty($twitter_link))
				{
					?>
					<span><a target="_blank" href="<?php echo $twitter_link;?>"><i class="fa fa-twitter"></i></a></span>
					<?php
				}
				if(!empty($instagram_link))
				{
					?>
					<span><a target="_blank" href="<?php echo $instagram_link;?>"><i class="fa fa-instagram"></i></a></span>
					<?php
				}
				if(!empty($youtube_link))
				{
					?>
					<span><a target="_blank" href="<?php echo $youtube_link;?>"><i class="fa fa-youtube-play"></i></a></span>
					<?php
				}
				if(!empty($googleplus_link))
				{
					?>
					<span><a target="_blank" href="<?php echo $googleplus_link;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gplus-red-iocn.png" alt="img"></a></span>
					<?php
				} ?>
			</div>
		</div>
		<div class=" banner-top-content">
			<div class="banner-top-content-text">
				<?php
				$heading_upper= get_option( THEME_PREFIX.'home_heading_section_1');
				$heading_lower= get_option( THEME_PREFIX.'home_heading_section_2');
				if(!empty($heading_upper))
				{
				?>
					<span class=" trans-bg"><h1><?php echo $heading_upper;?></h1></span>
				<?php
				}
				if(!empty($heading_lower))
				{
				?>
					<h1><?php echo $heading_lower;?></h1>
				<?php
				}
				?>
				<div class=" banner-top-content-btn">
					<?php
					$box_title_1= get_option( THEME_PREFIX.'box_title_1');
					$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
					$box_title_2= get_option( THEME_PREFIX.'box_title_2');
					$box_title_2_link= get_option( THEME_PREFIX.'box_title_2_link');
					$box_title_3= get_option( THEME_PREFIX.'box_title_3');
					$box_title_3_link= get_option( THEME_PREFIX.'box_title_3_link');
					if($box_title_1!="" && $box_title_1_link!="")
					{
					?>
						<a href="<?php echo $box_title_1_link;?>"><?php echo $box_title_1;?></a>
					<?php
					}
					if($box_title_2!="" && $box_title_2_link!="")
					{
					?>
						<a href="<?php echo $box_title_2_link;?>" class="takeyour-btn2"><?php echo $box_title_2;?></a>
					<?php
					}
					if($box_title_3!="" && $box_title_3_link!="")
					{
					?>
						<a href="<?php echo $box_title_3_link;?>" class="takeyour-btn"><?php echo $box_title_3;?></a>
					<?php
					}
					?>
				</div>
				<span class="down-arrow"><a href="javascript:void(0)" class="scroll_by_id" data-value="section1"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-arrow.png" alt="img"></a></span>
			</div>
			<div class="homebanner-big-circle"><img src="<?php echo get_template_directory_uri(); ?>/images/homebanner-big-circle.png" alt="circle" class="spinit"></div>
			<div class="homebanner-small-circle"><img src="<?php echo get_template_directory_uri(); ?>/images/homebanner-small-circle.png" alt="circle" class="spinit"></div>
		</div>
	</div>
</div><!--banner end here-->
<?php

if ( is_user_logged_in() )
	{
		?>
		<style>.menufix{ top:68px!important;}</style>
		<?php
	}
	else
	{
		?>
		<style>.menufix{ top:37px!important;}</style>
		<?php
	}

?>


<div class="homec-page main-menu-wrap">
	<div id="main-menu-sec">
		<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img)) { ?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
			<?php
			} ?>

			<div id="main-menu">
				<?php
					$defaults = array( 'menu' => 'Middle Menu' );
					wp_nav_menu($defaults);
				?>
				<?php include 'mm.php'; ?>
			</div>

			<?php
			if($box_title_1!="" && $box_title_1_link!="") { ?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
			<?php
			} ?>
		</div>
		<?php
			$mm1_image = get_option(THEME_PREFIX.'menu_item_1_image');
			$mm1_title = get_option(THEME_PREFIX.'menu_item_1_title');
			$mm1_link = get_option(THEME_PREFIX.'menu_item_1_link');
			$mm1_content = get_option(THEME_PREFIX.'menu_item_1_content');

			$mm2_image = get_option(THEME_PREFIX.'menu_item_2_image');
			$mm2_title = get_option(THEME_PREFIX.'menu_item_2_title');
			$mm2_link = get_option(THEME_PREFIX.'menu_item_2_link');
			$mm2_content = get_option(THEME_PREFIX.'menu_item_2_content');

			$mm3_image = get_option(THEME_PREFIX.'menu_item_3_image');
			$mm3_title = get_option(THEME_PREFIX.'menu_item_3_title');
			$mm3_link = get_option(THEME_PREFIX.'menu_item_3_link');
			$mm3_content = get_option(THEME_PREFIX.'menu_item_3_content');

			$mm4_image = get_option(THEME_PREFIX.'menu_item_4_image');
			$mm4_title = get_option(THEME_PREFIX.'menu_item_4_title');
			$mm4_link = get_option(THEME_PREFIX.'menu_item_4_link');
			$mm4_content = get_option(THEME_PREFIX.'menu_item_4_content');

		?>
	</div><!----main-menu-sec end here--->
</div>

<!--HOMEPAGE CONTENT ROWS START-->
<?php
for ($x = 1; $x <= 4; $x++) {
	$FCtitle = get_field('first_column'.$x.'_title', 42);
	$FCdescription = get_field('first_column'.$x.'_description', 42);
	$FCurltitle = get_field('first_column'.$x.'_url_title', 42);
	$FCsurl = get_field('first_column'.$x.'_url', 42);
	$FCcustomurl = get_field('first_column'.$x.'_custom_button_url', 42);
	$FCfimage = get_field('first_column'.$x.'_fimage', 42);
	$FCsimage = get_field('first_column'.$x.'_simage', 42);
	$FCtimage = get_field('first_column'.$x.'_timage', 42);

	$FCurl = '';
	if(!empty($FCcustomurl)) {
		$FCurl = $FCsurl.$FCcustomurl;
	} else {
		$FCurl = $FCsurl;
	}

	$FCtimage_position = get_field('first_column'.$x.'_thumbnail_position', 42);

	$SCtitle = get_field('second_column'.$x.'_title', 42);
	$SCdescription = get_field('second_column'.$x.'_description', 42);
	$SCurltitle = get_field('second_column'.$x.'_url_title', 42);
	$SCcheck = get_field('allow_external_link', 42);
	$SCExurl = get_field('button_url_external', 42);
	$SCsurl = get_field('second_column'.$x.'_url', 42);
	$SCsection = get_field('second_column'.$x.'_page_section', 42);
	$SCfimage = get_field('second_column'.$x.'_first_image', 42);
	$SCsimage = get_field('second_column'.$x.'_second_image', 42);
	$SCtimage = get_field('second_column'.$x.'_third_image', 42);

	$SCurl = '';
	if(!empty($SCsection)) {
		$SCurl = $SCsurl.$SCsection;
	} else {
		if(isset($SCcheck)) {
			$SCurl = $SCExurl;
		} else {
			$SCurl = $SCsurl;
		}
	}
	$view_menu = get_field('home_r'.$x.'_view_menu', 42);

	$SCtimage_position = get_field('second_column'.$x.'_thumbnail_position', 42);

	$container_background = get_field('container'.$x.'_background', 42);
	$container_background_parrallax = get_field('container'.$x.'_background_parallax', 42);
	$tachometer = get_field('tachometer'.$x.'_image', 42);

	$background_style = '';
	$parallax = '';

	if ( $container_background != '' ) {
		if ($container_background_parrallax == 'parallaxon') {
			$background_style = 'data-parallax="scroll" data-position="top center" data-image-src="'.$container_background.'"';
			$parallax = 'class="parallax-window"';
		} else {
			$background_style = 'style="background: url('.$container_background.') center top no-repeat; background-size: cover;"';
		}
	} else {
		$background_style = 'style="background: #FFF;"';
	} ?>
	<div <?php echo $background_style; ?>id="section<?php echo $x; ?>" <?php echo $parallax; ?>>
		<div class=" row clearfix">
			<div class=" racing-header">
				<div class="racing-arrow <?php if($x==2 || $x==4){ echo 'white-text';} ?>">
					<?php echo '.o'.$x; ?>
					<br />
					<i class="fa fa-chevron-down"></i>
				</div>
				<div class="racing-heading">
					<span class="paging-icon-mobile">
						<img src="<?php echo $tachometer;?>" alt="g" id="spin_img1">
					</span>
					<span class="high-speed-text <?php if($x==2 || $x==4){ echo 'color-light';} ?>">
						<?php
						if($x==2 || $x==4){
							echo $FCtitle;
						} else {
							echo '<span>'.$FCtitle.'</span>';
						}
						?>
					</span>
					<span class="paging-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/pagin-circle1.png" alt="g" id="spin_img" class="spinit">
						<img src="<?php echo $tachometer; ?>" alt="icon1" class="paging-icon-inner">
					</span>
					<span class="high-speed-text">
						<?php
						if($x==1 || $x==3){
							echo $SCtitle;
						} else {
							echo '<span>'.$SCtitle.'</span>';
						}
						?>
					</span>
				</div>
			</div>
			<div class="racing-col-common clearfix">
				<div class="racing-col-2">
					<div class="images-wapper <?php if($x==2 || $x==4){ ?>white-hoverPlus<?php } ?>">
						<?php if(!empty($FCfimage) && !empty($FCsimage)) : ?>
							<?php if($FCtimage_position == 'left') : ?>
								<ul class=" racing-list" id="racing-list-row<?php echo $x; ?>-col1">
									<?php if(!empty($FCfimage)) : ?>
									<li class="current_thumbnail">
										<a href="javascript:void(0);" class="change_image" id="list-img1" data-value-small="<?php echo $FCfimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCfimage['url']; ?>">
											<img src="<?php echo $FCfimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($FCsimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img2" data-value-small="<?php echo $FCsimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCsimage['url']; ?>">
											<img src="<?php echo $FCsimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($FCtimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img3" data-value-small="<?php echo $FCtimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCtimage['url']; ?>">
											<img src="<?php echo $FCtimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
								</ul>
								<ul class="racing-list2">
									<li>
										<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $FCfimage['url']; ?>">
											<img class="target_image list-img1" src="<?php echo $FCfimage['sizes']['home_big']; ?>" alt="img">
											<img class="target_image list-img2" src="<?php echo $FCsimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
											<img class="target_image list-img3" src="<?php echo $FCtimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
										</a>
									</li>
								</ul>
							<?php else : ?>
								<ul class="racing-list2" style="margin-right: 0.7%;">
									<li>
										<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $FCfimage['url']; ?>">
											<img class="target_image list-img1" src="<?php echo $FCfimage['sizes']['home_big']; ?>" alt="img">
											<img class="target_image list-img2" src="<?php echo $FCsimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
											<img class="target_image list-img3" src="<?php echo $FCtimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
										</a>
									</li>
								</ul>
								<ul class=" racing-list" id="racing-list-row<?php echo $x; ?>-col1" style="margin-right: 0;">
									<?php if(!empty($FCfimage)) : ?>
									<li class="current_thumbnail">
										<a href="javascript:void(0);" class="change_image" id="list-img1" data-value-small="<?php echo $FCfimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCfimage['url']; ?>">
											<img src="<?php echo $FCfimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($FCsimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img2" data-value-small="<?php echo $FCsimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCsimage['url']; ?>">
											<img src="<?php echo $FCsimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($FCtimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img3" data-value-small="<?php echo $FCtimage['sizes']['home_big']; ?>" data-value-big="<?php echo $FCtimage['url']; ?>">
											<img src="<?php echo $FCtimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
								</ul>
							<?php endif; ?>
						<?php else: ?>
							<?php if(!empty($FCfimage)) : ?>
								<?php if(isset($view_menu) && !empty($view_menu)) : ?>
									<a href="#hp_popup_menu<?php echo $x; ?>" class="hp_popup_menu">
										<span class="hp_view_menu">DOWNLOAD CATERING MENU</span>
										<img src="<?php echo $FCfimage['sizes']['home_1big']; ?>" alt="" />
									</a>
								<?php else: ?>
									<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $FCfimage['url']; ?>">
										<img src="<?php echo $FCfimage['sizes']['home_1big']; ?>" alt="" />
									</a>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<div <?php if($x==2 || $x==4){ ?>class=" white-text"<?php } ?>><?php echo $FCdescription; ?></div>
					<a href="<?php echo $FCurl; ?>" class="button-default <?php if($x==2 || $x==4){ echo 'button-white';} ?>"><?php echo $FCurltitle; ?></a>
				</div>

				<div class="racing-col-2">
					<div class="images-wapper <?php if($x==2 || $x==4){ ?>white-hoverPlus<?php } ?>">
						<?php if(!empty($SCfimage) && !empty($SCsimage)) : ?>
							<?php if($SCtimage_position == 'left') : ?>
								<ul class=" racing-list" id="racing-list-row<?php echo $x; ?>-col2">
									<?php if(!empty($SCfimage)) : ?>
									<li class="current_thumbnail">
										<a href="javascript:void(0);" class="change_image" id="list-img1" data-value-small="<?php echo $SCfimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCfimage['url']; ?>">
											<img src="<?php echo $SCfimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($SCsimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img2" data-value-small="<?php echo $SCsimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCsimage['url']; ?>">
											<img src="<?php echo $SCsimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($SCtimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img3" data-value-small="<?php echo $SCtimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCtimage['url']; ?>">
											<img src="<?php echo $SCtimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
								</ul>
								<ul class="racing-list2">
									<li>
										<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $SCfimage['url']; ?>">
											<img class="target_image list-img1" src="<?php echo $SCfimage['sizes']['home_big']; ?>" alt="img">
											<img class="target_image list-img2" src="<?php echo $SCsimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
											<img class="target_image list-img3" src="<?php echo $SCtimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
										</a>
									</li>
								</ul>
							<?php else: ?>
								<ul class="racing-list2" style="margin-right: 0.7%;">
									<li>
										<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $SCfimage['url']; ?>">
											<img class="target_image list-img1" src="<?php echo $SCfimage['sizes']['home_big']; ?>" alt="img">
											<img class="target_image list-img2" src="<?php echo $SCsimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
											<img class="target_image list-img3" src="<?php echo $SCtimage['sizes']['home_big']; ?>" alt="img" style="display: none;">
										</a>
									</li>
								</ul>
								<ul class=" racing-list" id="racing-list-row<?php echo $x; ?>-col2" style="margin-right: 0;">
									<?php if(!empty($SCfimage)) : ?>
									<li class="current_thumbnail">
										<a href="javascript:void(0);" class="change_image" id="list-img1" data-value-small="<?php echo $SCfimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCfimage['url']; ?>">
											<img src="<?php echo $SCfimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($SCsimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img2" data-value-small="<?php echo $SCsimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCsimage['url']; ?>">
											<img src="<?php echo $SCsimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
									<?php if(!empty($SCtimage)) : ?>
									<li>
										<a href="javascript:void(0);" class="change_image" id="list-img3" data-value-small="<?php echo $SCtimage['sizes']['home_big']; ?>" data-value-big="<?php echo $SCtimage['url']; ?>">
											<img src="<?php echo $SCtimage['sizes']['thumbnail'];?>" alt="img">
											<span>
												<?php if($x==2 || $x==4): ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon-white.png" alt="img">
												<?php else: ?>
													<img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img">
												<?php endif; ?>
											</span>
										</a>
									</li>
									<?php endif; ?>
								</ul>
							<?php endif; ?>
						<?php else: ?>
							<?php if(!empty($SCfimage)) : ?>
								<?php if(isset($view_menu) && !empty($view_menu)) : ?>
									<a href="#hp_popup_menu<?php echo $x; ?>" class="popup-with-form">
										<span class="hp_view_menu">VIEW MENU</span>
										<span>
											<img src="<?php echo $SCfimage['sizes']['home_1big']; ?>" alt="" />
										</span>
									</a>
								<?php else: ?>
									<a class="fancybox" data-fancybox-group="gallery" href="<?php echo $SCfimage['url']; ?>">
										<img src="<?php echo $SCfimage['sizes']['home_1big']; ?>" alt="" />
									</a>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<div <?php if($x==2 || $x==4){ ?>class=" white-text"<?php } ?>><?php echo $SCdescription; ?></div>
					<?php if(isset($view_menu) && !empty($view_menu)) : ?>
						<a href="#hp_popup_menu<?php echo $x; ?>" class="button-default popup-with-form <?php if($x==2 || $x==4){ echo 'button-white';} ?>"><?php echo $SCurltitle; ?></a>
					<?php else: ?>
						<a href="<?php echo $SCurl; ?>" class="button-default <?php if($x==2 || $x==4){ echo 'button-white';} ?>"><?php echo $SCurltitle; ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if(isset($view_menu) && !empty($view_menu)) : ?>
		<div id="hp_popup_menu<?php echo $x; ?>" class="popupbox-main white-popup-block mfp-hide">
			<div class="popup-content">
				<div class="popupbox-inner popupbox-inner-scroll"><embed width="100%" height="450" src="<?php echo $view_menu; ?>" type="application/pdf" internalinstanceid="27"></div>
			</div>
		</div>
	<?php endif; ?>
<?php
} ?>

<div style="background: url(<?php the_field('fifth_row_background', 42); ?>) center top no-repeat; background-size: cover;" id="section5">
	<div class="full-width">
        <div class="banner">
		  <div class="row clearfix">
			<?php the_field('fifth_row_content', 42); ?>
		  </div>
		</div>
	</div>
</div>

<div style="background: url(<?php the_field('sixth_row_background', 42); ?>) center top no-repeat; background-size: cover;" id="section6">
	<div class="full-width">
        <div class="banner">
		  <div class="row clearfix">
			<?php the_field('sixth_row_content', 42); ?>
		  </div>
		</div>
	</div>
</div>

<!--HOMEPAGE CONTENT ROWS END-->

<div class="folow-us-lisitn-sec">
	<div class=" row">
		<div class=" follow-listing">
			<ul>
				<?php
				$menu_items = wp_get_nav_menu_items('Footer Logo Menu');
				//r_print_r($menu_items);exit;
				$inc="1";
				foreach ( (array) $menu_items as $key => $menu_item ) {
					$title = $menu_item->title;
					$url = $menu_item->url;

					?>
					<li><a href="<?php echo $url;?>"><span class="follow-icon<?php echo $inc;?>"></span><small><?php echo $title;?></small></a></li>
					<?php
					$inc++;
				}  ?>
			</ul>
		</div>
	</div>
</div><!---follow-listing end here---->

<div class=" follow-bg">
	<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } else {
	$instagram_link= get_option( THEME_PREFIX.'instagram_link');
	if($instagram_link!="") { ?>
		<div class=" row">
			<div class="follow-content">
				<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
			</div>
		</div>
	<?php }
	} ?>
</div><!---follow-bg end here---->
<script>
jQuery(document).ready(function() {
 jQuery("#my-video").backgroundVideo({
    $videoWrap: $("#video-wrap"),
    $outerWrap: $("#outer-wrap"),
    preventContextMenu: true,
    parallaxOptions: {
        effect: 1.9
    }
 });
});
 jQuery("#section1").waypoint(function() {
  $(window).trigger('resize.px.parallax');
});
jQuery(document).ready(function() {
window.dispatchEvent(new Event('scroll'));
});
 jQuery('#section1 .racing-col-2:first-child .images-wapper').html(<?php echo json_encode($container_gallery); ?>);
 jQuery('#section1 .racing-col-2:nth-child(2) .images-wapper').html(<?php echo json_encode($container_gallery7); ?>);
 jQuery('#section2 .racing-col-2:first-child .images-wapper').html(<?php echo json_encode($container_gallery2); ?>);
 jQuery('#section2 .racing-col-2:nth-child(2) .images-wapper').html(<?php echo json_encode($container_gallery3); ?>);
 jQuery('#section3 .racing-col-2:first-child .images-wapper').html(<?php echo json_encode($container_gallery4); ?>);
 jQuery('#section4 .racing-col-2:first-child .images-wapper').html(<?php echo json_encode($container_gallery5); ?>);
 jQuery('#section4 .racing-col-2:nth-child(2) .images-wapper').html(<?php echo json_encode($container_gallery6); ?>);
</script>
<?php get_footer();?>