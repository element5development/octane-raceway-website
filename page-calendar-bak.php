<?php
/**
 * Template Name: calendar
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
                 <div class="calendar-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
                   <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
                  </div>
                  <?php include 'sb.php'; ?>
                 <div id="main-menu-sec" class="menu-innerpage">
        <div class=" row clearfix">
        <?php
							$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
							if(!empty($inner_logo_img))
							{
								?>
								<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
								<?php	
							}
							?>
        <div id="main-menu">
        <?php
								$defaults = array( 'menu' => 'Middle Menu' );
								wp_nav_menu($defaults);
								?>
		</div>
        <?php
							$box_title_1= get_option( THEME_PREFIX.'box_title_1');
							$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
								if($box_title_1!="" && $box_title_1_link!="")
								{
								?>
								<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
								<?php
								} ?>
        </div>
		<?php include 'mm.php'; ?>
</div><!----main-menu-sec end here--->
           </div><!----banner end here--->

<div class="container">
	<div class="row clearfix">

<?php
$type = 'event';
$featured_args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1,
  'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => 'featured-event',
		),
	),
);
$events_args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1,
);

$my_query = null;
$my_query = new WP_Query;
$featured_results = $my_query->query($featured_args);
$events_results = $my_query->query($events_args);

$featured_postID = '';
$x = 0;
foreach ($featured_results as $featured => $f) {
	if($x == 0 ){
		$featured_postID .= $f->ID;
	} else {
		$featured_postID .= ', '.$f->ID;
	}
	$x++;
}

$events_postID = '';
$y = 0;
foreach ($events_results as $events => $e) {
	if($y == 0 ){
		$events_postID .= $e->ID;
	} else {
		$events_postID .= ', '.$e->ID;
	}
	$y++;
}

$current_month = date('m');
$current_year= "20".date('y');
$current_date= date('d');
$current_time=time();
$featured_event = "select post_id from ".$wpdb->prefix."custom_event_table where start_date>'$current_time' AND post_id IN ($featured_postID) ORDER BY start_date ASC LIMIT 1";
$featured_eventinfo = $wpdb->get_results($featured_event);

if(count($featured_eventinfo)>0) {
	$post_id=$featured_eventinfo[0]->post_id;
	$eventstartdate=get_post_meta($post_id,'event_date', true);
	$start_time=get_post_meta($post_id,'event_start', true);
	$end_time=get_post_meta($post_id,'event_finish', true);
	$event_desc=get_post_meta($post_id,'event_desc', true);

	$event_month=date('m', $eventstartdate);
	$event_year="20".date('y', $eventstartdate);
	$event_date=date('d', $eventstartdate);
?>
	<div class="calender-contant-left">
		<div class="calender-contant-left-in">
			<div class="calender-contant-left-head">
				<h3>FEATURED EVENT</h3>
				<img src="<?php echo get_template_directory_uri();?>/images/down-red-arrow.png" alt="img">
			</div>
			<div class="calender-contant-left-cont"><h3 class="featured-event-month"><?php echo date('F', $eventstartdate);?></h3>
				<span class="featured-event-date"><?php echo $event_date;?></span>
				<span class="pub-txt"><?php echo $start_time." - ".$end_time;?></span>
				<p><?php echo $event_desc;?></p>
			</div>
		</div>
	</div>
<?php 
} else {
	$event = "select post_id from ".$wpdb->prefix."custom_event_table where start_date>'$current_time' AND post_id IN ($events_postID) ORDER BY start_date ASC LIMIT 1";
	$eventinfo = $wpdb->get_results($event);
	if(count($eventinfo)>"0") {
		$post_id=$eventinfo[0]->post_id;
		$eventstartdate=get_post_meta($post_id,'event_date', true);
		$start_time=get_post_meta($post_id,'event_start', true);
		$end_time=get_post_meta($post_id,'event_finish', true);
		$event_desc=get_post_meta($post_id,'event_desc', true);
		
		$event_month=date('m', $eventstartdate);
		$event_year="20".date('y', $eventstartdate);
		$event_date=date('d', $eventstartdate); ?>
		<div class="calender-contant-left">
			<div class="calender-contant-left-in">
				<div class="calender-contant-left-head">
					<h3>EVENT</h3>
					<img src="<?php echo get_template_directory_uri();?>/images/down-red-arrow.png" alt="img">
				</div>
				<div class="calender-contant-left-cont"><h3 class="featured-event-month"><?php echo date('F', $eventstartdate);?></h3>
					<span class="featured-event-date"><?php echo $event_date;?></span>
					<span class="pub-txt"><?php echo $start_time." - ".$end_time;?></span>
					<p><?php echo $event_desc;?></p>
				</div>
			</div>
		</div>
<?php }
} 
?>

<?php
$current_month_detail = date('m');
$current_year_detail= "20".date('y');
$current_day_detail= "20".date('d');
$current_month_name=date('F');
$prev_month_name=Date('F', strtotime($current_month_name . " last month"));
$next_month_name=Date('F', strtotime($current_month_name . " next month"));
if($current_month_detail=="1") {
	$prev_month="12";
	$prev_year=$current_year_detail-1;
	$next_month=$current_month_detail+1;
	$next_year=$current_year_detail;
} 
elseif($current_month_detail=="12") {
	$prev_month=$current_month_detail-1;
	$prev_year=$current_year_detail;
	$next_month="1";
	$next_year=$current_year_detail+1; 
} else{
	$prev_month=$current_month_detail-1;
	$prev_year=$current_year_detail;
	$next_month=$current_month_detail+1;
	$next_year=$current_year_detail; 
}

$url=site_url()."/calendar";

if(isset($_REQUEST['qemmonth']) && isset($_REQUEST['qemyear'])) { }
$prev_url=$url."/?qemmonth=".$prev_month."&qemyear=".$prev_year."#qem_calreload";
$next_url=$url."/?qemmonth=".$next_month."&qemyear=".$next_year."#qem_calreload";
$current_url=$url."/?qemmonth=".$current_month_detail."&qemyear=".$current_year_detail."#qem_calreload"; ?>

<div class="calendar-contant-right">
	<div class="tabs">
		<a id="prev_month" href="#prev">Previous</a>
		<div class="tabs_wrap">
			<ul class="months_menu month-1">
				<li><a href="#month-1_year-2016">January</a></li>
				<li><a href="#month-2_year-2016">February</a></li>
				<li><a href="#month-3_year-2016">March</a></li>
				<li><a href="#month-4_year-2016">April</a></li>
				<li><a href="#month-5_year-2016">May</a></li>
				<li><a href="#month-6_year-2016">Jun</a></li>
				<li><a href="#month-7_year-2016">July</a></li>
				<li><a href="#month-8_year-2016">August</a></li>
				<li><a href="#month-9_year-2016">September</a></li>
				<li><a href="#month-10_year-2016">October</a></li>
				<li><a href="#month-11_year-2016">November</a></li>
				<li><a href="#month-12_year-2016">December</a></li>
			</ul>
		</div>
		<a id="next_month" href="#next">Next</a>
	</div><!--shop-list-sec-nav ends here-->
	<div class="calendar-main">
		<div class="qem_calreload_wrap">
			<?php echo qem_show_calendar('','',''); ?>
			<a href="#close_popup" class="mobile_close_popup">x</a>
		</div>
		<div class="calendar-main-footer">
			<ul> 
				<li class="white-box"><i class="fa fa-stop"></i>Open Racing</li>
				<li class="green-box"><i class="fa fa-stop"></i>Today’s Date</li>
				<li class="red-box"><i class="fa fa-stop"></i>Event</li>
			</ul> 
		</div>
	</div>
</div>    
                      
                </div>
        </div><!----container end here--->
		<?php

 }
 } ?>
 
<div class=" regular-hours-bg">
	<div  class=" hours-heading-sec">
		<div class="row"> 
			<h2>HOURS</h2>
		</div>
	</div>
	<div class=" row">
		<div class=" regular-hours-in clearfix">
			<?php if ( is_active_sidebar( 'contact_regular_hours_sidebar' ) ) {  dynamic_sidebar( 'contact_regular_hours_sidebar' );  } ?>
		</div>
	</div>
</div><!----regular-hours end here--->
<div class=" holiday-bg">
	<div class=" row">
		<div class=" regular-hours-in clearfix">
			<?php if ( is_active_sidebar( 'contact_holiday_hours_sidebar' ) ) {  dynamic_sidebar( 'contact_holiday_hours_sidebar' );  } ?>
		</div>
	</div>
</div><!----holiday-sec end here--->
<div class=" follow-bg">
<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } 
$instagram_link= get_option( THEME_PREFIX.'instagram_link');
if($instagram_link!="")
{
	?>
	<div class=" row">
		<div class="follow-content">
			<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
		</div>
	</div>
	
	<?php
}
?>
	
</div><!---follow-bg end here---->
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('span.qem-caption').css( 'display', 'none' );
	jQuery('a.calnav').css( 'display', 'none' );
	jQuery('a.event').css( 'display', 'none' );
	
	jQuery('.months_menu a').click(function(e){
		e.preventDefault();
		var res_array = jQuery(this).attr('href').split('#')[1].split('_');
		jQuery('.qem_calreload_wrap').html("<div class='loader_gif'></div>");
		if(res_array != ''){
			var months = { January:1, February:2, March:3, April:4,
							May:5, June:6, July:7, August:8,
							September:9, October:10, November:11, December:12 };
			for (i = 0; i < 13; i++) {
				$('.months_menu').removeClass('month-'+i);
				$('#prev_month').removeClass('month-'+i+'_wrap');
				$('#next_month').removeClass('month-'+i+'_wrap');
			}
			$('.months_menu li').removeClass('current_month');
							
			month = res_array[0].split('-')[1];
			year = res_array[1].split('-')[1];
			
			if(month != 1) {
				$('#prev_month').addClass('month-'+(parseInt(month)-1)+'_wrap');
			} else { $('#prev_month').addClass('month-'+month+'_wrap'); }
			if(month != 12) {
				$('#next_month').addClass('month-'+(parseInt(month)+1)+'_wrap');
			} else {
				$('#next_month').addClass('month-'+month+'_wrap');
			}
			$('.months_menu').addClass('month-'+month);
			$('.months_menu li:nth-of-type('+month+')').addClass('current_month');
			
			var data = {
				'action': 'calendar_callback',
				'month': month,
				'year': year
			};

			jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function(response) {
				jQuery('.qem_calreload_wrap').html(response);
			});
		} else {
			jQuery('.qem_calreload_wrap').html("<div class='text-align: center; padding: 30px 0;'><p>There was an error, please refresh the page and try again. Thank you!</p></div>");
		}
	});
});
</script>
<?php
get_footer();
?>