<?php
/**
 * Template Name: blog
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner parallax-banner" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center center / cover ;"<?php } ?>>
           <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_field('banner_heading_1', $post->ID);
								$pagetitle2=get_field('banner_heading_2', $post->ID);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
          </div>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
		


     </div>  <!--about-us-banner ends here-->
	
    
          
    
    <div id="blogpage"> 
  <div class=" row clearfix">
     <div class="content">
	 
       <div class="blog-page-main">
         
			<div class=" blog-listing">
			
			<?php the_content();?>
			
			<div class="list-social">
				<a class="facebook customer share" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
				<a class="twitter customer share" href="http://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title();?>&hashtags=octaneraceway" target="_blank"><i class="fa fa-twitter"></i></a>
				<a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
			</div>
			<div class="clearfix idea-box-main">
				<div class="idea-box">
					<a href="<?php echo site_url();?>/blog">Back To Blog Page</a>
					<?php previous_post_link('%link', 'Previous'); ?>
					<?php next_post_link( '%link', 'Next' ); ?>
				</div>
			</div>
			
			
			
			
			
			</div>
			
			<?php 
				if ( comments_open() ) :
					comments_template(); 
				endif;
			?>


       </div>
     </div>
	<div id="sidebar">
	<h2>CUSTOMER REVIEWS</h2>
		<div class="sidebar-box tripadvisor">
			<div id="TA_selfserveprop392" class="TA_selfserveprop">
				<ul id="VpEj6jU3F" class="TA_links 1RqK371zDQeG">
					<li id="sGfazHHiZx" class="sJ5NI3b6fSdE">
						<a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
					</li>
				</ul>
			</div>
			<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=392&amp;locationId=2243652&amp;lang=en_US&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
		</div>
		
		<!--<div class="sidebar-box customer-list">
			<div id='yelpwidget'></div>
		</div>-->
	</div>
  </div>
</div>
<script>
;(function($){
  
  /**
   * jQuery function to prevent default anchor event and take the href * and the title to make a share pupup
   *
   * @param  {[object]} e           [Mouse event]
   * @param  {[integer]} intWidth   [Popup width defalut 500]
   * @param  {[integer]} intHeight  [Popup height defalut 400]
   * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
   */
  $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
    
    // Prevent default anchor event
    e.preventDefault();
    
    // Set values for window
    intWidth = intWidth || '500';
    intHeight = intHeight || '400';
    strResize = (blnResize ? 'yes' : 'no');

    // Set title and open popup with focus on it
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
  }
  
  /* ================================================== */
  
  $(document).ready(function ($) {
    $('.customer.share').on("click", function(e) {
      $(this).customerPopup(e);
    });
	$('label[for="author"] small').text('Name*');
	$('label[for="email"] small').text('Mail (will not be published)*');
  });
    
}(jQuery));

</script>
	<?php 
	}
}

get_footer();
?>