<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }
 ?>

<div id="birthday-section" class="event-section <?php echo $post->post_name;?>" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center top / cover ; padding: 40px 0;"<?php } ?>>
           <div class="row">
              <div class="txtcenter">
                <h1><?php the_title();?></h1>
                <?php 
				       //remove_filter( 'the_content', 'wpautop' );
					   the_content();
					   //add_filter( 'the_content', 'wpautop' );
				   ?>
              </div>
           </div>
        </div><!--birthday-section ends here-->