<?php
/**
 * Template Name: velocity-vr
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
                  <div class="book-race-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
                  	 <div class="row" style="position: relative; z-index: 2;"> 
                      <div class="innerpage-banner-in">
                      <div class="banner-top-content">
						 <div class="kartracing-big-circle velocity-big-circle">
						 <div class=" speed-icon"> <img src="<?php echo get_template_directory_uri();?>/images/velocity-icon.png" alt="img"></div>
       <img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
       </div>
                         <div class="kart-race-head">
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							 <h1>
							 <?php 
								 $pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								 $pagetitle2=get_post_meta($post->ID,'pagetitle2', true);
								 
								 if($pagetitle1!="" && $pagetitle2!="")
									 {
										 echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
									 }
								 else
									 {
										 the_title();
									 } ?>
							 
							 </h1>
							 <img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						 </div>
                      </div>
                      </div>
                   </div>
				   <div class="black-line-banner" style="display: none;"></div>
                  </div>
                  <?php //include 'sb.php'; 
                  ?>
                <div id="main-menu-sec" class="menu-innerpage">
                <div class=" row clearfix">
                <?php
					$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
					if(!empty($inner_logo_img))
					{
						?>
						<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
						<?php	
					}
					?>
                <div id="main-menu">
                <?php
					$defaults = array( 'menu' => 'Middle Menu' );
					wp_nav_menu($defaults);
					?>
                </div>
                <?php
				$box_title_1= get_option( THEME_PREFIX.'box_title_1');
				$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
					{
					?>
					<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
					<?php
					} ?>
                </div>
		<?php include 'mm.php'; ?>
</div><!----main-menu-sec end here--->
                </div>

            <div class="container kart-race-contant">
              <div class="row clearfix">
                 <div class="content">
                   <div class="kart-race-contant-left">
                     <?php the_content();?>
                   </div>
                 </div> 
                 <?php get_sidebar('velocity-vr');?>
              </div>
            </div>
<?php } 
} 
			$top_heading=get_post_meta($post->ID,'top_heading', true);					 $middle_heading=get_post_meta($post->ID,'middle_heading', true);
			$background_image=get_post_meta($post->ID,'background_image', true);
			$link_title=get_post_meta($post->ID,'link_title', true);
			$link_url=get_post_meta($post->ID,'link_url', true);
			$instragram_heading=get_post_meta($post->ID,'instragram_heading', true);
			$follow_heading=get_post_meta($post->ID,'follow_heading', true);
			//$top_heading= !empty($top_heading) ? $top_heading : "REALITY REDEFINED!";
			$top_heading= "REALITY REDEFINED!";
			$middle_heading= !empty($middle_heading) ? $middle_heading : "BOOK A SESSION";
			$link_title= !empty($timings) ? $timings : "CLICK HERE";
			$instragram_heading= !empty($instragram_heading) ? $instragram_heading : "SEE HOW FUN VR CAN BE!";
			$follow_heading= !empty($follow_heading) ? $follow_heading : "MORE AWESOME THINGS TO DO AT OCTANE";
			
			$BAR_background = get_field('bookarace_background_image', 151);
			$BAR_background_parallax = get_field('bookarace_background_parallax', 151);
			$BAR_title = "BOOK A SESSION";//get_field('bookarace_title', 151);
			$BAR_tag_line = "Experience a new reality!";//get_field('bookarace_tag_line', 151);
			$BAR_button = get_field('bookarace_button_title', 151);
			$BAR_buttonURL = get_field('bookarace_button_url', 151);
			
			
			$bsimage_url=get_post_meta($post->ID,'bsimage_url', true);
		    $bsbtn_url=get_post_meta($post->ID,'bsbtn_url', true);
			
			$_background = '';
			if($bsimage_url == ""){
				if(!empty($BAR_background)) {
					if($BAR_background_parallax == 'parallaxon') {
						$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$BAR_background.'"';
					}else{
						$_background = 'style="background: url('.$BAR_background.') center top no-repeat; background-size: cover;"';
					}
				}else{
					$_background = 'style="background: #FFF;"';
				}
			}
			else{
				$_background = 'style="background: url('.$bsimage_url.') center top no-repeat; background-size: cover;"';
			}
			
			
			
			/*$_background = '';
			if(!empty($BAR_background)) {
				if($BAR_background_parallax == 'parallaxon') {
					$_background = 'data-parallax="scroll" data-position="top" data-image-src="'.$BAR_background.'"';
				}else{
					$_background = 'style="background: url('.$BAR_background.') center top no-repeat; background-size: cover;"';
				}
			}else{
				$_background = 'style="background: #FFF;"';
			}*/
			?>
            <div class="happy-hour-sec kart-racebook-sec <?php echo (($BAR_background_parallax == 'parallaxon' && !empty($BAR_background)) ? 'parallax-window' : ''); ?>" <?php echo $_background; ?>>
			
                <div class="row">
					<img class="spinit bookaraceBig-tachometer" src="<?php bloginfo('template_url'); ?>/images/big-tachometer.png" alt="" />
					<img class="spinit bookaraceSmall-tachometer" src="<?php bloginfo('template_url'); ?>/images/small-tachometer.png" alt="" />
                   <div class="happy-hour-sec-in kart-racebook-sec-in">
                     <span class="join-text"><?php echo $BAR_tag_line;?></span>
                     <h1><?php echo $BAR_title;?></h1>
                     <a href="<?php if($bsbtn_url == "") echo "https://www.velocityvr.com/book-now/"; else echo $bsbtn_url;?>" class="trans-bg day-text"><?php echo $BAR_button;?></a>
                   </div>
                </div>
            </div> <!--happy-hour-sec ends here-->
            <div class="fun-kart-race-sec">
              <div class="row">
                  <div class="fun-kart-race-sec-in txtcenter">
                    <h2><?php echo $instragram_heading;?></h2>
                    <a href="javascript:void(0)" class="scroll_by_class" data-value="folow-us-lisitn-sec"><img src="<?php echo get_template_directory_uri(); ?>/images/down-arrow-with-box.png" alt="img"></a>
                  </div>
              </div> 
            </div>
            <div class="people-image" id="people">
		<div class=" follow-bg">
<?php if ( is_active_sidebar( 'instragram_sidebar' ) ) {  dynamic_sidebar( 'instragram_sidebar' );  } else {
$instagram_link= get_option( THEME_PREFIX.'instagram_link');
if($instagram_link!="")
{
	?>
	<div class=" row">
		<div class="follow-content">
			<a href="<?php echo $instagram_link;?>" class="trans-bg" target="_blank"><i class="fa fa-instagram"></i>FOLLOW</a>
		</div>
	</div>
	
	<?php }
}
?>
	
</div><!---follow-bg end here---->
		</div>
			<div class="folow-us-lisitn-sec kart-racing-sec">
                <div class="row">
                 <h2 class="txtcenter"><?php echo $follow_heading;?></h2>
                 <span class="gray-arrow txtcenter"><i class="fa fa-chevron-down"></i></span>
                <div class=" follow-listing">
                <ul>
                <?php
					$menu_items = wp_get_nav_menu_items('Footer Logo Menu');
					//r_print_r($menu_items);exit;
					$inc="1";
					foreach ( (array) $menu_items as $key => $menu_item ) 
					{
						$title = $menu_item->title;
						$url = $menu_item->url;
						
						?>
						<li><a href="<?php echo $url;?>"><span class="follow-icon<?php echo $inc;?>"></span><small><?php echo $title;?></small></a></li>
						<?php
						$inc++;
					}  ?>
                </ul>
                </div>
                </div>
			</div><!---follow-listing end here---->
<?php
$pdf_file = get_field('popupwaiver_pdf', 151);
if(!empty($pdf_file)) :
?>
<div id="popup-terms" class="popupbox-main mfp-hide white-popup-block">
	<div class="popup-content">
		<div class="popupbox-inner popupbox-inner-scroll">
			<embed src="<?php echo $pdf_file; ?>" type="application/pdf" width="100%" height="450"></embed>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
get_footer();
?>