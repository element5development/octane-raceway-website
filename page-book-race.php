<?php
/**
 * Template Name: book-race
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
                  <div class="book-race-banner parallax-banner fixed" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center center / cover ;"<?php } ?>>
                  	 <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
					<div class="book-big-circle">
                  <img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="img" class="spinit">
                </div>
                 <div class="trans-bg">
				 <img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
						<div>
							<h1 style="line-height: 72px;">
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span>".$pagetitle1."</span><br /> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
						</div><img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
					</div>
				</div>
			</div>
                  </div>
                  <?php //include 'sb.php'; 
                  ?>
                <div id="main-menu-sec" class="menu-innerpage">
					<div class=" row clearfix">
								<?php
								$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
								if(!empty($inner_logo_img))
								{
									?>
									<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
									<?php	
								}
								?>
					<div id="main-menu">
					<?php
								$defaults = array( 'menu' => 'Middle Menu' );
								wp_nav_menu($defaults);
								?>
					</div>
					<?php
								$box_title_1= get_option( THEME_PREFIX.'box_title_1');
								$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
									if($box_title_1!="" && $box_title_1_link!="")
									{
									?>
									<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
									<?php
									} ?>
					</div>
		<?php include 'mm.php'; ?>
</div><!----main-menu-sec end here--->
                </div><!----banner end here---->

            <div class="container race-sec">
            		<div class="row">
                    <div class="race-sec-in">
                         <?php the_content();?>
                         
                    </div>
                    
                    </div>
            </div><!--race-sec ends here-->
			<?php 
	}
}
	
		   $termcondition_text1=get_post_meta($post->ID,'termcondition_text1', true);
		   $termcondition_text2=get_post_meta($post->ID,'termcondition_text2', true);
		   $reservation_text=get_post_meta($post->ID,'reservation_text', true);
		   $termcondition_text1 = !empty($termcondition_text1) ? $termcondition_text1 : 'I HAVE READ THE TERMS & CONDITIONS';
		   $termcondition_text2 = !empty($termcondition_text2) ? $termcondition_text2 : '';
		   $reservation_text = !empty($reservation_text) ? $reservation_text : 'CONTINUE TO RESERVATION FORM';
	?>
            <div class="race-sec-main-list">
              <div class="row">
                  <ul class="race-sec-main-list-inner">
                    <li>
                      <div class="clearfix">
                          <div class="race-main-list-left"><span>1</span></div>
                          <div class="race-main-list-right">
                            <h4><?php echo $termcondition_text1;?></h4>
                            <p><?php echo $termcondition_text2;?></p>
							<div class="clearfix">
							<form>
                             <div class="complete-check">
                               <input name="term_cond" type="radio" value="yes" id="completed" onchange="check('yes')"><label for="completed"> I Agree </label>
                             </div>
                            <div class="not-complete-check">
                              <input name="term_cond" type="radio" value="no" id="NotCompleted" onchange="check('no')" checked="checked"><label for="NotCompleted"> I Do Not Agree</label>
                            </div>
							</form>
                            </div>
                          </div><!--race-main-list-right ends here-->
                      </div>
                    </li>
                    <li class="second">
                      <div class="clearfix">
                          <div class="race-main-list-left"><span>2</span></div>
                          <div class="race-main-list-right">
                            <h4><?php echo $reservation_text;?></h4>
                            <a href="javascript:void(0)" class="button-grey" id="goToBookRace">CONTINUE</a>
						 </div><!--race-main-list-right ends here-->
                      </div>
                    </li>
                  </ul>
              </div>
            </div><!--race-sec-main-list ends here-->
			<script type="text/javascript">
			jQuery(document).ready(function($){
				
				jQuery('#completed').prop( "checked", false );
				jQuery('#NotCompleted').prop( "checked", true );
			});
			function check(check_cond)
			{
				//alert(check_cond);
				if(check_cond=="yes")
				{
					jQuery('a.button-grey').addClass('grabeout');
					jQuery('a.button-grey').attr('href','https://octanephx.clubspeedtiming.com/booking/step1');
				}
				else{
					jQuery('a.button-grey').removeClass('grabeout');
					jQuery('a.button-grey').attr('href','javascript:void(0)');
				}
			}
			</script>
<?php
get_footer();
?>