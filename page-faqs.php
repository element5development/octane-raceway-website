<?php
/**
 * Template Name: faqs
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner <?php echo (!empty($thumb_url)? 'parallax-window' : ''); ?>" <?php if(!empty($thumb_url)){ ?>data-parallax="scroll" data-image-src="<?php echo $thumb_url;?>"<?php } ?>>
           <div class="row" style="position: relative; z-index: 2;"> 
				<div class="innerpage-banner-in txtcenter">
					<div class="banner-top-content">
						<div>
							<img class="line-from-left" src="<?php bloginfo('template_url'); ?>/images/line-from-left.png" alt="" />
							<h1>
								<div class="about-big-circle">
									<img src="<?php echo get_template_directory_uri();?>/images/aboutbanner-big-circle.png" alt="mg" class="spinit">
								</div> 
								<?php 
								$pagetitle1=get_post_meta($post->ID,'pagetitle1', true);
								$pagetitle2=get_post_meta($post->ID,'pagetitle2', true);

								if($pagetitle1!="" || $pagetitle2!="") {
									echo "<span class='trans-bg'>".$pagetitle1."</span> ".$pagetitle2;
								} else {
									the_title();
								} ?>
							</h1>
							<img class="line-from-right" src="<?php bloginfo('template_url'); ?>/images/line-from-right.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="black-line-banner" style="display: none;"></div>
          </div>
          <?php //include 'sb.php'; 
          ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url(); ?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
     </div>  <!--about-us-banner ends here-->
	
    
          <?php //the_content();?>
        
	<?php 
	}
}
	
	?>
    
    <div class="about-us-list-sec">
      <div class="row">
        <div class="about-us-list-sec-in faq-content">
          <!--<h2 class="txtcenter">FAQS</h2>-->
           <ul class="about-content-list">
		   <?php $args = array( 'post_type'  => 'faq', 'showposts' => -1, 'post_status' => 'publish', 'orderby'          => 'menu_order', 'order' => 'ASC' );
                 $posts_array = get_posts( $args );
				 foreach($posts_array as $post) : setup_postdata( $post ); ?>
             <li class="<?php echo $post->post_name; ?>">
              <div class="clearfix">
			  <?php
			  /*
			  if ( has_post_thumbnail() )
								{
				$thumb_img=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$thumb_img_url=$thumb_img['0'];?>
				<div class="about-content-list-left">
                 <span class="about-list-img" style="background-image:url(<?php echo $thumb_img_url;?>);"></span>
               </div><!--about-content-list-left ends here-->
						<?php
				} */ ?>
               
               <div class="about-content-list-right">
                 <div>
                   <div class="about-us-list-head">
                      <h3><?php the_title();?></h3>
                   </div>
                   <div class="clear"></div> 
                 </div>
                  <p><?php the_content();?></p>
                 
               </div><!--about-content-list-right ends here-->
               </div>
             </li>
             <?php endforeach; 
             wp_reset_postdata();?>
          </ul>
        </div>
		<div class="faq-sidebar">
			<div class="pinned">
				<h2>FREQUENTLY ASKED QUESTIONS</h2>
				<div class="faqs_sidebar">
					<?php foreach($posts_array as $post) : setup_postdata( $post ); ?>
						<a href="javascript:void(0)" class="scroll_by_class " data-value="<?php echo $post->post_name; ?>"><?php the_title();?></a>
					<?php endforeach; 
					wp_reset_postdata();?>
				</div>
				<a href="<?php echo site_url();?>/book-race/" class="book-arace">BOOK A RACE</a>
			</div>
		</div>
      </div>
	  <div class="grey_background"></div>
    </div>
<?php
get_footer();
?>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.pin.js"></script>
<script>
	jQuery(".pinned").pin({containerSelector: ".about-us-list-sec", minWidth: 940});
	jQuery('a.scroll_by_class').click(function(){ 
		jQuery('.pinned a').removeClass('active_sidemenu');
		jQuery(this).addClass('active_sidemenu');
		var a = jQuery(this).attr('data-value');
		jQuery('html , body').animate({scrollTop:jQuery('.'+a).offset().top - 99 },400);	
	});
	jQuery('.cd-top').click(function(){
		jQuery('html , body').animate({scrollTop: 0 },400);
	});
	jQuery( "li.menu-item-36, .main-submenu" ).hover(function() {
		
		jQuery( '.main-submenu' ).show( );
		jQuery( 'li.menu-item-36' ).addClass('hovered_submenu');
	}, function(){
		jQuery( '.main-submenu' ).hide( );
		jQuery( 'li.menu-item-36' ).removeClass('hovered_submenu');
	});
</script>