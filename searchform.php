<?php
/**
 * Template for displaying search forms in Octane
 *
 * @package WordPress

 */
?>
<div class=" top-bar-right-search">
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text" style="font-size:12px;font-weight:700;"><?php echo _x( 'Search for:', 'label', 'octane' ); ?></span>
		<input type="search" class="search" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'octane' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'octane' ); ?>" />
	</label>
	<button type="submit" class=" search-btn"><i class="fa fa-search"></i></button>
</form>
</div>