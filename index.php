<?php get_header();
$bg_image= get_option( THEME_PREFIX.'home_bg_image');
?>
<div class="home-banner" <?php if($bg_image!=""){ ?>style="background:url('<?php echo $bg_image;?>') repeat scroll center top / cover ;"<?php } ?>>
<div class=" row">
<div class=" banner-top clearfix">
<div class=" logo">
<?php 
   $logo_img= get_option( THEME_PREFIX.'home_page_logo');
   $logo_img = !empty($logo_img)? $logo_img : get_template_directory_uri().'/images/logo.png';
   ?>

<a href="#"><img src="<?php echo $logo_img;?>" alt="img"></a></div>
<div class="banner-top-right">
<span><a href="#"><i class="fa fa-facebook"></i></a></span>
<span><a href="#"><i class="fa fa-twitter"></i></a></span>
<span><a href="#"><i class="fa fa-instagram"></i></a></span>
<span><a href="#"><i class="fa fa-youtube-play"> </i></a></span>
<span><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/gplus-red-iocn.png" alt="img"></a></span>
</div>
</div>
<div class=" banner-top-content">
<div class="banner-top-content-text">
<?php
$heading_upper= get_option( THEME_PREFIX.'home_heading_section_1');
$heading_lower= get_option( THEME_PREFIX.'home_heading_section_2');
if(!empty($heading_upper))
{
?>
<span class=" trans-bg"><h1><?php echo $heading_upper;?></h1></span>
<?php	
}
if(!empty($heading_lower))
{
?>
<h1><?php echo $heading_lower;?></h1>
<?php	
}
?>	
<div class=" banner-top-content-btn">
<?php
$box_title_1= get_option( THEME_PREFIX.'box_title_1');
$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
$box_title_2= get_option( THEME_PREFIX.'box_title_2');
$box_title_2_link= get_option( THEME_PREFIX.'box_title_2_link');
    if($box_title_1!="" && $box_title_1_link!="")
    {
	?>
	<a href="<?php echo $box_title_1_link;?>"><?php echo $box_title_1;?></a>
	<?php
	}
	if($box_title_2!="" && $box_title_2_link!="")
    {
	?>
	<a href="<?php echo $box_title_2_link;?>" class=" takeyour-btn"><?php echo $box_title_2;?></a>
	<?php
	}
    ?>
</div>
<span class="down-arrow"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-arrow.png" alt="img"></a></span>
</div>
<div class="homebanner-big-circle"><img src="<?php echo get_template_directory_uri(); ?>/images/homebanner-big-circle.png" alt="circle"></div> 
<div class="homebanner-small-circle"><img src="<?php echo get_template_directory_uri(); ?>/images/homebanner-small-circle.png" alt="circle"></div>  
</div>
</div>
</div><!----banner end here--->
<div id="main-menu-sec">
<div class=" row clearfix">
<?php
$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
if(!empty($inner_logo_img))
{
	?>
	<div class=" logo-small"><a href="#"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
	<?php	
}
?>

<div id="main-menu">
<?php
$defaults = array( 'menu' => 'Middle Menu' );
wp_nav_menu($defaults);
?>

</div>
<?php
    if($box_title_1!="" && $box_title_1_link!="")
    {
	?>
<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
	<?php
	} ?>

</div>
</div><!----main-menu-sec end here--->
<div class=" racing-sec">
<div class=" row clearfix">	
<div class=" racing-header">
     <div class="racing-arrow">
     .o1 <br /> <i class="fa fa-chevron-down"></i>
     </div>
     <div class="racing-heading">
       <span class="paging-icon-mobile"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon.png" alt="g"></span>
       <span class="high-speed-text"><span>HIGH-SPEED</span></span>
       <span class="paging-icon">
          <img src="<?php echo get_template_directory_uri(); ?>/images/pagin-circle1.png" alt="g">
          <img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon1.png" alt="icon1" class="paging-icon-inner">
       </span>
       <span class="high-speed-text">KART RACING</span>     
     </div>
</div>
<div class="racing-col-common clearfix">
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/racing-gallery-img.png" alt="img"></span>
<p>If you're looking for a great kart track in America, Octane Raceway in Scottsdale gives you the Ultimate Adrenaline Rush! Our 1/3 mile track is the only full time Indoor/Outdoor track in the entire United States! </p>
<a href="#" class="button-default">LEARN MORE</a>
</div>
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/racing-gallery-img.png" alt="img"></span>
<p>Specifically designed for the venue, the track layout is unique in that each lap begins indoors then winds through an outdoor section of the course before returning inside. Click button below to view our directory!</p>
<a href="#" class="button-default">VIEW DIRECTORY</a>
</div>
</div>
</div>
</div><!----racing end here---->
<div class=" corporate-events-sec">
<div class=" row clearfix">	
<div class=" racing-header">
     <div class="racing-arrow white-text">
     .o2 <br /> <i class="fa fa-chevron-down"></i>
     </div>
     <div class="racing-heading">
        <span class="paging-icon-mobile"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon2.png" alt="g"></span>
        <span class="high-speed-text color-light">CORPORATE EVENTS </span>
        <span class="paging-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon2.png" alt="g"></span>
        <span class="high-speed-text"><span>PRIVATE PARTIES</span></span>     
     </div>
</div>
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img.png" alt="img"></span>
<p class=" white-text">Octane Raceway expertly assists groups of ten to 500 with corporate team building exercises, corporate sales outings, reward adventures and VIP customer experiences. </p>
<a href="#" class="button-default button-white">LEARN MORE</a>
</div>
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img2.png" alt="img"></span>
<p class=" white-text">Looking for fun party ideas? For the ultimate Private Party, speed over to Octane Raceway & give your guests one of the most memorable, out-of-the box experiences of the year!</p>
<a href="#" class="button-default button-white">VIEW DIRECTORY</a>
</div>
</div>
</div><!----corporate-events-sec end here---->
<div class=" track-side-sec">
<div class=" row clearfix">	
<div class=" racing-header">
     <div class="racing-arrow">
        .o3 <br /> <i class="fa fa-chevron-down"></i>
     </div>
     <div class="racing-heading">
     <span class="paging-icon-mobile"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon3.png" alt="g"></span>
     <span class="high-speed-text"><span>TRACKSIDE</span></span>
     <span class="paging-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon3.png" alt="g"></span>
     <span class="high-speed-text">BAR & GRILL</span>     
     </div>
</div>
<div class="racing-col-common clearfix">
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img.png" alt="img"></span>
<p>Octane Raceway expertly assists groups of ten to 500 with corporate team building exercises, corporate sales outings, reward adventures and VIP customer experiences. </p>
<a href="#" class="button-default">LEARN MORE</a>
</div>
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img2.png" alt="img"></span>
<p>Looking for fun party ideas? For the ultimate Private Party, speed over to Octane Raceway & give your guests one of the most memorable, out-of-the box experiences of the year!</p>
<a href="#" class="button-default">VIEW DIRECTORY</a>
</div>
</div>
</div>
</div><!---track-side-sec end here---->
<div class=" bowling-sec">
<div class=" row clearfix">	
<div class=" racing-header">
     <div class="racing-arrow white-text">
        .o4 <br /> <i class="fa fa-chevron-down"></i>
     </div>
     <div class="racing-heading">
     <span class="paging-icon-mobile"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon4.png" alt="g"></span>
     <span class="high-speed-text color-light">MINI BOWLING</span>
     <span class="paging-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/pagin-icon4.png" alt="g"></span>
     <span class="high-speed-text"><span>ARCADE GAMES</span></span>     
     </div>
</div>
<div class="racing-col-common clearfix">
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img.png" alt="img"></span>
<p class=" white-text">Octane Raceway expertly assists groups of ten to 500 with corporate team building exercises, corporate sales outings, reward adventures and VIP customer experiences. </p>
<a href="#" class="button-default button-white">LEARN MORE</a>
</div>
<div class="racing-col-2">
<span> <img src="<?php echo get_template_directory_uri(); ?>/images/corporatve-gallery-img2.png" alt="img"></span>
<p class=" white-text">Looking for fun party ideas? For the ultimate Private Party, speed over to Octane Raceway & give your guests one of the most memorable, out-of-the box experiences of the year!</p>
<a href="#" class="button-default button-white">VIEW DIRECTORY</a>
</div>
</div>
<div class="bowling-check-text">
  <a href="#">CHECK OUT ALL OF OUR AWESOME THINGS TO DO BELOW2! <br /> <i class="fa fa-chevron-down"></i></a></div>
</div>
</div><!---bowling sec end here---->
<div class="folow-us-lisitn-sec">
<div class=" row">
<div class=" follow-listing">
<ul>
<li><a href="#"><span class="follow-icon1"></span><small>KART RACING</small></a></li>
<li><a href="#"><span class="follow-icon2"></span><small>MINI BOWLING</small></a></li>
<li><a href="#"><span class="follow-icon3"></span><small>ARCADE GAMES</small></a></li>
<li><a href="#"><span class="follow-icon4"></span><small>BILLIARDS</small></a></li>
<li><a href="#"><span class="follow-icon5"></span><small>RC KART<br>CHALLENEGE</small></a></li>
<li><a href="#"><span class="follow-icon6"></span><small>PIT CREW CHALLENGE</small></a></li>
</ul>
</div>
</div>
</div><!---follow-listing end here---->
<div class=" follow-bg">
<div class=" row">
<div class="follow-content">
<a href="#" class="trans-bg"><i class="fa fa-instagram"></i>FOLLOW</a>
</div>
</div>
</div><!---follow-bg end here---->
<?php get_footer();?>