<?php
//echo $play_page_id;
		$exclusive_event_heading=get_post_meta($play_page_id,'exclusive_event_heading', true);
		$exclusive_event_background=get_post_meta($play_page_id,'exclusive_event_background', true);
		$exclusive_event_heading= empty($exclusive_event_heading) ? $exclusive_event_heading : "EXCLUSIVE GROUP EVENT GAMES";
		?>
<div class="ex-group-event" <?php if($exclusive_event_background!=""){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $exclusive_event_background;?>) no-repeat scroll center top / cover ; padding: 0 0 60px;"<?php } ?>>
      <div  class="heading-arrow-sec">
        <div class="row">
		
           <h2><?php echo $exclusive_event_heading;?></h2>
        </div>
      </div>
      <div class="row">                    
         <div class=" clearfix common2">
          <?php if ( is_active_sidebar( 'play_group_events_sidebar' ) ) {  dynamic_sidebar( 'play_group_events_sidebar' );  } ?>
        </div>
      </div>
    </Div><!--ex-group-event ends here-->