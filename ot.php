<div class="testimonials-slider">
<?php 
$args = array (
	'post_type'              => array( 'octane_testimonials' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'            => '10',
	'order'                  => 'DESC',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post(); 
?>
		
<div class="gs_testimonial_container gs_style5">
<div class="cycle-slideshow composite-example">
<div class="gs_testimonial_single cycle-slide cycle-sentinel">
<div class="testimonial-box">
<div class="box-image">
<img class="" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="img">
</div>
<div class="box-content"><p><?php the_content(); ?></div>
<?php if(get_field('octane_t_name'))
{
	echo '<h3 class="box-title">' . the_field('octane_t_name') . '</h3>';
}
?>
<div class="box-companyinfo">
 
<?php if(get_field('octane_t_company'))

{   
    ?> <span class="box-label">Company : </span> <?php
	echo '<span class="box-com-name"> ' . the_field('octane_t_company') . '</span>';
}
?>
</div>
<div class="box-desiginfo">
	<?php if(get_field('octane_t_designation'))
	
{   
    ?><span class="box-label">Designation : </span><?php 
	echo '<span class="box-design-name"> ' . the_field('octane_t_designation') . '</span>';
}
?>
</div>
</div>
</div>
</div>
</div>

<?php	
}
} 
wp_reset_postdata(); ?>
</div>