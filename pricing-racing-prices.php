<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */
?>

<div class="junior-racing-sec txtcenter">
          <div class="row">
            <h2><?php the_title();?></h2> 
            <div class="clearfix common3">
              <?php 
				   remove_filter( 'the_content', 'wpautop' );
				   the_content();
				   add_filter( 'the_content', 'wpautop' );
				   ?>
            </div>
          </div>
        </div>