<?php
/**
 * subscriber Dyanmic Form
 *
 * @since Standard Theme 1.0
 */
 
 
function subscriber_fun($content=array())
{ 
	do_action('before_subscriber_form', $content);
	?>
	
	<?php
	if(!empty($content))
	{
		$sf_h_content = !empty($content['subscriber_form_heading_content']) ? $content['subscriber_form_heading_content'] :'';
		$sf_h_class = !empty($content['subscriber_form_heading_class']) ? $content['subscriber_form_heading_class']: '';
		$sf_h_msg = !empty($content['thank_msg']) ? $content['thank_msg']: '';
		
		
            
            
            
		if(!empty($sf_h_content))
		{
		?>
		<div class="stay-informed">
			<?php echo $sf_h_content;
			
		if(!empty($sf_h_msg))
		{
				?>
                <p id="success_subscribe" style="display:none;"><?php  echo $sf_h_msg; ?></p>
		<?php
		} ?>
        </div>
        <?php
		}
		?>
		<div class=" stay-informed-from">
		<form action="<?php echo admin_url('admin-ajax.php'); ?>"  class="subscriber-form-area <?php echo !empty($content['subscriber_form_class']) ? $content['subscriber_form_class']: ''; ?>" method="post">
		<div class=" clearfix common">
			<input type="hidden" name="action" value="subscribe_user" />
            <?php wp_nonce_field( 'subscribe_user_action', 'subscribe_user_field' ); ?>
            <?php
			   $form_fiels = $content['form_fields'];
			   if(!empty($form_fiels))
			   {
				 foreach($form_fiels as $field_key=>$field_value)
				 {
				 	if($field_value['field_is_show'] == 'yes')
					{
						do_action('before_subscriber_form_fields', $content);
				 	?>
                      
                          <div class="<?php echo !empty($content['form_field_wrapper_class']) ? $content['form_field_wrapper_class']: ''; ?> one-third">
                              <?php
                                if($field_value['field_type'] == 'text')
                                {
                                ?>          
                                    <input maxlength="<?php echo $field_value['field_id'];  ?>" name="<?php echo $field_value['field_id'];  ?>" data-type="<?php echo $field_value['field_data_type']; ?>" type="text"  class="field-white ipt-field <?php echo ( !empty($field_value['field_is_required']) && $field_value['field_is_required'] == 'yes' ) ?  'field_required' : '';    echo $content['subscriber_form_field_class'].' ';  echo !empty($field_value['field_css'])  ?  ' '.$field_value['field_css'] : '';      ?>" maxlength="<?php echo $field_value['field_size']; ?>" placeholder="<?php echo  !empty($field_value['field_title']) ? $field_value['field_title'] : ''; ?>"/> 
                                    <?php
                                    if(!empty($field_value['field-guide']))
                                    {?>
                                    <small class="field-guide"><?php echo $field_value['field-guide']; ?></small>
                                    <?php
                                    }
                                 }
								 else
								 {
								 	
                                ?>         
                                    <textarea  maxlength="<?php echo $field_value['field_id'];  ?>" name="<?php echo $field_value['field_id'];  ?>" data-type="<?php echo $field_value['field_data_type']; ?>" class="ipt-field <?php echo ( !empty($field_value['field_is_required']) && $field_value['field_is_required'] == 'yes' ) ?  ' field_required ' : '';    echo ' '.$content['subscriber_form_field_class'].' ';  echo !empty($field_value['field_css'])  ?  ' '.$field_value['field_css'] : '';      ?>" maxlength="<?php echo $field_value['field_size']; ?>" placeholder="<?php echo  !empty($field_value['field_title']) ? $field_value['field_title'] : ''; ?>"></textarea>  
                                    <?php
                                    if(!empty($field_value['field-guide']))
                                    {?>
                                    <small class="field-guide"><?php echo $field_value['field-guide']; ?></small>
                                    <?php
                                    }
                                 
								 }
                                 ?>
                          </div>
					<?php
						do_action('after_subscriber_form_fields', $content);
					}
									
				 }
			   
			   }
			   
			   do_action('before_subscriber_form_submit_button', $content);
			?>
                
                <div class="subscriber_btn one-third">
                    <button type="button" class="red-button subscriberbtn <?php echo $content['submit_button_class']; ?>"><?php echo $content['submit_button_text']; ?>
                    </button>
                </div>
            <?php
			   do_action('after_subscriber_form_submit_button', $content);
			?>
            </div>
		</form>
		</div>
		<?php
	}	
	
	
 	do_action('after_subscriber_form', $content);
}



//Save Subscriber 
add_action( 'wp_ajax_subscribe_user', 'subscribe_user_action_handler' );
add_action( 'wp_ajax_nopriv_subscribe_user', 'subscribe_user_action_handler' );
function subscribe_user_action_handler(){ 
	
	global $wpdb, $current_user; 

	//GDP ADD REMOVE FAVORITE
	
	if ( 
	! isset( $_POST['subscribe_user_field'] ) 
	|| ! wp_verify_nonce( $_POST['subscribe_user_field'], 'subscribe_user_action' ) 
	) {
	return ;
	}
  
	$email = !empty($_REQUEST['email']) ? esc_attr($_REQUEST['email']) : ''; 
	$name = !empty($_REQUEST['name']) ? esc_attr($_REQUEST['name']) : ''; 
	
	
		$exists_user = '';
		if(!empty($email))
		{
			
			// Save Count Increment one by one	
			$exists_user = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".$wpdb->prefix."subscribe_users WHERE 	email = %s ", array($email)));

			if(!empty($exists_user))
			{
				
				$result=$wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix."subscribe_users SET  name= %s WHERE sub_id = %d", array($name, $exists_user->sub_id)));
				
			 }
			 else
			 {
			 	
				$result=$wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."subscribe_users SET email = %s,  name = %s", array($email, $name)));
						

			 }

		}
		if (!$result)
				{
					die('Invalid query: ' . mysql_error());
				}
				else{
					echo "1";
				}
		
		
	 
		exit;

	}
 