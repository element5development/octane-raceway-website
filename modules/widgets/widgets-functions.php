<?php
// =============================== Home Page Bottom Sidebar3-in-1 Widget ======================================
class eat_drink_food_menu_widget extends WP_Widget {
	function eat_drink_food_menu_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Eat&Drink Food Menu Widget' );
		$this->WP_Widget('eat_drink_food_menu_widget', 'CR &rarr; Eat&Drink Food Menu Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$menu_title = empty($instance['menu_title']) ? '' : apply_filters('widget_menu_title', $instance['menu_title']);
		$menu_description = empty($instance['menu_description']) ? '' : apply_filters('widget_menu_description', $instance['menu_description']);
		
		$image_url = empty($instance['image_url']) ? '' : apply_filters('widget_image_url', $instance['image_url']);
		
		?>
         					
					<li>
                          <a href="javascript:void(0);">
                              <Div class="side-bar-list-img side-bar-list-odd">
                                <img src="<?php echo $image_url; ?>" alt="img">
                                <Div class="side-bar-list-content">
                                    <h6><?php echo $menu_title;?></h6>
                                    <span><?php echo $menu_description;?></span>
                                </Div>
                              </Div>
                          </a>
                    </li>
        <?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['menu_title'] = strip_tags($new_instance['menu_title']);
		$instance['menu_description'] = ($new_instance['menu_description']);
		$instance['image_url'] = ($new_instance['image_url']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'menu_title' => '', 'desc' => '', 'image_url' => '' ) );		
		
		$menu_title = strip_tags($instance['menu_title']);
		$menu_description = ($instance['menu_description']);
		$image_url = ($instance['image_url']);
		
?>
		<p><label for="<?php echo $this->get_field_id('menu_title'); ?>"><?php _e('Menu Title');?>: <input class="widefat" id="<?php echo $this->get_field_id('menu_title'); ?>" name="<?php echo $this->get_field_name('menu_title'); ?>" type="text" value="<?php echo attribute_escape($menu_title); ?>" /></label></p>
        
        <p><label for="<?php echo $this->get_field_id('menu_description'); ?>"><?php _e('Menu Description');?>: 
        <textarea rows="08" class="widefat" id="<?php echo $this->get_field_id('menu_description'); ?>" name="<?php echo $this->get_field_name('menu_description'); ?>"><?php echo attribute_escape($menu_description); ?></textarea></label></p>
        
        <p><label for="<?php echo $this->get_field_id('image_url'); ?>"><?php _e('Image Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('image_url'); ?>" name="<?php echo $this->get_field_name('image_url'); ?>" type="text" value="<?php echo attribute_escape($image_url); ?>" /></label></p>
        
        
<?php
	}
	}
register_widget('eat_drink_food_menu_widget');


class eat_drink_food_menu_heading_text_widget extends WP_Widget {
	function eat_drink_food_menu_heading_text_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Eat&Drink Page Food Menu Heading Widget' );		
		$this->WP_Widget('eat_drink_food_menu_heading_text_widget', 'CR &rarr; Food Menu Heading Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		echo $heading_text;
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '' ) );		
		
		$heading_text = strip_tags($instance['heading_text']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Menu Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
                
<?php
	}
	}
register_widget('eat_drink_food_menu_heading_text_widget');

//******************* contact page holiday regular hours widgets code****************//

class contact_hours_widget extends WP_Widget {
	function contact_hours_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Contact Page Regular Hours Widget' );		
		$this->WP_Widget('contact_hours_widget', 'CR &rarr; Contact Regular Hours Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$day_name1 = empty($instance['day_name1']) ? '' : apply_filters('widget_day_name1', $instance['day_name1']);
		$day_timing1 = empty($instance['day_timing1']) ? '' : apply_filters('widget_day_timing1', $instance['day_timing1']);
		$day_name2 = empty($instance['day_name2']) ? '' : apply_filters('widget_day_name2', $instance['day_name2']);
		$day_timing2 = empty($instance['day_timing2']) ? '' : apply_filters('widget_day_timing2', $instance['day_timing2']);
		?>
		<div class="clearfix common">
			<div class="one-half regular-day">
			<span class="working-day"><?php echo $day_name1;?></span>
			<span class="working-time"><?php echo $day_timing1;?></span>
			</div>
			<div class="one-half regular-day">
			<span class="working-day"><?php echo $day_name2;?></span>
			<span class="working-time"><?php echo $day_timing2;?></span>
			</div>
		</div>
		
		
		
		<?php
		//echo $heading_text;
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['day_name1'] = strip_tags($new_instance['day_name1']);
		$instance['day_timing1'] = strip_tags($new_instance['day_timing1']);
		$instance['day_name2'] = strip_tags($new_instance['day_name2']);
		$instance['day_timing2'] = strip_tags($new_instance['day_timing2']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'day_name1' => '', 'day_timing1' => '', 'day_name2' => '', 'day_timing2' => '' ) );		
		
		$day_name1 = strip_tags($instance['day_name1']);
		$day_timing1 = strip_tags($instance['day_timing1']);
		$day_name2 = strip_tags($instance['day_name2']);
		$day_timing2 = strip_tags($instance['day_timing2']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('day_name1'); ?>"><?php _e('Day Name First');?>: <input class="widefat" id="<?php echo $this->get_field_id('day_name1'); ?>" name="<?php echo $this->get_field_name('day_name1'); ?>" type="text" value="<?php echo attribute_escape($day_name1); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('day_timing1'); ?>"><?php _e('Day Timings');?>: <input class="widefat" id="<?php echo $this->get_field_id('day_timing1'); ?>" name="<?php echo $this->get_field_name('day_timing1'); ?>" type="text" value="<?php echo attribute_escape($day_timing1); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('day_name2'); ?>"><?php _e('Day Name Second');?>: <input class="widefat" id="<?php echo $this->get_field_id('day_name2'); ?>" name="<?php echo $this->get_field_name('day_name2'); ?>" type="text" value="<?php echo attribute_escape($day_name2); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('day_timing2'); ?>"><?php _e('Day Timings');?>: <input class="widefat" id="<?php echo $this->get_field_id('day_timing2'); ?>" name="<?php echo $this->get_field_name('day_timing2'); ?>" type="text" value="<?php echo attribute_escape($day_timing2); ?>" /></label></p>
                
<?php
	}
	}
register_widget('contact_hours_widget');

class contact_regular_hour_heading_change_widget extends WP_Widget {
	function contact_regular_hour_heading_change_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Contact Regular Hour Heading Change Widget' );		
		$this->WP_Widget('contact_regular_hour_heading_change_widget', 'CR &rarr; Contact Regular Hour Heading Change Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		
		?><h2 class="txtcenter"><?php echo $heading_text;?></h2><?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '' ) );		
		
		$heading_text = strip_tags($instance['heading_text']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
                
<?php
	}
	}
register_widget('contact_regular_hour_heading_change_widget');

//******************* contact page holiday hours widgets code****************//

class contact_holidayhours_widget extends WP_Widget {
	function contact_holidayhours_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Contact Set Holiday Hours Widget' );		
		$this->WP_Widget('contact_holidayhours_widget', 'CR &rarr; Set Holiday Hours Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$date1 = empty($instance['date1']) ? '' : apply_filters('widget_date1', $instance['date1']);
		$date_timing1 = empty($instance['date_timing1']) ? '' : apply_filters('widget_date_timing1', $instance['date_timing1']);
		$text1 = empty($instance['text1']) ? '' : apply_filters('widget_text1', $instance['text1']);
		$text_heading = empty($instance['text_heading']) ? '' : apply_filters('widget_text_heading', $instance['text_heading']);
		$date2 = empty($instance['date2']) ? '' : apply_filters('widget_date2', $instance['date2']);
		$date_timing2 = empty($instance['date_timing2']) ? '' : apply_filters('widget_date_timing2', $instance['date_timing2']);
		$text2 = empty($instance['text2']) ? '' : apply_filters('widget_text2', $instance['text2']);
		?>
		<?php if(!empty($date1) || !empty($date2)) : ?>
			<div class="clearfix common">
				<div class="one-half regular-day">
					<span class="working-day"><?php echo $date1;?></span>
					<span class="working-time"><?php echo $date_timing1;?></span>
					
					
				</div>
				<div class="one-half regular-day">
					<span class="working-day"><?php echo $date2;?></span>
					<span class="working-time"><?php echo $date_timing2;?></span>
					
					
				</div>
			</div>
		<?php endif; ?>
		<?php if(!empty($text1) || !empty($text2)) : ?>
			<div class="clearfix common">
				<div class="one-half regular-day">
					<?php
					if(!empty($text1))
					{
						?>
						<p>
						<?php
						if(!empty($text_heading))
						{
							?><span class=" holiday-text"><?php echo $text_heading;?></span><?php
						}
						
						echo $text1;?></p>
						<?php
					} ?>
					
					
				</div>
				<div class="one-half regular-day">
					
					<?php
					if(!empty($text2))
					{
						?>
						<p><?php echo $text2;?></p>
						<?php
					} ?>
					
				</div>
			</div>
		<?php endif; ?>
		
		
		<?php
		//echo $heading_text;
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['date1'] = strip_tags($new_instance['date1']);
		$instance['date_timing1'] = strip_tags($new_instance['date_timing1']);
		$instance['text1'] = strip_tags($new_instance['text1']);
		$instance['date2'] = strip_tags($new_instance['date2']);
		$instance['date_timing2'] = strip_tags($new_instance['date_timing2']);
		$instance['text2'] = strip_tags($new_instance['text2']);
		$instance['text_heading'] = strip_tags($new_instance['text_heading']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'date1' => '', 'date_timing1' => '', 'text1' => '', 'date2' => '', 'date_timing2' => '', 'text2' => '', 'text_heading' => '' ) );		
		
		$date1 = strip_tags($instance['date1']);
		$date_timing1 = strip_tags($instance['date_timing1']);
		$text1 = strip_tags($instance['text1']);
		$date2 = strip_tags($instance['date2']);
		$date_timing2 = strip_tags($instance['date_timing2']);
		$text2 = strip_tags($instance['text2']);
		$text_heading = strip_tags($instance['text_heading']);
		
?>
		<p><label for="<?php echo $this->get_field_id('date1'); ?>"><?php _e('Date First');?>: <input class="widefat" id="<?php echo $this->get_field_id('date1'); ?>" name="<?php echo $this->get_field_name('date1'); ?>" type="text" value="<?php echo attribute_escape($date1); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('date_timing1'); ?>"><?php _e('Date Timings');?>: <input class="widefat" id="<?php echo $this->get_field_id('date_timing1'); ?>" name="<?php echo $this->get_field_name('date_timing1'); ?>" type="text" value="<?php echo attribute_escape($date_timing1); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text_heading'); ?>"><?php _e('Text Heading');?>: <input class="widefat" id="<?php echo $this->get_field_id('text_heading'); ?>" name="<?php echo $this->get_field_name('text_heading'); ?>" type="text" value="<?php echo attribute_escape($text_heading); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text1'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text1'); ?>" name="<?php echo $this->get_field_name('text1'); ?>"><?php echo attribute_escape($text1); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('date2'); ?>"><?php _e('Date Second');?>: <input class="widefat" id="<?php echo $this->get_field_id('date2'); ?>" name="<?php echo $this->get_field_name('date2'); ?>" type="text" value="<?php echo attribute_escape($date2); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('date_timing2'); ?>"><?php _e('Date Timings');?>: <input class="widefat" id="<?php echo $this->get_field_id('date_timing2'); ?>" name="<?php echo $this->get_field_name('date_timing2'); ?>" type="text" value="<?php echo attribute_escape($date_timing2); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text2'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text2'); ?>" name="<?php echo $this->get_field_name('text2'); ?>"><?php echo attribute_escape($text2); ?></textarea></label></p>
                
<?php
	}
	}
register_widget('contact_holidayhours_widget');

class contact_holiday_hour_heading_change_widget extends WP_Widget {
	function contact_holiday_hour_heading_change_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Holiday Hour Heading Change Widget' );		
		$this->WP_Widget('contact_holiday_hour_heading_change_widget', 'CR &rarr; Contact Holiday Hour Heading Change Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		
		?><h2 class="txtcenter"><?php echo $heading_text;?></h2><?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '' ) );		
		
		$heading_text = strip_tags($instance['heading_text']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
                
<?php
	}
	}
register_widget('contact_holiday_hour_heading_change_widget');

class kart_racing_sidebar_widget extends WP_Widget {
	function kart_racing_sidebar_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Kart Racing Widget' );		
		$this->WP_Widget('kart_racing_sidebar_widget', 'CR &rarr; Kart Racing Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		$link_name = empty($instance['link_name']) ? '' : apply_filters('widget_link_name', $instance['link_name']);
		$link_url = empty($instance['link_url']) ? '' : apply_filters('widget_link_url', $instance['link_url']);
		
		?>
		<div class="fitst-kart-list">
                      <h4><span><img src="<?php echo get_template_directory_uri(); ?>/images/rightside-arrow.png" alt="img"></span><?php echo $heading_text;?></h4>
                      <div class="fitst-kart-list-cont">
                      	<p><?php echo $text;?></p>
                      	<a href="<?php echo $link_url;?>"><?php echo $link_name;?></a>
                      </div>
        </div>
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['link_name'] = strip_tags($new_instance['link_name']);
		$instance['link_url'] = strip_tags($new_instance['link_url']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '',  'text' => '', 'link_name' => '', 'link_url' => '' ) );
		
		$heading_text = strip_tags($instance['heading_text']);
		$text = strip_tags($instance['text']);
		$link_name = strip_tags($instance['link_name']);
		$link_url = strip_tags($instance['link_url']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo attribute_escape($text); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_name'); ?>"><?php _e('Link Name');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_name'); ?>" name="<?php echo $this->get_field_name('link_name'); ?>" type="text" value="<?php echo attribute_escape($link_name); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_url'); ?>"><?php _e('Link Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_url'); ?>" name="<?php echo $this->get_field_name('link_url'); ?>" type="text" value="<?php echo attribute_escape($link_url); ?>" /></label></p>
                
<?php
	}
	}
register_widget('kart_racing_sidebar_widget');

class velocity_vr_racing_sidebar_widget extends WP_Widget {
	function velocity_vr_racing_sidebar_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Velocity VR Widget' );		
		$this->WP_Widget('velocity_vr_racing_sidebar_widget', 'CR &rarr; Velocity VR Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		$link_name = empty($instance['link_name']) ? '' : apply_filters('widget_link_name', $instance['link_name']);
		$link_url = empty($instance['link_url']) ? '' : apply_filters('widget_link_url', $instance['link_url']);
		
		?>
		<div class="fitst-kart-list">
                      <h4><span><img src="<?php echo get_template_directory_uri(); ?>/images/rightside-arrow.png" alt="img"></span><?php echo $heading_text;?></h4>
                      <div class="fitst-kart-list-cont">
                      	<p><?php echo $text;?></p>
                      	<a href="<?php echo $link_url;?>"><?php echo $link_name;?></a>
                      </div>
        </div>
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['link_name'] = strip_tags($new_instance['link_name']);
		$instance['link_url'] = strip_tags($new_instance['link_url']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '',  'text' => '', 'link_name' => '', 'link_url' => '' ) );
		
		$heading_text = strip_tags($instance['heading_text']);
		$text = strip_tags($instance['text']);
		$link_name = strip_tags($instance['link_name']);
		$link_url = strip_tags($instance['link_url']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo attribute_escape($text); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_name'); ?>"><?php _e('Link Name');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_name'); ?>" name="<?php echo $this->get_field_name('link_name'); ?>" type="text" value="<?php echo attribute_escape($link_name); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_url'); ?>"><?php _e('Link Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_url'); ?>" name="<?php echo $this->get_field_name('link_url'); ?>" type="text" value="<?php echo attribute_escape($link_url); ?>" /></label></p>
                
<?php
	}
	}
register_widget('velocity_vr_racing_sidebar_widget');


class kart_racing_sidebar_pdf_popup_widget extends WP_Widget {
	function kart_racing_sidebar_pdf_popup_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Kart Pdf Popup Widget' );		
		$this->WP_Widget('kart_racing_sidebar_pdf_popup_widget', 'CR &rarr; Kart Pdf Popup Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		$link_name = empty($instance['link_name']) ? '' : apply_filters('widget_link_name', $instance['link_name']);
		$link_url = empty($instance['link_url']) ? '' : apply_filters('widget_link_url', $instance['link_url']);
		$popup_id = empty($instance['popup_id']) ? '' : apply_filters('widget_popup_id', $instance['popup_id']);
		$popup_text = empty($instance['popup_text']) ? '' : apply_filters('widget_popup_text', $instance['popup_text']);
		
		?>
		<div class="fitst-kart-list">
                      <h4><span><img src="<?php echo get_template_directory_uri(); ?>/images/rightside-arrow.png" alt="img"></span><?php echo $heading_text;?></h4>
                      <div class="fitst-kart-list-cont">
                      	<p><?php echo $text;?></p>
                      	<a href="#<?php echo $popup_id;?>" class="popup-with-form"><?php echo $link_name;?></a>
                      </div>
        </div>
		<div id="<?php echo $popup_id;?>" class="popupbox-main mfp-hide white-popup-block"><div class="popup-content"><div class="popupbox-inner popupbox-inner-scroll">
		<?php if($link_url!="")
		{
			?>
			<embed width="100%" height="450" src="<?php echo $link_url;?>" type="application/pdf"></embed>
			<?php
		} echo $popup_text;?>
		
		
		</div></div></div>
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['link_name'] = strip_tags($new_instance['link_name']);
		$instance['link_url'] = strip_tags($new_instance['link_url']);
		$instance['popup_id'] = strip_tags($new_instance['popup_id']);
		$instance['popup_text'] = $new_instance['popup_text'];
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '',  'text' => '', 'link_name' => '', 'link_url' => '', 'popup_id' => '', 'popup_text' => '' ) );
		
		$heading_text = strip_tags($instance['heading_text']);
		$text = strip_tags($instance['text']);
		$link_name = strip_tags($instance['link_name']);
		$link_url = strip_tags($instance['link_url']);
		$popup_id = strip_tags($instance['popup_id']);
		$popup_text = $instance['popup_text'];
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo attribute_escape($text); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_name'); ?>"><?php _e('Link Name');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_name'); ?>" name="<?php echo $this->get_field_name('link_name'); ?>" type="text" value="<?php echo attribute_escape($link_name); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_url'); ?>"><?php _e('Pdf Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_url'); ?>" name="<?php echo $this->get_field_name('link_url'); ?>" type="text" value="<?php echo attribute_escape($link_url); ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('popup_text'); ?>"><?php _e('Pop Up Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('popup_text'); ?>" name="<?php echo $this->get_field_name('popup_text'); ?>"><?php echo stripslashes(attribute_escape($popup_text)); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('popup_id'); ?>"><?php _e('Popup Unique Id');?>: <input class="widefat" id="<?php echo $this->get_field_id('popup_id'); ?>" name="<?php echo $this->get_field_name('popup_id'); ?>" type="text" value="<?php echo attribute_escape($popup_id); ?>" /></label></p>
                
<?php
	}
	}
register_widget('kart_racing_sidebar_pdf_popup_widget');

class velocity_vr_sidebar_pdf_popup_widget extends WP_Widget {
	function velocity_vr_sidebar_pdf_popup_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Velocity VR Pdf Popup Widget' );		
		$this->WP_Widget('velocity_vr_sidebar_pdf_popup_widget', 'CR &rarr; Velocity VR Pdf Popup Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		$link_name = empty($instance['link_name']) ? '' : apply_filters('widget_link_name', $instance['link_name']);
		$link_url = empty($instance['link_url']) ? '' : apply_filters('widget_link_url', $instance['link_url']);
		$popup_id = empty($instance['popup_id']) ? '' : apply_filters('widget_popup_id', $instance['popup_id']);
		$popup_text = empty($instance['popup_text']) ? '' : apply_filters('widget_popup_text', $instance['popup_text']);
		
		?>
		<div class="fitst-kart-list">
                      <h4><span><img src="<?php echo get_template_directory_uri(); ?>/images/rightside-arrow.png" alt="img"></span><?php echo $heading_text;?></h4>
                      <div class="fitst-kart-list-cont">
                      	<p><?php echo $text;?></p>
                      	<a href="#<?php echo $popup_id;?>" class="popup-with-form"><?php echo $link_name;?></a>
                      </div>
        </div>
		<div id="<?php echo $popup_id;?>" class="popupbox-main mfp-hide white-popup-block"><div class="popup-content"><div class="popupbox-inner popupbox-inner-scroll">
		<?php if($link_url!="")
		{
			?>
			<embed width="100%" height="450" src="<?php echo $link_url;?>" type="application/pdf"></embed>
			<?php
		} echo $popup_text;?>
		
		
		</div></div></div>
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['link_name'] = strip_tags($new_instance['link_name']);
		$instance['link_url'] = strip_tags($new_instance['link_url']);
		$instance['popup_id'] = strip_tags($new_instance['popup_id']);
		$instance['popup_text'] = $new_instance['popup_text'];
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '',  'text' => '', 'link_name' => '', 'link_url' => '', 'popup_id' => '', 'popup_text' => '' ) );
		
		$heading_text = strip_tags($instance['heading_text']);
		$text = strip_tags($instance['text']);
		$link_name = strip_tags($instance['link_name']);
		$link_url = strip_tags($instance['link_url']);
		$popup_id = strip_tags($instance['popup_id']);
		$popup_text = $instance['popup_text'];
		
?>
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo attribute_escape($text); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_name'); ?>"><?php _e('Link Name');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_name'); ?>" name="<?php echo $this->get_field_name('link_name'); ?>" type="text" value="<?php echo attribute_escape($link_name); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('link_url'); ?>"><?php _e('Pdf Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('link_url'); ?>" name="<?php echo $this->get_field_name('link_url'); ?>" type="text" value="<?php echo attribute_escape($link_url); ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('popup_text'); ?>"><?php _e('Pop Up Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('popup_text'); ?>" name="<?php echo $this->get_field_name('popup_text'); ?>"><?php echo stripslashes(attribute_escape($popup_text)); ?></textarea></label></p>
		
		<p><label for="<?php echo $this->get_field_id('popup_id'); ?>"><?php _e('Popup Unique Id');?>: <input class="widefat" id="<?php echo $this->get_field_id('popup_id'); ?>" name="<?php echo $this->get_field_name('popup_id'); ?>" type="text" value="<?php echo attribute_escape($popup_id); ?>" /></label></p>
                
<?php
	}
	}
register_widget('velocity_vr_sidebar_pdf_popup_widget');


class play_exclusive_events_widget extends WP_Widget {
	function play_exclusive_events_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Play Exclusive Events Widget' );		
		$this->WP_Widget('play_exclusive_events_widget', 'CR &rarr; Play Exclusive Events Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		$logo_url = empty($instance['logo_url']) ? '' : apply_filters('widget_logo_url', $instance['logo_url']);
		$image_url = empty($instance['image_url']) ? '' : apply_filters('widget_image_url', $instance['image_url']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		
		?>
		<Div class="grup-one-third">
            <span class="group-item" style="background-image:url(<?php echo $image_url; ?>);"></span>
            <div class="grup-one-third-cont">
              <span class="head-img">
                <img src="<?php echo $logo_url; ?>" alt="img">
              </span>
              <span class="head"><?php echo $title; ?></span>
              <p><?php echo $text; ?></p>
            </div>
          </Div><!--grup-one-third ends here-->
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['logo_url'] = strip_tags($new_instance['logo_url']);
		$instance['image_url'] = strip_tags($new_instance['image_url']);
		$instance['text'] = strip_tags($new_instance['text']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '',  'logo_url' => '', 'image_url' => '', 'text' => '' ) );
		
		$title = strip_tags($instance['title']);
		$logo_url = strip_tags($instance['logo_url']);
		$image_url = strip_tags($instance['image_url']);
		$text = strip_tags($instance['text']);
		
		
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title');?>: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('logo_url'); ?>"><?php _e('Logo Url');?>: <input type="text" class="widefat" id="<?php echo $this->get_field_id('logo_url'); ?>" name="<?php echo $this->get_field_name('logo_url'); ?>" value="<?php echo attribute_escape($logo_url); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('image_url'); ?>"><?php _e('Image Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('image_url'); ?>" name="<?php echo $this->get_field_name('image_url'); ?>" type="text" value="<?php echo attribute_escape($image_url); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo attribute_escape($text); ?></textarea></label></p>
                
<?php
	}
	}
register_widget('play_exclusive_events_widget');

class play_outdoor_games_widget extends WP_Widget {
	function play_outdoor_games_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Play Outdoor Games Widget' );		
		$this->WP_Widget('play_outdoor_games_widget', 'CR &rarr; Play Outdoor Games Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		$logo_url = empty($instance['logo_url']) ? '' : apply_filters('widget_logo_url', $instance['logo_url']);
		$image_url = empty($instance['image_url']) ? '' : apply_filters('widget_image_url', $instance['image_url']);
		$text = empty($instance['text']) ? '' : apply_filters('widget_text', $instance['text']);
		$apply_css = empty($instance['apply_css']) ? '' : apply_filters('widget_apply_css', $instance['apply_css']);
		
		?>
		<div class="one-half outdoor-game-sec1">
                <img src="<?php echo $image_url; ?>" alt="img">
               <div class="outdoor-game-sec1-cont <?php if($apply_css=="yes"){ echo 'outdoor-game-sec2';} ?>"> 
               <h2><img src="<?php echo $logo_url; ?>" alt="img"><?php echo $title; ?></h2>
               <p><?php echo $text; ?></p>
               </div>
            </div>
		
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['logo_url'] = strip_tags($new_instance['logo_url']);
		$instance['image_url'] = strip_tags($new_instance['image_url']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['apply_css'] = strip_tags($new_instance['apply_css']);
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '',  'logo_url' => '', 'image_url' => '', 'text' => '', 'apply_css' => '' ) );
		
		$title = strip_tags($instance['title']);
		$logo_url = strip_tags($instance['logo_url']);
		$image_url = strip_tags($instance['image_url']);
		$text = strip_tags($instance['text']);
		$apply_css = strip_tags($instance['apply_css']);
		
		
?>       
        <p><label for="<?php echo $this->get_field_id('apply_css'); ?>"><?php _e('Apply Css');?>: <input class="widefat" name="<?php echo $this->get_field_name('apply_css'); ?>" type="radio" value="yes" <?php if(attribute_escape($apply_css)=="yes"){ echo checked;} ?>/>yes<input class="widefat" name="<?php echo $this->get_field_name('apply_css'); ?>" type="radio" value="no" <?php if(attribute_escape($apply_css)=="no" || attribute_escape($apply_css)==""){ echo checked;} ?>/>no</label></p>
		
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title');?>: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('logo_url'); ?>"><?php _e('Logo Url');?>: <input type="text" class="widefat" id="<?php echo $this->get_field_id('logo_url'); ?>" name="<?php echo $this->get_field_name('logo_url'); ?>" value="<?php echo attribute_escape($logo_url); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('image_url'); ?>"><?php _e('Image Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('image_url'); ?>" name="<?php echo $this->get_field_name('image_url'); ?>" type="text" value="<?php echo attribute_escape($image_url); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text');?>: <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo attribute_escape($text); ?></textarea></label></p>
                
<?php
	}
	}
register_widget('play_outdoor_games_widget');


class group_event_form_widget extends WP_Widget {
	function group_event_form_widget() {
	//Constructor
		$widget_ops = array('classname' => 'BottomPageBox', 'description' => 'Contact Form Widget' );		
		$this->WP_Widget('group_event_form_widget', 'CR &rarr; Group Event Contact Form Widget', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$heading_text = empty($instance['heading_text']) ? '' : apply_filters('widget_heading_text', $instance['heading_text']);
		$shoot_message = empty($instance['shoot_message']) ? '' : apply_filters('widget_shoot_message', $instance['shoot_message']);
		$response_message = empty($instance['response_message']) ? '' : apply_filters('widget_response_message', $instance['response_message']);
		$heading_text= !empty($heading_text) ? $heading_text : "Want to learn more?";
		$shoot_message= !empty($shoot_message) ? $shoot_message : "Shoot us a message!";
		$response_message= !empty($response_message) ? $response_message : "How would you like us to respond?";
		?>
		<div class=" wemake-content-form">
         <span>
         <h4><?php echo $heading_text;?></h4>
         <span class=" sub-head"><?php echo $shoot_message;?></span>
		 <span class=" sub-head" id="success_msg" style="display:none;"><?php echo get_option( THEME_PREFIX.'thank_you_msg');?></span>
         </span>
		 <form class=" main-form" action="<?php echo admin_url('admin-ajax.php'); ?>">
		 <input type="hidden" name="action" value="event_contact_detail_submit" />
         <input name="name" type="text" class=" field2 required" placeholder="Name*">
         <input name="email" type="text" class=" field2 required" placeholder="email*">
         <input name="phone" type="text" class=" field2 required" placeholder="phone*">
         <textarea name="question" cols="" rows="" class="field2 required" placeholder="Type your questions here*"></textarea>
         <div class=" check-blog clearfix">
         <span><?php echo $response_message;?>*</span>
         <div class=" one-half2"><input name="response[]" type="checkbox" value="phone" class="check-box" onchange="open_input();" id="phone"><label>Phone</label></div>
         <div class=" one-half2"><input name="response[]" type="checkbox" value="email" class="check-box" onchange="open_input();" id="email"><label>Email</label></div>
         </div>
		 <div class="error_msg"></div>
		 <input name="response_email" type="hidden" class=" field2 required" placeholder="email*" id="response_email">
         <input name="response_phone" type="hidden" class=" field2 required" placeholder="phone*" id="response_phone">
         <script src='https://www.google.com/recaptcha/api.js'></script>
          <div class="g-recaptcha" data-sitekey="6Lcl6iATAAAAAPcqEgbDOCittX9Rfet-yvlGLkIU" style="transform:scale(0.74);-webkit-transform:scale(0.74);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
         <button class="group_event_submit submit-btn" type="button">submit</button>
         </form>
         </div>
		
		
		<?php
	}
		function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		
		$instance['heading_text'] = strip_tags($new_instance['heading_text']);
		$instance['shoot_message'] = strip_tags($new_instance['shoot_message']);
		$instance['response_message'] = strip_tags($new_instance['response_message']);
		
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'heading_text' => '',  'shoot_message' => '', 'response_message' => '' ) );
		
		$heading_text = strip_tags($instance['heading_text']);
		$shoot_message = strip_tags($instance['shoot_message']);
		$response_message = strip_tags($instance['response_message']);
				
?>       
        
		
		<p><label for="<?php echo $this->get_field_id('heading_text'); ?>"><?php _e('Heading Text');?>: <input class="widefat" id="<?php echo $this->get_field_id('heading_text'); ?>" name="<?php echo $this->get_field_name('heading_text'); ?>" type="text" value="<?php echo attribute_escape($heading_text); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('shoot_message'); ?>"><?php _e('Shoot Message');?>: <input type="text" class="widefat" id="<?php echo $this->get_field_id('shoot_message'); ?>" name="<?php echo $this->get_field_name('shoot_message'); ?>" value="<?php echo attribute_escape($shoot_message); ?>" /></label></p>
		
		<p><label for="<?php echo $this->get_field_id('response_message'); ?>"><?php _e('Image Url');?>: <input class="widefat" id="<?php echo $this->get_field_id('response_message'); ?>" name="<?php echo $this->get_field_name('response_message'); ?>" type="text" value="<?php echo attribute_escape($response_message); ?>" /></label></p>
		
		
<?php
	}
	}
register_widget('group_event_form_widget');
