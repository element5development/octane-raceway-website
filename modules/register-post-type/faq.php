<?php
add_action( 'init', 'codex_faq_init' );
function codex_faq_init() {
	$labels = array( 
		'name'              => _x( 'FAQ', 'FAQ', THEME_TEXTDOMAIN ),
		'singular_name'      => _x( 'Faq', 'Faq', THEME_TEXTDOMAIN ),
		'menu_name'          => _x( 'FAQ', 'FAQ', THEME_TEXTDOMAIN ),
		'name_admin_bar'     => _x( 'Faq', 'Faq', THEME_TEXTDOMAIN ),
		'add_new'            => _x( 'Add New', 'testimonials', THEME_TEXTDOMAIN ),
		'add_new_item'       => __( 'Add New Faq', THEME_TEXTDOMAIN ),
		'new_item'           => __( 'New Faq', THEME_TEXTDOMAIN ),
		'edit_item'          => __( 'Edit Faq', THEME_TEXTDOMAIN ),
		'view_item'          => __( 'View Faq', THEME_TEXTDOMAIN ),
		'all_items'          => __( 'All FAQ', THEME_TEXTDOMAIN ),
		'search_items'       => __( 'Search FAQ', THEME_TEXTDOMAIN ),
		'parent_item_colon'  => __( 'Parent FAQ:', THEME_TEXTDOMAIN ),
		'not_found'          => __( 'No testimonials found.', THEME_TEXTDOMAIN ),
		'not_found_in_trash' => __( 'No testimonials found in Trash.', THEME_TEXTDOMAIN )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'faq' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor','custom-fields', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes' )
	);

	register_post_type( 'faq', $args );
}
add_action( 'init', 'create_faq_taxonomies', 0 );
function create_faq_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Faq Categories', THEME_TEXTDOMAIN ),
		'singular_name'     => _x( 'Faq Category', THEME_TEXTDOMAIN ),
		'search_items'      => __( 'Search Faq Categories',THEME_TEXTDOMAIN ),
		'all_items'         => __( 'All Faq Categories',THEME_TEXTDOMAIN ),
		'parent_item'       => __( 'Parent Faq Category',THEME_TEXTDOMAIN ),
		'parent_item_colon' => __( 'Parent Faq Category:',THEME_TEXTDOMAIN ),
		'edit_item'         => __( 'Edit Faq Category' ,THEME_TEXTDOMAIN),
		'update_item'       => __( 'Update Faq Category',THEME_TEXTDOMAIN ),
		'add_new_item'      => __( 'Add New Faq Category',THEME_TEXTDOMAIN ),
		'new_item_name'     => __( 'New Faq Category Name',THEME_TEXTDOMAIN ),
		'menu_name'         => __( 'Faq Category',THEME_TEXTDOMAIN ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'faq-category' ),
	);

	register_taxonomy( 'faq-category', array( 'faq' ), $args );

}