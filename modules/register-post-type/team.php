<?php
add_action( 'init', 'codex_teams_init' );
function codex_teams_init() {
	$labels = array(
		'name'              => _x( 'Teams', 'Teams', THEME_TEXTDOMAIN ),
		'singular_name'      => _x( 'Team', 'Team', THEME_TEXTDOMAIN ),
		'menu_name'          => _x( 'Teams', 'Teams', THEME_TEXTDOMAIN ),
		'name_admin_bar'     => _x( 'Team', 'Team', THEME_TEXTDOMAIN ),
		'add_new'            => _x( 'Add New', 'teams', THEME_TEXTDOMAIN ),
		'add_new_item'       => __( 'Add New Team', THEME_TEXTDOMAIN ),
		'new_item'           => __( 'New Team', THEME_TEXTDOMAIN ),
		'edit_item'          => __( 'Edit Team', THEME_TEXTDOMAIN ),
		'view_item'          => __( 'View Team', THEME_TEXTDOMAIN ),
		'all_items'          => __( 'All Teams', THEME_TEXTDOMAIN ),
		'search_items'       => __( 'Search Teams', THEME_TEXTDOMAIN ),
		'parent_item_colon'  => __( 'Parent Teams:', THEME_TEXTDOMAIN ),
		'not_found'          => __( 'No teams found.', THEME_TEXTDOMAIN ),
		'not_found_in_trash' => __( 'No teams found in Trash.', THEME_TEXTDOMAIN )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor','custom-fields', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes' )
	);

	register_post_type( 'team', $args );
}
add_action( 'init', 'create_team_taxonomies', 0 );
function create_team_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Team Categories', 'Categories' ),
		'singular_name'     => _x( 'Team Category', 'Category' ),
		'search_items'      => __( 'Search Team Categories' ),
		'all_items'         => __( 'All Team Categories' ),
		'parent_item'       => __( 'Parent Team Category' ),
		'parent_item_colon' => __( 'Parent Team Category:' ),
		'edit_item'         => __( 'Edit Team Category' ),
		'update_item'       => __( 'Update Team Category' ),
		'add_new_item'      => __( 'Add New Team Category' ),
		'new_item_name'     => __( 'New Team Category Name' ),
		'menu_name'         => __( 'Team Category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'team-category' ),
	);

	register_taxonomy( 'team-category', array( 'team' ), $args );

}