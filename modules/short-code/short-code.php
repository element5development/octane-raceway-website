	<?php 
	class Octane_ShortCode {
		
		
		public static function email_phone_section_func( $atts, $content ) 
		{
		  ob_start();
		  $email = !empty($atts['email']) ? $atts['email'] : get_option( THEME_PREFIX.'contact_email');
		  $phone = !empty($atts['phone']) ? $atts['phone'] : get_option( THEME_PREFIX.'contact_phone');
		  
		  
		  echo do_shortcode($content);
		 ?>
		 <div class="coming-more-info clearfix">
			<?php
			if( !empty($email) )
			{
			?>
			 <span class="email"><a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o"></i> <?php echo $email; ?> </a></span> 
			<?php
			}
			if( !empty($phone) )
			{
			?>
			 <span class="phone"><a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone"></i> <?php echo $phone; ?></a></span> 
			<?php
			}
			?>
		 </div>
			
			
		  <?php 
		  
		  return ob_get_clean();     
		}
		public static function map_section_func($atts, $content) 
		{
		 ob_start(); 
		 ?>
		 <div class=" octane-recaway-right">
          <img src="<?php echo do_shortcode($content);?>" alt="img">
         </div>
		<?php
		return ob_get_clean();   
		}
		public static function subscriber_func($atts, $content="") 
		{
			
        ob_start();
		$main_heading = $atts['main-heading'];
		$lower_heading = $atts['lower-heading'];
		$subscriber_content_args = array(
										 'container_class'=>'',
										 'subscriber_form_heading_content'=>'<h3>'.$main_heading.'</h3>',
										 'subscriber_form_field_class'=>' field-white',
										 'submit_button_class'=>'button_class',
										 'submit_button_text'=>'SUBSCRIBE',
										 'thank_msg'=>$lower_heading,
									    );
									 
									 
		$subscriber_content_args['form_fields'][] = array(
												'field_is_fixed'=>1,
												'field_section'=>'contact',
												'field_id'=>'name',
												'field_title'=>__('Name*', THEME_TEXTDOMAIN) ,
												'field_type'=>'text',
												'field_width'=>'full',
												'field_is_required'=>'yes',
												'field_size'=>'',
												'field_css'=>'name',
												'field_guide'=> '',
												'field_is_show' =>'yes',
												'field_data_type'=>'text',
											  );

		$subscriber_content_args['form_fields'][] = array(
												'field_is_fixed'=>1,
												'field_section'=>'contact',
												'field_id'=>'email',
												'field_title'=>__('Email*', THEME_TEXTDOMAIN) ,
												'field_type'=>'text',
												'field_width'=>'full',
												'field_is_required'=>'yes',
												'field_size'=>'',
												'field_css'=>'email',
												'field_guide'=> '',
												'field_is_show' =>'yes',
												'field_data_type'=>'text',
											  );
									 
        subscriber_fun($subscriber_content_args);
        return ob_get_clean();
		}
		
		public static function home_subpage_heading_section_func( $atts, $content ) 
		{
		  ob_start();
		  $first_heading = $atts['first-heading'];
		  $second_heading = $atts['second-heading'];
		  $middle_image_url = $atts['middle-image-url'];
		  $css=!empty($atts['css']) ? $atts['css'] : 'no';
		  echo do_shortcode($content);
		  
		 ?>
		 <div class=" racing-header">
			 <div class="racing-arrow <?php if($css=='yes'){ echo 'white-text';} ?>">
			 <?php $var_val=$GLOBALS['var'];
			 $var_val = sprintf("%02d", $var_val);
			 ?>
				.<?php echo $var_val;?> <br /> <i class="fa fa-chevron-down"></i>
			 </div>
			 <div class="racing-heading">
				 <span class="paging-icon-mobile"><img src="<?php echo $middle_image_url;?>" alt="g" id="spin_img1"></span>
				 <span class="high-speed-text <?php if($css=='yes'){ echo 'color-light';} ?>">
				 <?php if($css=='yes')
				 {
					 echo $first_heading;
				 } 
				 else
				 {
					 ?>
					 <span><?php echo $first_heading;?></span>
					 <?php
					 
				 } ?>
				 
				 </span>
				 <span class="paging-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/pagin-circle1.png" alt="g" id="spin_img" class="spinit">
                    <img src="<?php echo $middle_image_url;?>" alt="icon1" class="paging-icon-inner">
                 </span>
				 <span class="high-speed-text">
				 <?php if($css=='no')
				 {
					 echo $second_heading;
				 } 
				 else
				 {
					 ?>
					 <span><?php echo $second_heading;?></span>
					 <?php
					 
				 } ?>
				 </span>     
			 </div>
		</div>
			
		  <?php 
		  
		  return ob_get_clean();     
		}
		public static function home_subpage_content_section_func( $atts, $content ) 
		{
		  ob_start();
		  $pdf_path = $atts['pdf-path'];
		  $slider_image_left_postid = $atts['slider-image-left_postid'];
		  $slider_image_right_postid = $atts['slider-image-right_postid'];
		  $learn_more_link = $atts['learn-more-link'];
		  $learn_more_link = !empty($learn_more_link) ? $learn_more_link : esc_url( get_permalink($slider_image_left_postid) );
		  $view_directory_link = $atts['view-directory-link'];
		  $view_directory_link = !empty($view_directory_link) ? $view_directory_link : esc_url( get_permalink($slider_image_right_postid) );
		  $css=!empty($atts['css']) ? $atts['css'] : 'no';
		  echo do_shortcode($content);
		  
		 ?>
		 <div class="racing-col-common clearfix">
			<div class="racing-col-2">
			
		<ul class=" racing-list">
		
		
		<?php 
			           
					   
					  $args1 = array( 'post_type' => 'attachment','numberposts' => 3, 'post_parent' =>$slider_image_left_postid );
					  $attachments1 = get_posts( $args1 );
						 if ( $attachments1 ) {
							foreach ( $attachments1 as $attachment1 ) 
							{
								
								$full_image1= wp_get_attachment_image_src( $attachment1->ID, 'home_big' );
							    $full_image_path1=$full_image1[0];
								$thumb_image1= wp_get_attachment_image_src( $attachment1->ID, 'custom' );
							    $thumb_image_path1=$thumb_image1[0];
								$full_image_fancy1= wp_get_attachment_image_src( $attachment1->ID, 'full' );
							    $full_image_fancypath1=$full_image_fancy1[0];
							   ?>
							   
								 
							  <li><a href="javascript:void(0);" class="change_image" data-value-small="<?php echo $full_image_path1;?>" data-value-big="<?php echo $full_image_fancypath1;?>"><img src="<?php echo $thumb_image_path1;?>" alt="img">
						<span> <img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img"></span></a></li>
							   
							   <?php
							}
						 } ?>
		
      
     
    </ul>
	<ul class="racing-list2">
	<?php 
					   $args2 = array( 'post_type' => 'attachment','numberposts' => 1, 'post_parent' =>$slider_image_left_postid );
					   $attachments2 = get_posts( $args2 );
					   
						 if ( $attachments2 ) {
							foreach ( $attachments2 as $attachment2 ) 
							{
								$full_image2= wp_get_attachment_image_src( $attachment2->ID, 'home_big' );
								$full_image_fancy2= wp_get_attachment_image_src( $attachment2->ID, 'full' );
							    $full_image_path2=$full_image2[0];
								$full_image_fancy2=$full_image_fancy2[0];
							   ?>
							   
								<li><a class="fancybox" data-fancybox-group="gallery" href="<?php echo $full_image_fancy2;?>"><img class="target_image" src="<?php echo $full_image_path2;?>" alt="img"></a></li>	  
									  
							   <?php
							}
						 } ?>
        
    </ul>
	
				<p <?php if($css=='yes'){ ?>class=" white-text"<?php } ?>><?php echo  get_post_field('post_content', $slider_image_left_postid);?> </p>
				<a href="<?php echo $learn_more_link;?>" class="button-default <?php if($css=='yes'){ echo 'button-white';} ?>">LEARN MORE</a>
			</div>
			
			<div class="racing-col-2">
				<ul class=" racing-list">
		
		
		<?php 
			           
					   
					   $args3 = array( 'post_type' => 'attachment','numberposts' => 3, 'post_parent' =>$slider_image_right_postid );
					  $attachments3 = get_posts( $args3 );
						 if ( $attachments3 ) {
							foreach ( $attachments3 as $attachment3 ) 
							{
								
								$full_image3= wp_get_attachment_image_src( $attachment3->ID, 'home_big' );
							    $full_image_path3=$full_image3[0];
								$thumb_image3= wp_get_attachment_image_src( $attachment3->ID, 'custom' );
							    $thumb_image_path3=$thumb_image3[0];
								$fancy_image3= wp_get_attachment_image_src( $attachment3->ID, 'full' );
							    $fancy_image_path3=$fancy_image3[0];
							   ?>
							   
								 
							  <li><a <a href="javascript:void(0);" class="change_image" data-value-small="<?php echo $full_image_path3;?>" data-value-big="<?php echo $fancy_image_path3;?>"><img src="<?php echo $thumb_image_path3;?>" alt="img">
						<span> <img src="<?php echo get_template_directory_uri(); ?>/images/plus-icon.png" alt="img"></span></a></li>
							   
							   <?php
							}
						 } ?>
		
      
     
    </ul>
	<ul class="racing-list2">
	<?php 
					   $args4 = array( 'post_type' => 'attachment','numberposts' => 1, 'post_parent' =>$slider_image_right_postid );
					   $attachments4 = get_posts( $args4 );
					   
						 if ( $attachments4 ) {
							foreach ( $attachments4 as $attachment4 ) 
							{
							$full_image4= wp_get_attachment_image_src( $attachment4->ID, 'home_big' );
							    $full_image_path4=$full_image4[0];
							$full_image_fancy4= wp_get_attachment_image_src( $attachment4->ID, 'full' );
							    $full_image_fancypath4=$full_image_fancy4[0];	
							   ?>
							   
								<li><a class="fancybox" data-fancybox-group="gallery" href="<?php echo $full_image_fancypath4;?>"><img class="target_image" src="<?php echo $full_image_path4;?>" alt="img"></a></li>	   
									  
							   <?php
							}
						 } ?>
        
    </ul>
				
				<p <?php if($css=='yes'){ ?>class=" white-text"<?php } ?>><?php echo  get_post_field('post_content', $slider_image_right_postid);?></p>
				
			<a href="<?php echo $view_directory_link;?>" class="button-default <?php if($css=='yes'){ echo 'button-white';} ?>">VIEW DIRECTORY</a>
			
				
			</div>
		</div>
			
			
		  <?php 
		  
		  return ob_get_clean();     
		}
		public static function contact_form_func( $atts, $content ) 
		{
		  ob_start();
		  
		  
		  
		 ?>
		 <div class="contact-form-bg">
				<div class=" row">
				<div class=" form-main-bg">
				<h2><?php echo do_shortcode($content);?></h2>
				<h3 id="thanx-msg" style="display:none;">
				
					<?php echo get_option( THEME_PREFIX.'thank_you_msg');?>
				</h3>
				<form class="contact-form conform" action="<?php echo admin_url('admin-ajax.php'); ?>">
				<div class=" clearfix common">
				<input type="hidden" name="action" value="contact_email_submit" />
				<div class=" one-half"> <input name="first_name" type="text" class=" field require_field" placeholder="First Name*"></div>
				<div class=" one-half"> <input name="last_name" type="text" class=" field require_field" placeholder="last Name*"></div>
				</div>
				<div class=" clearfix common">
				<div class=" one-half"> <input name="email" type="text" class=" field require_field" placeholder="Email*"></div>
				<div class=" one-half"> <input name="phone" type="text" class=" field require_field" placeholder="Phone Number*"></div>
				</div>
				<textarea name="message" cols="" rows="" class=" field textarea require_field" placeholder="Your Comment"></textarea>
				<button class=" submit-btn1 contact_button" type="button">SUBMIT</button>
				</form>
				</div>
				</div>
		</div>
			
			
		  <?php 
		  
		  return ob_get_clean();     
		}
		//*********** play page content shortcode*******//
		public static function play_content_section1_func( $atts, $content ) 
		{
		  ob_start();
		  $post_name = $atts['post-name'];
		  $logo_url = $atts['logo-url'];
		  echo do_shortcode($content);
		
			$args=array(
			  'name' => $post_name,
			  'post_type' => 'post',
			  'post_status' => 'publish'
			);
			$my_query = null;
			$my_query = new WP_Query($args);
			if( $my_query->have_posts() ) 
			{
				while ($my_query->have_posts()) : $my_query->the_post(); 
				if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                } ?>
		 <div class="arcode-sec" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center top / cover ;"<?php } ?>>
        <div class="row clearfix">
         <div class="catering-sec-left clearfix">
           <div class="catering-sec-left-one">
               <?php 
			           $posts_id = get_the_ID();
					   $args1 = array( 'post_type' => 'attachment','numberposts' => 4, 'post_parent' =>$posts_id );
					   $attachments1 = get_posts( $args1 );
					   $i=1;
					   
						 if ( $attachments1 ) {
							foreach ( $attachments1 as $attachment1 ) 
							{
								$full_image1= wp_get_attachment_image_src( $attachment1->ID, 'eat_drink_big' );
							    $full_image_path1=$full_image1[0];
								
							   ?>
							   <div class="one">
								 <a href="javascript:void(0)" data-value="<?php echo $full_image_path1;?>" onclick="changeimage(<?php echo $i;?>)" id="clickid<?php echo $i;?>">
								   <div class="one-img" id="change_img<?php echo $i;?>">
									  <?php echo wp_get_attachment_image( $attachment1->ID, 'eat_drink_small' );?>
									  <div class="one-img-hov">+</div>
								   </div>
								 </a>
						       </div>
							   
							   <?php
							$i++;}
						 } ?>
               
               
           </div>
           <?php 
					   $args2 = array( 'post_type' => 'attachment','numberposts' => 1, 'post_parent' =>$posts_id );
					   $attachments2 = get_posts( $args2 );
					   
						 if ( $attachments2 ) {
							foreach ( $attachments2 as $attachment2 ) 
							{
								$full_image2= wp_get_attachment_image_src( $attachment2->ID, 'eat_drink_big' );
							    $full_image_path2=$full_image2[0];
								
							   ?>
							   <div class="catering-sec-left-two">
								 <img src="<?php echo $full_image_path2;?>" alt="img">
							   </div>
									  
									  
							   <?php
							}
						 } ?>
        </div>
          <div class="catering-sec-right">
          <h2>
		  <?php if(!empty($logo_url))
		  { ?><img src="<?php echo $logo_url;?>" alt="img" class="arcode-img"><?php } ?>
		  
		  <?php the_title();?>
		  </h2>
          <p><?php the_content();?></p>
          
        </div>
      </div>
      
    </div>
			
			
		  <?php endwhile;
			}
		  return ob_get_clean();     
		}
		public static function play_content_section2_func( $atts, $content ) 
		{
		  ob_start();
		  $post_name = $atts['post-name'];
		  $logo_url = $atts['logo-url'];
		  echo do_shortcode($content);
		
			$args=array(
			  'name' => $post_name,
			  'post_type' => 'post',
			  'post_status' => 'publish'
			);
			$my_query = null;
			$my_query = new WP_Query($args);
			//$media = get_attached_media( 'image', 225 );
			//print_r($media);
			if( $my_query->have_posts() ) 
			{
				while ($my_query->have_posts()) : $my_query->the_post(); 
				if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								        $thumb_url=$thumb['0']; } ?>
          <div class="mini-bowling-sec" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center top / cover ;"<?php } ?>>
        <div class="row clearfix">
        <div class=" mini-bowling-right clearfix">
           <div class="mini-bowling-left-one">
		   
               <?php 
			           $posts_id = get_the_ID();
					   $args1 = array( 'post_type' => 'attachment','numberposts' => 4, 'post_parent' =>$posts_id );
					   $attachments1 = get_posts( $args1 );
					   $i=1;
					   //echo get_the_ID();
						 if ( $attachments1 ) {
							foreach ( $attachments1 as $attachment1 ) 
							{
								$full_image1= wp_get_attachment_image_src( $attachment1->ID, 'eat_drink_big' );
							    $full_image_path1=$full_image1[0];
								
							   ?>
							   <div class="one">
								 <a href="javascript:void(0)" data-value="<?php echo $full_image_path1;?>" onclick="changeimagemin(<?php echo $i;?>)" id="clickidmin<?php echo $i;?>">
								   <div class="one-img" id="change_imgmin<?php echo $i;?>">
									  <?php echo wp_get_attachment_image( $attachment1->ID, 'eat_drink_small' );?>
									  <div class="one-img-hov">+</div>
								   </div>
								 </a>
						       </div>
							   
							   <?php
							$i++;}
						 } ?>
               
               
           </div>
		   <?php 
					   $args2 = array( 'post_type' => 'attachment','numberposts' => 1, 'post_parent' =>$posts_id );
					   $attachments2 = get_posts( $args2 );
					   
						 if ( $attachments2 ) {
							foreach ( $attachments2 as $attachment2 ) 
							{
								$full_image2= wp_get_attachment_image_src( $attachment2->ID, 'eat_drink_big' );
							    $full_image_path2=$full_image2[0];
								
							   ?>
							   <div class="mini-bowling-right-two">
								 <img src="<?php echo $full_image_path2;?>" alt="img">
							   </div>
									  
									  
							   <?php
							}
						 } ?>
           
        </div>
		
        <div class="mini-bowling-left">
          <h2><?php if(!empty($logo_url))
		  { ?><img src="<?php echo $logo_url;?>" alt="img" class="mini-bowl-img"><?php } 
	  the_title();?></h2>
          <p><?php the_content();?></p>
          
        </div>
      </div>
      
    </div><!--mini-bowling-sec ends here-->
			
		  <?php endwhile;
			}
		  return ob_get_clean();     
		}
		//*********** book party form shortcode****************//
		public static function book_party_form_func( $atts, $content ) 
		{
		  ob_start();
		  
		  echo do_shortcode($content);
		  
		 ?>
		 
				
				<div id="thanx" style="display:none;">Thank you for choosing Octane Raceway! One of our Event Planners will be in contact with you soon.</div>
					
				<form class="contact-form book_form" action="<?php echo admin_url('admin-ajax.php'); ?>">
				<div class=" clearfix common">
				<input type="hidden" name="action" value="book_party_email" />
                <div class=" one-half"> <input name="first_name" type="text" class=" field required" placeholder="First Name*"></div>
                <div class=" one-half"> <input name="last_name" type="text" class=" field required" placeholder="last Name*"></div>
                </div>
                <div class=" clearfix common">
                <div class=" one-half"> <input name="company" type="text" class=" field" placeholder="Company"></div>
                <div class=" one-half"> <input name="zip_code" type="text" class=" field required" placeholder="Zip Code*"></div>
                </div>
                <div class=" clearfix common">
                <div class=" one-half"> <input name="email" type="text" class=" field required" placeholder="Email*" style="text-transform:none;"></div>
                <div class=" one-half"> <input name="phone" type="text" class=" field required" placeholder="Phone Number*"></div>
                </div>
                <div class="clearfix common">
                   <Div class="racing-select">
                     <select name="party_type" class="field field2 required">
					 <option value="">Select Type of Party*</option>
					 <option value="Adult Party">Adult Party</option>
					 <option value="Corporate Event">Corporate Event</option>
					 <option value="Junior Party">Junior Party</option>
					 <option value="Social Event">Social Event</option>
					 </select>
                   </Div>
                   <Div class="racing-select">
                     <select name="guests_count" class="field field2 required">
					 <option value="">Select Number of Guests</option>
					 <?php for($num=8;$num<=300;$num++)
					 { ?>
						 <option value="<?php echo $num;?>"><?php echo $num;?></option>
					<?php } ?>
					 
					 </select>
                   </Div>
                   <Div class="racing-select">
                     <select name="heard_about_us" id="heard_about_us" class="field field2 required" onchange="open_new()">
					 <option value="">How did you hear about us?</option>
					 <option value="Billboard">Billboard</option>
					 <option value="Drive By">Drive By</option>
					 <option value="Facebook">Facebook</option>
					 <option value="Linked In">Linked In</option>
					 <option value="Other">Other</option>
					 <option value="Prior Event">Prior Event</option>
					 <option value="Radio Ad">Radio Ad</option>
					 <option value="Referral">Referral</option>
					 <option value="Restroom Ad">Restroom Ad</option>
					 <option value="Websearch">Websearch</option>
					 </select>
                   </Div>
				   
                </div>
				<Div  class=" clearfix common" id="racing_select_hide" style="display:none">
                     <div class=" one-half"><input type="text" name="heard_about_us_other_option" class="field required" placeholder="Type For Heard About Other Options*" style="text-transform:none;"/></div>
					 
                </Div>
                <textarea name="message" cols="" rows="" placeholder="Type Message Here...." class=" field textarea required" style="text-transform:none;"></textarea>
                <script src='https://www.google.com/recaptcha/api.js'></script>
                 <div class="g-recaptcha" data-sitekey="6Lcl6iATAAAAAPcqEgbDOCittX9Rfet-yvlGLkIU"></div><br />
                <button type="button" class=" submit-btn1 book_button">SUBMIT</button>
                </form>
			
		  <?php 
		  
		  return ob_get_clean();     
		}
		
	}  
	//Contact Form
	add_shortcode('home_subpage_content_section', array('Octane_ShortCode', 'home_subpage_content_section_func'));
	
	add_shortcode('home_subpage_content1', array('Octane_ShortCode', 'home_subpage_heading_section_func'));
	add_shortcode('subscriber', array('Octane_ShortCode', 'subscriber_func'));
	add_shortcode('email_phone_section', array('Octane_ShortCode', 'email_phone_section_func'));
	add_shortcode('map_section', array('Octane_ShortCode', 'map_section_func'));
	add_shortcode('form_section', array('Octane_ShortCode', 'contact_form_func'));
	add_shortcode('play_page_section1', array('Octane_ShortCode', 'play_content_section1_func'));
	add_shortcode('play_page_section2', array('Octane_ShortCode', 'play_content_section2_func'));
	add_shortcode('book-party-form', array('Octane_ShortCode', 'book_party_form_func'));
    