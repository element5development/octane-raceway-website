<?php
/**
 * Template Name: blog
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
$page = get_page_by_title( 'Blog' );
//r_print_r($page);
if ( has_post_thumbnail($page->ID) )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>
	 <div id="banner" class="innerpage-banner">
          <div class="about-us-banner parallax-banner" <?php if(!empty($thumb_url)){ ?>style="background: rgba(0, 0, 0, 0) url(<?php echo $thumb_url;?>) no-repeat scroll center center / cover ;"<?php } ?>>
			   <div class="row"> 
				  <div class="innerpage-banner-in txtcenter">
                    <div class=" banner-top-content">
					    <div class="trans-bg"><h1><?php
							printf( __( ' %s', 'twentyeleven' ), '<span>' . single_cat_title( '', false ) . '</span>' );
						?></h1></div>
						<div class="blog-cat-circle"><img src="<?php echo get_template_directory_uri();?>/images/blog-circle.png" alt="circle" class="spinit"/></div>
                    </div>
				  </div>
			   </div>
          </div>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url();?>">
					<img src="<?php echo $inner_logo_img;?>" alt="img"></a>
				</div>
				<?php	
			}
			?>
				<div id="main-menu">
					<?php
					$defaults = array( 'menu' => 'Middle Menu' );
					wp_nav_menu($defaults);
					?>
				</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		</div><!----main-menu-sec end here--->
		<div class="stay-inform-form"> 
     <div class="row">
        <div class="stay-inform-form-in clearfix">
          <!---<div class=" stay-informed">
		  <h3>STAY INFORMED!</h3>
		  <p>Subscribe to our posts to get them delivered to your inbox.</p>
		  </div>
          <div class=" stay-informed-from">
		  
		  		<form>
				 <div class=" clearfix common">
				 <div class=" one-third">
				  <input name="" type="text" class="field-white" placeholder="Name*"></div>
				  <div class=" one-third">
				  <input name="" type="text" class="field-white" placeholder="Email*"></div>
				  <div class=" one-third">
				 <button class="red-button">SUBSCRIBE</button>
				 </div>
				  </div>
			  </form>
  </div>----><?php echo do_shortcode($page->post_content);?>
        </div>
     </div>
</div>
     </div>  <!--about-us-banner ends here-->
	
    <div id="blogpage"> 
  <div class=" row clearfix">
     <div class="content">
       <div class="blog-page-main">
         <div class="tabs blog-tabs">
		 <?php
		 
		 
		 $no_of_post=$wp_query->found_posts;
		 ?>
        <ul>
          <?php
			$defaults = array( 'menu' => 'Blog Menu' );
			wp_nav_menu($defaults);
			?>
        </ul>
        </div>
         <div class=" blog-listing">
		 
    <ul id="more_list_post">
	<?php
	global $post;
	if(have_posts())
	{
		while(have_posts())
		{
			the_post();
			?>
    <li>
     <div class=" list-img">
	 <?php
			  if ( has_post_thumbnail() )
								{
                                        the_post_thumbnail('full');
								} ?>
     
     <div class=" list-heading"> 
     <h3><?php the_title();?></h3>
     <span class=" date-text"><?php echo get_the_date(); ?></span>
    </div>
     </div>
     <div class="list-bottom-heading clearfix">
       <div class="list-social">
         <?php
							$facebook_link=get_post_meta($post->ID,'facebook_link', true);
							$linkedin_url=get_post_meta($post->ID,'linkedin_url', true);
							$twitter_link=get_post_meta($post->ID,'twitter_link', true);
							if(!empty($facebook_link))
							{ ?>
							<a href="<?php echo $facebook_link;?>"><i class="fa fa-facebook"></i></a>
							<?php }
						   if(!empty($linkedin_url))
							{ ?>
							<a href="<?php echo $facebook_link;?>"><i class="fa fa-twitter"></i></a>	
							<?php }
						   if(!empty($linkedin_url))
							{ ?>
							<a href="<?php echo $facebook_link;?>"><i class="fa fa-linkedin"></i></a>	
							<?php }
						   ?>
       </div>
       <div class="list-read-btn clearfix">
          <a href="<?php the_permalink(); ?>" class="red-button red-button-small">READ</a>
       </div> 
     </div>
    </li>
      <?php
		}
	}
  ?>
       </ul>
	   <script type="text/javascript">
  function get_more_posts(id,number){
	  //alert(id);
		  jQuery('#loding_window').fadeIn();
		  var url =  '<?php echo  admin_url('admin-ajax.php');?>?action=my_action&cat_id='+id+'&number='+number;
			jQuery.post(url, {  },
				function( data1 ) {
				//alert(data1);
				jQuery('#more_list_post').html(data1);
				jQuery('#loding_window').fadeOut();
				jQuery('#load').fadeOut();
				}
				);
		   
	   }
    </script>
	<img src="<?php echo get_template_directory_uri();?>/images/loading.gif" style="padding-left:267px;display:none;" id="loding_window">
	   <?php 
	   
	   if($no_of_post>get_option('posts_per_page'))
	   {
		   ?>
		   <a class="button-default button-full" onclick="get_more_posts(<?php the_category_ID();?>,<?php echo $no_of_post;?>)" id="load">load more</a>
		   <?php
	   } ?>
    
    
    </div>
       </div>
     </div>
     <div id="sidebar">
      <h2>CUSTOMER REVIEWS</h2>
      <div class="sidebar-box customer-list">
      <ul>
      <li>
      <div class="customer-left-img"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-img.jpg" alt="img"></a></div>
      <div class=" customer-right-content">
         <span> <img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-star-icon-jpg.png" alt="img"></span>
         <span> <img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-button.png" alt="img"></span>
         <strong><a href="#">71 reviews</a></strong>
       </div>
      </li>
      <li>
      <div class=" customer-left-img"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-img2.jpg" alt="img"></a></div>
      <div class=" customer-right-content">
            <strong><a href="#">71 reviews</a></strong>
            <span> <img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-star-icon2-jpg.png" alt="img"></span>
            <p>orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
 
       </div>
      </li>
      <li>
      <div class=" customer-left-img"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-img2.jpg" alt="img"></a></div>
      <div class=" customer-right-content">
            <strong><a href="#">71 reviews</a></strong>
            <span> <img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-star-icon2-jpg.png" alt="img"></span>
            <p>orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
 
       </div>
      </li>
      <li>
      <div class=" customer-left-img"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-img2.jpg" alt="img"></a></div>
      <div class=" customer-right-content">
            <strong><a href="#">71 reviews</a></strong>
            <span> <img src="<?php echo get_template_directory_uri(); ?>/images/customer-listin-star-icon2-jpg.png" alt="img"></span>
            <p>orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
 
       </div>
      </li>			 
      </ul>
      </div>
      <div class="sidebar-box tripadvisor">
         <span><img src="<?php echo get_template_directory_uri(); ?>/images/tripadvisor.png" alt="img"></span></div>
      </div>
  </div>
</div>
<?php
get_footer();
?>