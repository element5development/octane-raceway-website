<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 *
 */

 ?>

<div id="blogpage"> 
  <div class=" row clearfix">
     <div class="content">
	 
       <div class="blog-page-main">
         
			<div class="blog-detail">
			
			<?php the_content();?>
			<div class="clearfix idea-box-main">
					<div class="idea-box">
						<a href="<?php echo site_url();?>/blog">Back To Blog Page</a>
					</div>
					<?php $args1 = array('posts_per_page'   => 1, 'category_name'  => 'Blog', 'orderby'     => 'date', 'order'    => 'ASC', 'post_type'   => 'post', 'suppress_filters' => true );
                    $posts_array1 = get_posts( $args1 );

					if($post->ID==$posts_array1[0]->ID)
					{
						?>
						<div class="idea-box button-inactive">
						<a href="javascript:vpid(0);">Previous</a>
						</div>
						<?php
					}
					else
					{
						?>
						<div class="idea-box">
						<?php previous_post_link('%link', 'Previous', FALSE, array(1, 11)); ?>
					</div>
						<?php
					}
				?>
					
					<?php $args = array('posts_per_page'   => 1, 'category_name'  => 'Blog', 'orderby'     => 'date', 'order'    => 'DESC', 'post_type'   => 'post', 'suppress_filters' => true );
                    $posts_array = get_posts( $args );

					if($post->ID==$posts_array[0]->ID)
					{
						?>
						<div class="idea-box button-inactive">
						<a href="javascript:vpid(0);">Next</a>
						</div>
						<?php
					}
					else
					{
						?>
						<div class="idea-box">
						<?php next_post_link( '%link', 'Next', FALSE, array(1, 11) );?>
						</div>
						<?php
					}
				?>
					
			</div>
			
			
			
			
			
			</div>
			




       </div>
     </div>
     
  </div>
</div>