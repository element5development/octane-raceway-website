<?php
/**
 * Template Name: blank-page
 * @package WordPress
 * @subpackage octane
 * @since octane 1.0
 */
get_header();
global $post;
//r_print_r($post);
if(have_posts())
{
	while(have_posts())
	{
			the_post();
if ( has_post_thumbnail() )
								{
                                        $thumb=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$thumb_url=$thumb['0'];
                                }

?>

	 <div id="banner" class="innerpage-banner">
          
          <?php 
           //include 'sb.php'; 
           ?>
        <div id="main-menu-sec" class="menu-innerpage">
			<div class=" row clearfix">
			<?php
			$inner_logo_img= get_option( THEME_PREFIX.'inner_page_logo');
			if(!empty($inner_logo_img))
			{
				?>
				<div class=" logo-small"><a href="<?php echo site_url();?>"> <img src="<?php echo $inner_logo_img;?>" alt="img"></a></div>
				<?php	
			}
			?>
			<div id="main-menu">
			<?php
			$defaults = array( 'menu' => 'Middle Menu' );
			wp_nav_menu($defaults);
			?>
			</div>
			<?php
			$box_title_1= get_option( THEME_PREFIX.'box_title_1');
			$box_title_1_link= get_option( THEME_PREFIX.'box_title_1_link');
				if($box_title_1!="" && $box_title_1_link!="")
				{
				?>
				<!--	<div class=" book-box"><a href="<?php //echo $box_title_1_link;?>"><?php //echo $box_title_1;?></a></div> -->
				<?php
				} ?>
			</div>
		<?php include 'mm.php'; ?>
		</div><!----main-menu-sec end here--->
     </div>  <!--about-us-banner ends here-->
	
<div class="container" style="padding: 110px 0 40px;">
	<div class="row">
		<?php the_content(); ?>
	</div>
</div>
   
   
     
	<?php 
	}
}
	
	?>

	
<?php
get_footer();
?>